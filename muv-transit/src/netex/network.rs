use std::collections::HashMap;

use crate::{
	netex::{
		CommonFrame, PublicationDelivery, QuayRel, ResourceFrame, RouteView, ServiceCalendarFrame,
		ServiceFrame, SiteFrame, TransportMode,
	},
	Mode, Network,
};

impl PublicationDelivery {
	pub fn insert_into(self, network: &mut Network) {
		for frame in self.data_objects.into_iter().flat_map(|f| f.frames) {
			#[allow(clippy::single_match)]
			match frame {
				CommonFrame::SiteFrame(frame) => frame.insert_into(network),
				CommonFrame::InfrastructureFrame(_) => {}
				CommonFrame::ServiceCalendarFrame(frame) => frame.insert_into(network),
				CommonFrame::ServiceFrame(frame) => frame.insert_into(network),
				CommonFrame::ResourceFrame(frame) => frame.insert_into(network),
				CommonFrame::TimetableFrame(_) => {}
			}
		}
	}
}

impl SiteFrame {
	pub fn insert_into(self, network: &mut Network) {
		for mut place in self.stop_places {
			let location = place
				.centroid
				.and_then(|c| c.location)
				.map(|loc| (loc.latitude, loc.longitude));
			let cplace = crate::Stop {
				name: place.name.map(|s| s.content),
				code: None,
				r#ref: None,
				ifopt: place.key_list.remove("GlobalID"),
				location,
				parent_stop: None,
			};

			for quay in place.quays {
				let QuayRel::Owned(mut quay) = quay else {
					continue;
				};

				let location = quay
					.centroid
					.and_then(|c| c.location)
					.map(|loc| (loc.latitude, loc.longitude));
				let cstop = crate::Stop {
					name: quay.name.map(|s| s.content),
					code: None,
					r#ref: None,
					ifopt: quay.key_list.remove("GlobalID"),
					location,
					parent_stop: Some(place.id.to_owned()),
				};
				network.stops.insert(quay.id, cstop);
			}

			network.stops.insert(place.id, cplace);
		}
	}
}

impl ServiceCalendarFrame {
	pub fn insert_into(self, network: &mut Network) {
		let Some(calendar) = self.service_calendar else {
			return;
		};

		for op in calendar.operating_periods {
			network.services.insert(
				op.id,
				crate::Service::Netex {
					from_date: op.from_date.date(),
					to_date: op.to_date.map(|d| d.date()),
					valid_days: op.valid_day_bits,
				},
			);
		}
	}
}

impl ServiceFrame {
	pub fn insert_into(self, network: &mut Network) {
		for line in self.lines {
			let (color, text_color) = if let Some(p) = line.presentation {
				(p.colour, p.text_colour)
			} else {
				(None, None)
			};

			let l = crate::Line {
				authority: line.authority_ref.map(|r| r.r#ref),
				operators: line
					.operator_ref
					.into_iter()
					.chain(line.additional_operators)
					.map(|r| r.r#ref)
					.collect(),
				mode: line.transport_mode.and_then(Into::into),
				name: line.name.content,
				color,
				text_color,
			};
			network.lines.insert(line.id, l);
		}

		let ssp_to_quay: HashMap<_, _> = self
			.stop_assignments
			.into_iter()
			.map(|sa| {
				(
					sa.scheduled_stop_point.unwrap().into_id(),
					sa.quay.unwrap().into_id(),
				)
			})
			.collect();

		for ssp in self.scheduled_stop_points {
			let cstop = crate::Stop {
				name: ssp.name.map(|s| s.content),
				code: None,
				r#ref: None,
				ifopt: None,
				location: ssp.location.map(|l| (l.latitude, l.longitude)),
				parent_stop: ssp_to_quay.get(&ssp.id).cloned(),
			};
			network.stops.insert(ssp.id, cstop);
		}

		let routes: HashMap<_, _> = self
			.routes
			.into_iter()
			.filter_map(|r| Some((r.id, r.line_ref?.r#ref)))
			.collect();

		for jp in self.journey_patterns {
			let jp = jp.journey_pattern();

			let line = match jp.route_view {
				Some(RouteView::Owned(r)) => r.line_ref.map(|r| r.r#ref),
				Some(RouteView::Ref(r)) => routes.get(&r.r#ref).cloned(),
				None => None,
			};
			let stops = jp
				.points_in_sequence
				.into_iter()
				.map(|s| crate::RouteStop {
					id: s.scheduled_stop_point_ref.r#ref,

					arrival: None,   // TODO
					departure: None, // TODO

					alight: s.for_alighting,
					board: s.for_boarding,
				})
				.collect();
			network.routes.insert(
				jp.id,
				crate::Route {
					service: String::new(), // TODO
					line,
					shape: None,
					stops,
				},
			);
		}
	}
}

impl ResourceFrame {
	pub fn insert_into(self, network: &mut Network) {
		for o in self.organisations {
			let o = o.operator();
			let (url, email, phone) = if let Some(cd) = o.contact_details {
				(cd.url, cd.email, cd.phone)
			} else {
				(None, None, None)
			};

			network.organizations.insert(
				o.id,
				crate::Organization {
					name: o.name.map(|s| s.content),
					url,
					email,
					phone,
				},
			);
		}
	}
}

impl From<TransportMode> for Option<Mode> {
	fn from(value: TransportMode) -> Self {
		Some(match value {
			TransportMode::All
			| TransportMode::Unknown
			| TransportMode::AnyMode
			| TransportMode::Other => return None,
			TransportMode::Bus => Mode::Bus,
			TransportMode::TrolleyBus => Mode::TrolleyBus,
			TransportMode::Tram => Mode::Tram,
			TransportMode::Coach => Mode::Coach,
			TransportMode::Rail => Mode::RegionalRail,
			TransportMode::IntercityRail => Mode::LongDistanceRail,
			TransportMode::UrbanRail => Mode::UrbanRail,
			TransportMode::Metro => Mode::Metro,
			TransportMode::Air => Mode::Air,
			TransportMode::Water | TransportMode::Ferry => Mode::Ferry,
			TransportMode::Cablecar => Mode::Cablecar,
			TransportMode::Funicular => Mode::Funicular,
			TransportMode::SnowAndIce => todo!(),
			TransportMode::Taxi => Mode::Taxi,
			TransportMode::Lift => todo!(),
			TransportMode::SelfDrive => todo!(),
		})
	}
}
