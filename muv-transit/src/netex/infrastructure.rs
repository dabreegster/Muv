use serde::Deserialize;

use crate::netex::VersionFrame;

#[derive(Debug, Deserialize)]
pub struct InfrastructureFrame {
	#[serde(flatten, default)]
	pub version_frame: VersionFrame,
}
