use chrono::NaiveTime;
use iso8601_duration::Duration;
use serde::Deserialize;

use crate::netex::{unwrap_list, Ref, TransportMode, VersionFrame};

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TimetableFrame {
	#[serde(rename = "@id")]
	pub id: String,
	#[serde(flatten, default)]
	pub version_frame: VersionFrame,

	#[serde(deserialize_with = "unwrap_list")]
	pub vehicle_journeys: Vec<Journey>,
}

#[derive(Debug, Deserialize)]
pub enum Journey {
	ServiceJourney(ServiceJourney),
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct ServiceJourney {
	#[serde(rename = "@id")]
	pub id: String,

	pub transport_mode: Option<TransportMode>,

	pub departure_time: Option<NaiveTime>,
	pub departure_day_offset: Option<usize>,
	pub journey_duration: Option<Duration>,
	#[serde(rename = "passingTimes", deserialize_with = "unwrap_list")]
	pub passing_times: Vec<TimetabledPassingTime>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct TimetabledPassingTime {
	#[serde(rename = "@id")]
	pub id: String,

	#[serde(rename = "StopPointInJourneyPatternRef")]
	pub stop_point_in_journey_pattern: Option<Ref>,

	pub arrival_time: Option<NaiveTime>,
	#[serde(default)]
	pub arrival_day_offset: usize,

	pub departure_time: Option<NaiveTime>,
	#[serde(default)]
	pub departure_day_offset: usize,
}
