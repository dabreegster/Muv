use serde::Deserialize;

use crate::netex::{deserialize_optional_string, unwrap_list, MultilingualString, VersionFrame};

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ResourceFrame {
	#[serde(rename = "@id")]
	pub id: String,
	#[serde(flatten, default)]
	pub version_frame: VersionFrame,

	#[serde(deserialize_with = "unwrap_list", default)]
	pub data_sources: Vec<DataSource>,
	#[serde(deserialize_with = "unwrap_list", default)]
	pub organisations: Vec<Organisation_>,
}

#[derive(Debug, Deserialize)]
pub struct DataSource {
	#[serde(rename = "@id")]
	pub id: String,
}

#[derive(Debug, Deserialize)]
pub enum Organisation_ {
	Operator(Operator),
	Authority(Authority),
}

impl Organisation_ {
	pub fn operator(self) -> Operator {
		match self {
			Self::Operator(o) => o,
			Self::Authority(a) => a.operator,
		}
	}
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Operator {
	#[serde(rename = "@id")]
	pub id: String,

	#[serde(deserialize_with = "deserialize_optional_string", default)]
	pub public_code: Option<String>,
	#[serde(deserialize_with = "deserialize_optional_string", default)]
	pub private_code: Option<String>,
	pub name: Option<MultilingualString>,
	pub short_name: Option<MultilingualString>,
	pub legal_name: Option<MultilingualString>,
	pub trading_name: Option<MultilingualString>,

	pub contact_details: Option<ContactStructure>,
}

#[derive(Debug, Deserialize)]
pub struct Authority {
	#[serde(flatten)]
	pub operator: Operator,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct ContactStructure {
	pub contact_person: Option<MultilingualString>,
	pub email: Option<String>,
	pub phone: Option<String>,
	pub fax: Option<String>,
	pub url: Option<String>,
}
