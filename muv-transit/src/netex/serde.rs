use std::{
	collections::HashMap,
	fmt::{self, Display, Formatter},
	marker::PhantomData,
	str::FromStr,
};

use serde::{
	de::{self, MapAccess, Visitor},
	Deserialize, Deserializer,
};

pub(crate) fn unwrap_list<'de, T: Deserialize<'de>, D: Deserializer<'de>>(
	deserializer: D,
) -> Result<Vec<T>, D::Error> {
	struct ListVisitor<T>(PhantomData<T>);

	impl<'de, T: Deserialize<'de>> Visitor<'de> for ListVisitor<T> {
		type Value = Vec<T>;

		fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
			formatter.write_str("a list")
		}

		fn visit_map<A: MapAccess<'de>>(self, mut map: A) -> Result<Self::Value, A::Error> {
			map.next_key::<&str>()?;
			let val = map.next_value::<Vec<T>>()?;
			map.next_key::<()>()?;
			Ok(val)
		}
	}

	deserializer.deserialize_struct("", &["$value"], ListVisitor::<T>(PhantomData))
}

pub(crate) fn unwrap_enum_list<'de, T: Deserialize<'de>, D: Deserializer<'de>>(
	deserializer: D,
) -> Result<Vec<T>, D::Error> {
	struct ListVisitor<T>(PhantomData<T>);

	impl<'de, T: Deserialize<'de>> Visitor<'de> for ListVisitor<T> {
		type Value = Vec<T>;

		fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
			formatter.write_str("a list")
		}

		fn visit_map<A: MapAccess<'de>>(self, mut map: A) -> Result<Self::Value, A::Error> {
			let mut res = map.size_hint().map_or_else(Vec::new, Vec::with_capacity);
			while map.next_key::<String>()?.is_some() {
				res.push(map.next_value()?);
			}
			Ok(res)
		}
	}

	deserializer.deserialize_map(ListVisitor::<T>(PhantomData))
}

pub(crate) fn deserialize_from_str<'de, T: FromStr, D: Deserializer<'de>>(
	deserializer: D,
) -> Result<T, D::Error>
where
	T::Err: Display,
{
	struct ParseVisitor<T>(PhantomData<T>);

	impl<'de, T: FromStr> Visitor<'de> for ParseVisitor<T>
	where
		T::Err: Display,
	{
		type Value = T;

		fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
			formatter.write_str("a string")
		}

		fn visit_map<A: MapAccess<'de>>(self, mut map: A) -> Result<Self::Value, A::Error> {
			map.next_key::<&str>()?;
			let val = map.next_value::<String>()?;
			map.next_key::<()>()?;
			val.parse().map_err(de::Error::custom)
		}
	}

	deserializer.deserialize_struct("", &["$value"], ParseVisitor::<T>(PhantomData))
}

pub(crate) fn deserialize_optional_string<'de, D: Deserializer<'de>>(
	deserializer: D,
) -> Result<Option<String>, D::Error> {
	#[derive(Deserialize)]
	struct Text {
		#[serde(rename = "$text")]
		value: Option<String>,
	}

	Text::deserialize(deserializer).map(|t| t.value)
}

pub(crate) fn deserialize_map<'de, D: Deserializer<'de>>(
	deserializer: D,
) -> Result<HashMap<String, String>, D::Error> {
	struct MapVisitor;

	impl<'de> Visitor<'de> for MapVisitor {
		type Value = HashMap<String, String>;

		fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
			formatter.write_str("a list of KeyValues")
		}

		fn visit_map<A: MapAccess<'de>>(self, mut map: A) -> Result<Self::Value, A::Error> {
			let mut res = HashMap::new();
			while map.next_key::<&str>()?.is_some() {
				let kv = map.next_value::<KeyValue>()?;
				res.insert(kv.key, kv.value);
			}
			Ok(res)
		}
	}

	#[derive(Deserialize)]
	#[serde(rename_all = "PascalCase")]
	struct KeyValue {
		key: String,
		value: String,
	}

	deserializer.deserialize_struct("", &["$value"], MapVisitor)
}
