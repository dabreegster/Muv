use serde::Deserialize;

macro_rules! rel {
	($relident:ident($ident:ident) => $str:literal + $refstr:literal) => {
		#[derive(Debug, Deserialize)]
		pub enum $relident {
			#[serde(rename = $str)]
			Owned(Box<$ident>),
			#[serde(rename = $refstr)]
			Ref($crate::netex::Ref),
		}

		impl $relident {
			pub fn into_id(self) -> String {
				match self {
					Self::Owned(v) => v.id,
					Self::Ref(r) => r.r#ref,
				}
			}

			pub fn id(&self) -> &str {
				match self {
					Self::Owned(v) => &v.id,
					Self::Ref(r) => &r.r#ref,
				}
			}
		}
	};
}

#[derive(Debug, Deserialize)]
pub struct MultilingualString {
	#[serde(rename = "@lang")]
	pub lang: Option<String>,
	#[serde(rename = "$text")]
	pub content: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum TransportMode {
	All,
	Unknown,
	Bus,
	TrolleyBus,
	Tram,
	Coach,
	Rail,
	IntercityRail,
	UrbanRail,
	Metro,
	Air,
	Water,
	Cablecar,
	Funicular,
	SnowAndIce,
	Taxi,
	Ferry,
	Lift,
	SelfDrive,
	AnyMode,
	Other,
}

#[derive(Debug, Clone, Copy, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Location {
	pub latitude: f64,
	pub longitude: f64,
}

#[derive(Debug, Deserialize)]
pub struct Ref {
	#[serde(rename = "@ref")]
	pub r#ref: String,
}
