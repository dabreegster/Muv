use std::{
	fmt::{self, Debug, Formatter},
	iter::FlatMap,
	slice::Iter,
	vec::IntoIter,
};

use chrono::NaiveDateTime;
use serde::{de, Deserialize, Deserializer};

use crate::netex::{unwrap_list, MultilingualString, VersionFrame};

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct ServiceCalendarFrame {
	#[serde(rename = "@id")]
	pub id: String,
	#[serde(flatten, default)]
	pub version_frame: VersionFrame,

	pub service_calendar: Option<ServiceCalendar>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ServiceCalendar {
	#[serde(deserialize_with = "unwrap_list", default)]
	pub day_types: Vec<DayType>,
	#[serde(deserialize_with = "unwrap_list", default)]
	pub operating_periods: Vec<UicOperatingPeriod>,
}

#[derive(Debug, Deserialize)]
pub struct DayType {
	#[serde(rename = "@id")]
	pub id: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct UicOperatingPeriod {
	#[serde(rename = "@id")]
	pub id: String,

	pub name: Option<MultilingualString>,
	pub short_name: Option<MultilingualString>,

	pub from_date: NaiveDateTime,
	pub to_date: Option<NaiveDateTime>,
	pub valid_day_bits: DayBits,
}

#[derive(Clone, PartialEq)]
pub struct DayBits(Vec<u8>);

impl DayBits {
	pub fn contains(&self, index: usize) -> bool {
		self.0.get(index / 8).is_some_and(|block| {
			let mask = 1 << (7 - index % 8);
			block & mask != 0
		})
	}
}

impl Debug for DayBits {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		for v in &self.0 {
			write!(f, "{v:b}")?;
		}
		Ok(())
	}
}

impl<'de> Deserialize<'de> for DayBits {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		String::deserialize(deserializer)?
			.as_bytes()
			.chunks(8)
			.map(|c| {
				c.iter()
					.map(|b| match b {
						b'0' => Ok(0),
						b'1' => Ok(1),
						_ => Err("unknown byte in day bits"),
					})
					.enumerate()
					.try_fold(0, |byte, (i, bit)| bit.map(|bit| byte | bit << (7 - i)))
			})
			.collect::<Result<_, _>>()
			.map_err(de::Error::custom)
			.map(DayBits)
	}
}

pub type IterDayBits<'a> = FlatMap<Iter<'a, u8>, [bool; 8], fn(&u8) -> [bool; 8]>;
impl<'a> IntoIterator for &'a DayBits {
	type Item = bool;
	type IntoIter = IterDayBits<'a>;

	fn into_iter(self) -> Self::IntoIter {
		self.0.iter().flat_map(|b| {
			[
				b & 1 << 7 != 0,
				b & 1 << 6 != 0,
				b & 1 << 5 != 0,
				b & 1 << 4 != 0,
				b & 1 << 3 != 0,
				b & 1 << 2 != 0,
				b & 1 << 1 != 0,
				b & 1 << 0 != 0,
			]
		})
	}
}

pub type IntoDayBits = FlatMap<IntoIter<u8>, [bool; 8], fn(u8) -> [bool; 8]>;
impl IntoIterator for DayBits {
	type Item = bool;
	type IntoIter = IntoDayBits;

	fn into_iter(self) -> Self::IntoIter {
		self.0.into_iter().flat_map(|b| {
			[
				b & 1 << 7 != 0,
				b & 1 << 6 != 0,
				b & 1 << 5 != 0,
				b & 1 << 4 != 0,
				b & 1 << 3 != 0,
				b & 1 << 2 != 0,
				b & 1 << 1 != 0,
				b & 1 << 0 != 0,
			]
		})
	}
}
