use chrono::NaiveDateTime;
use serde::Deserialize;

use crate::netex::{unwrap_list, CompositeFrame};

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct PublicationDelivery {
	pub publication_timestamp: NaiveDateTime,

	#[serde(rename = "dataObjects", deserialize_with = "unwrap_list")]
	pub data_objects: Vec<CompositeFrame>,
}
