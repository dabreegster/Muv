use std::{
	collections::{btree_map::IntoIter, BTreeMap},
	error,
	fmt::{self, Display, Formatter},
	iter::Map,
	time::Duration,
};

use chrono::{DateTime, NaiveDate, Utc};
use serde::{de, Deserialize, Deserializer};

use crate::{Id, Vehicle};

#[derive(Debug, Deserialize)]
pub struct AllData(pub BTreeMap<String, Train>);

pub type IntoVehicles = Map<IntoIter<String, Train>, fn((String, Train)) -> Vehicle>;

impl AllData {
	pub const URL: &'static str = "https://tsimobile.viarail.ca/data/allData.json";
	pub const UPDATE_CYCLE: Duration = Duration::from_secs(15);

	#[cfg(feature = "ureq")]
	pub fn fetch_ureq() -> Result<Self, Error> {
		let r = ureq::get(Self::URL).call()?.into_reader();
		Ok(serde_json::from_reader(r)?)
	}

	#[cfg(feature = "reqwest")]
	pub async fn fetch_reqwest() -> Result<Self, Error> {
		let body = reqwest::Client::new()
			.get(Self::URL)
			.header("User-Agent", "muv-transit/0.1")
			.send()
			.await?
			.bytes()
			.await?;
		Ok(serde_json::from_slice(&body)?)
	}

	pub fn vehicles(self) -> IntoVehicles {
		self.0.into_iter().map(Into::into)
	}
}

impl From<(String, Train)> for Vehicle {
	fn from((id, train): (String, Train)) -> Self {
		let code = id.split(' ').next().unwrap().to_owned();
		Self {
			id: Id::new("viarail", id),
			code: Some(code),
			line_name: None,
			trip: None,
			location: train.lat.zip(train.lng),
			heading: train.direction,
			speed: train.speed,
		}
	}
}

#[derive(Debug)]
#[non_exhaustive]
pub enum Error {
	SerdeJson(serde_json::Error),
	#[cfg(feature = "ureq")]
	Ureq(Box<ureq::Error>),
	#[cfg(feature = "reqwest")]
	Reqwest(reqwest::Error),
}

impl From<serde_json::Error> for Error {
	fn from(value: serde_json::Error) -> Self {
		Self::SerdeJson(value)
	}
}

#[cfg(feature = "ureq")]
impl From<ureq::Error> for Error {
	fn from(value: ureq::Error) -> Self {
		Self::Ureq(Box::new(value))
	}
}

#[cfg(feature = "reqwest")]
impl From<reqwest::Error> for Error {
	fn from(value: reqwest::Error) -> Self {
		Self::Reqwest(value)
	}
}

impl Display for Error {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::SerdeJson(e) => write!(f, "failed to parse JSON because of {e}"),
			#[cfg(feature = "ureq")]
			Self::Ureq(e) => write!(f, "{e}"),
			#[cfg(feature = "reqwest")]
			Self::Reqwest(e) => write!(f, "{e}"),
		}
	}
}

impl error::Error for Error {}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Train {
	pub lat: Option<f64>,
	pub lng: Option<f64>,
	pub speed: Option<f64>,
	pub direction: Option<f64>,
	pub poll: Option<DateTime<Utc>>,
	pub departed: bool,
	pub arrived: bool,
	pub from: String,
	pub to: String,
	pub instance: NaiveDate,
	pub poll_min: Option<u16>,
	pub poll_radius: Option<u32>,
	pub times: Vec<Station>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Station {
	pub station: String,
	pub code: String,
	#[serde(deserialize_with = "deserialize_optional_datetime")]
	pub estimated: Option<DateTime<Utc>>,
	#[serde(deserialize_with = "deserialize_optional_datetime")]
	pub scheduled: Option<DateTime<Utc>>,
	pub eta: String,
	pub departure: Option<Time>,
	pub arrival: Option<Time>,
	pub diff: Option<Diff>,
	pub diff_min: Option<i16>,
}

fn deserialize_optional_datetime<'de, D: Deserializer<'de>>(
	deserializer: D,
) -> Result<Option<DateTime<Utc>>, D::Error> {
	let s = String::deserialize(deserializer)?;
	if s == "&mdash;" {
		Ok(None)
	} else {
		s.parse().map(Some).map_err(de::Error::custom)
	}
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum Diff {
	#[serde(rename = "goo")]
	Good,
	#[serde(rename = "med")]
	Medium,
	Bad,
}

#[derive(Debug, Deserialize)]
pub struct Time {
	pub estimated: Option<DateTime<Utc>>,
	pub scheduled: DateTime<Utc>,
}
