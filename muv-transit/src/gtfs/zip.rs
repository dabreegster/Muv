use std::{
	error,
	fmt::{self, Display, Formatter},
	io::{Read, Seek},
};

use csv::DeserializeRecordsIntoIter;
use serde::de::DeserializeOwned;
use zip::{read::ZipFile, result::ZipError, ZipArchive};

use crate::gtfs::{
	Agency, Area, Attribution, BookingRule, Fare, FareLegRule, FareMedia, FareProduct, FareRule,
	FareTransferRule, FeedInfo, Frequency, Level, LocationGroup, LocationGroupStop, Network,
	Pathway, Route, RouteNetwork, Service, ServiceDate, Shape, Stop, StopArea, StopTime, Timeframe,
	Transfer, Translation, Trip,
};

macro_rules! read_files {
	() => {};
	($file:literal $name:ident? -> $t:ty, $($rest:tt)*) => {
        #[inline]
		pub fn $name(&mut self) -> Result<FileIterator<$t>, ZipError> {
			self.read_optional($file)
		}

        read_files!($($rest)*);
	};
	($file:literal $name:ident -> $t:ty, $($rest:tt)*) => {
        #[inline]
		pub fn $name(&mut self) -> Result<FileIterator<$t>, ZipError> {
			self.read($file)
		}

        read_files!($($rest)*);
	};
}

pub struct Reader<R>(pub ZipArchive<R>);

impl<R: Read + Seek> Reader<R> {
	pub fn new(r: R) -> Result<Self, ZipError> {
		ZipArchive::new(r).map(Reader)
	}

	pub fn read<D: DeserializeOwned>(
		&mut self,
		file: &'static str,
	) -> Result<FileIterator<D>, ZipError> {
		let r = self.0.by_name(file)?;
		let iter = csv::Reader::from_reader(r).into_deserialize();
		Ok(FileIterator {
			file,
			iter: Some(iter),
		})
	}

	pub fn read_optional<D: DeserializeOwned>(
		&mut self,
		file: &'static str,
	) -> Result<FileIterator<D>, ZipError> {
		let r = self.read(file);
		if let Err(ZipError::FileNotFound) = r {
			Ok(FileIterator { file, iter: None })
		} else {
			r
		}
	}

	read_files! {
		"agency.txt" agencies -> Agency,
		"stops.txt" stops -> Stop,
		"routes.txt" routes -> Route,
		"trips.txt" trips -> Trip,
		"stop_times.txt" stop_times -> StopTime,
		"calendar.txt" calendar? -> Service,
		"calendar_dates.txt" calendar_dates? -> ServiceDate,
		"fare_attributes.txt" fares? -> Fare,
		"fare_rules.txt" fare_rules? -> FareRule,
		"timeframes.txt" timeframes? -> Timeframe,
		"fare_media.txt" fare_media? -> FareMedia,
		"fare_products.txt" fare_products? -> FareProduct,
		"fare_leg_rules.txt" fare_leg_rules? -> FareLegRule,
		"fare_transfer_rules.txt" fare_transfer_rules? -> FareTransferRule,
		"areas.txt" areas? -> Area,
		"stop_areas.txt" stop_areas? -> StopArea,
		"networks.txt" networks? -> Network,
		"route_networks.txt" route_networks? -> RouteNetwork,
		"shapes.txt" shapes? -> Shape,
		"frequencies.txt" frequencies? -> Frequency,
		"transfers.txt" transfers? -> Transfer,
		"pathways.txt" pathways? -> Pathway,
		"levels.txt" levels? -> Level,
		"location_groups.txt" location_groups? -> LocationGroup,
		"location_group_stops.txt" location_group_stops? -> LocationGroupStop,
		"booking_rules.txt" booking_rules? -> BookingRule,
		"translations.txt" translations? -> Translation,
		"feed_info.txt" feed_info? -> FeedInfo,
		"attributions.txt" attributions? -> Attribution,
	}
}

#[must_use]
pub struct FileIterator<'r, D> {
	pub file: &'static str,
	pub iter: Option<DeserializeRecordsIntoIter<ZipFile<'r>, D>>,
}

impl<'r, D: DeserializeOwned> Iterator for FileIterator<'r, D> {
	type Item = Result<D, FileError>;

	fn next(&mut self) -> Option<Self::Item> {
		let v = self.iter.as_mut()?.next()?;
		Some(v.map_err(|error| FileError::Csv {
			file: self.file,
			error,
		}))
	}
}

#[derive(Debug)]
pub enum FileError {
	Zip(ZipError),
	Csv {
		file: &'static str,
		error: csv::Error,
	},
}

impl Display for FileError {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::Zip(e) => write!(f, "{e}"),
			Self::Csv { file, error } => write!(f, "{file} had error {error}"),
		}
	}
}

impl error::Error for FileError {}

impl From<ZipError> for FileError {
	fn from(value: ZipError) -> Self {
		Self::Zip(value)
	}
}
