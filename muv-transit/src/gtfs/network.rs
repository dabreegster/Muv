use std::{
	collections::{BTreeMap, HashMap, HashSet},
	io::{Read, Seek},
};

use crate::{
	gtfs::{ExceptionType, FileError, LocationType, PickupDropOff, Reader, RouteType},
	Mode,
};

impl<R: Read + Seek> Reader<R> {
	pub fn insert_into(
		mut self,
		network: &mut crate::Network,
		ifopt_stop_ids: bool,
	) -> Result<(), FileError> {
		for agency in self.agencies()? {
			let agency = agency?;
			network.organizations.insert(
				agency.id.unwrap_or_default(),
				crate::Organization {
					name: Some(agency.name),
					url: Some(agency.url),
					email: agency.email,
					phone: agency.phone,
				},
			);
		}

		for stop in self.stops()? {
			let stop = stop?;
			if matches!(
				stop.location_type,
				LocationType::EntranceExit | LocationType::GenericNode
			) {
				continue;
			}

			let cstop = crate::Stop {
				name: stop.name,
				code: stop.code,
				r#ref: stop.platform_code,
				ifopt: ifopt_stop_ids.then(|| stop.id.clone()),
				location: stop.lat.zip(stop.lon),
				parent_stop: stop.parent_station,
			};
			network.stops.insert(stop.id, cstop);
		}

		for route in self.routes()? {
			let route = route?;
			network.lines.insert(
				route.id,
				crate::Line {
					authority: route.agency_id,
					operators: Vec::new(),
					mode: route.r#type.into(),
					name: route.short_name.or(route.long_name).unwrap(),
					color: route.color,
					text_color: route.text_color,
				},
			);
		}

		for trip in self.trips()? {
			let trip = trip?;
			network.routes.insert(
				trip.id,
				crate::Route {
					service: trip.service_id,
					line: Some(trip.route_id),
					shape: trip.shape_id,
					stops: Vec::new(),
				},
			);
		}

		for st in self.stop_times()? {
			let st = st?;
			network
				.routes
				.get_mut(&st.trip_id)
				.unwrap()
				.stops
				.push(crate::RouteStop {
					id: st.stop_id.unwrap(),

					arrival: st.arrival_time,
					departure: st.departure_time,

					board: st.pickup_type == PickupDropOff::Yes,
					alight: st.drop_off_type == PickupDropOff::Yes,
				});
		}

		for service in self.calendar()? {
			let service = service?;
			network.services.insert(
				service.id,
				crate::Service::Gtfs {
					start_date: service.start_date,
					end_date: service.end_date,
					weekdays: service.weekdays,

					added: HashSet::new(),
					removed: HashSet::new(),
				},
			);
		}

		for sd in self.calendar_dates()? {
			let sd = sd?;
			if let Some(service) = network.services.get_mut(&sd.service_id) {
				#[allow(irrefutable_let_patterns)]
				let crate::Service::Gtfs { added, removed, .. } = service
				else {
					panic!();
				};

				match sd.exception_type {
					ExceptionType::Added => added.insert(sd.date),
					ExceptionType::Removed => removed.insert(sd.date),
				};
			}
		}

		let mut between_shapes: HashMap<String, BTreeMap<_, _>> = HashMap::new();
		for shape in self.shapes()? {
			let shape = shape?;
			between_shapes
				.entry(shape.id)
				.or_default()
				.insert(shape.point_sequence, (shape.point_lat, shape.point_lon));
		}
		let shapes_iter = between_shapes
			.into_iter()
			.map(|(id, val)| (id, val.into_values().collect()));
		network.shapes.extend(shapes_iter);

		Ok(())
	}
}

impl From<RouteType> for Option<Mode> {
	fn from(value: RouteType) -> Self {
		Some(match value {
			RouteType::Tram
			| RouteType::CableTram
			| RouteType::CityTram
			| RouteType::LocalTram
			| RouteType::RegionalTram
			| RouteType::SightseeingTram
			| RouteType::ShuttleTram
			| RouteType::AllTram => Mode::Tram,

			RouteType::Subway | RouteType::Metro | RouteType::Underground => Mode::Metro,

			RouteType::Rail
			| RouteType::TouristRail
			| RouteType::RegionalRail
			| RouteType::ReplacementRail
			| RouteType::SpecialRail
			| RouteType::LorryTransportRail
			| RouteType::AllRail
			| RouteType::AdditionalRail => Mode::RegionalRail,

			RouteType::Bus
			| RouteType::RegionalBus
			| RouteType::ExpressBus
			| RouteType::StoppingBus
			| RouteType::LocalBus
			| RouteType::NightBus
			| RouteType::PostBus
			| RouteType::SpecialNeedsBus
			| RouteType::MobilityBus
			| RouteType::MobilityBusForRegisteredDisabled
			| RouteType::SightseeingBus
			| RouteType::ShuttleBus
			| RouteType::SchoolBus
			| RouteType::SchoolAndPublicServiceBus
			| RouteType::RailReplacementBus
			| RouteType::DemandAndResponseBus
			| RouteType::AllBus => Mode::Bus,

			RouteType::Ferry | RouteType::WaterTransport | RouteType::WaterTaxi => Mode::Ferry,

			RouteType::AerialLift
			| RouteType::Telecabin
			| RouteType::CableCar
			| RouteType::Elevator
			| RouteType::ChairLift
			| RouteType::DragLift
			| RouteType::SmallTelecabin
			| RouteType::AllTelecabin => Mode::Cablecar,

			RouteType::Funicular | RouteType::RackAndPinionRail => Mode::Funicular,

			RouteType::Trolleybus => Mode::TrolleyBus,

			RouteType::Monorail => Mode::Monorail,

			RouteType::HighSpeedRail
			| RouteType::LongDistanceRail
			| RouteType::InterRegionalRail
			| RouteType::CarTransportRail
			| RouteType::SleeperRail
			| RouteType::CrossCountryRail
			| RouteType::VehicleTransportRail => Mode::LongDistanceRail,

			RouteType::RailShuttle
			| RouteType::SururbanRail
			| RouteType::UrbanRailway
			| RouteType::UrbanRail
			| RouteType::AllUrbanRail => Mode::UrbanRail,

			RouteType::Coach
			| RouteType::InternationalCoach
			| RouteType::NationalCoach
			| RouteType::ShuttleCoach
			| RouteType::RegionalCoach
			| RouteType::SpecialCoach
			| RouteType::SightseeingCoach
			| RouteType::TouristCoach
			| RouteType::CommuterCoach
			| RouteType::AllCoach => Mode::Coach,

			RouteType::Air => Mode::Air,

			RouteType::Taxi
			| RouteType::CommunalTaxi
			| RouteType::RailTaxi
			| RouteType::BikeTaxi
			| RouteType::LicensedTaxi
			| RouteType::PrivateHireVehicle
			| RouteType::AllTaxi => Mode::Taxi,

			RouteType::Miscellaneous => return None,

			RouteType::HorseDrawnCarriage => Mode::HorseDrawnCarriage,
		})
	}
}
