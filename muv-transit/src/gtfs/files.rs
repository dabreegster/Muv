use std::num::{NonZeroIsize, NonZeroUsize};

use chrono_tz::Tz;
use serde::{de, Deserialize, Deserializer};

use crate::{
	gtfs::{
		deserialize_allowed, Color, CurrencyAmount, CurrencyCode, Email, LanguageCode, PhoneNumber,
		Url, Weekdays,
	},
	Time,
};

id!(AgencyId);
row! {Agency
	"agency_id" id: Option<AgencyId>,
	"agency_name" name: String,
	"agency_url" url: Url,
	"agency_timezone" timezone: Tz,
	"agency_lang" lang: Option<LanguageCode>,
	"agency_phone" phone: Option<PhoneNumber>,
	"agency_fare_url" fare_url: Option<Url>,
	"agency_email" email: Option<Email>,
}

id!(StopId);
row! {Stop
	"stop_id" id: StopId,
	"stop_code" code: Option<String>,
	"stop_name" name: Option<String>,
	"tts_stop_name" tts_name: Option<String>,
	"stop_desc" desc: Option<String>,
	"stop_lat" lat: Option<f64>,
	"stop_lon" lon: Option<f64>,
	zone_id: Option<ZoneId>,
	"stop_url" url: Option<Url>,
	#[serde(default)]
	location_type: LocationType,
	parent_station: Option<StopId>,
	"stop_timezone" timezone: Option<Tz>,
	#[serde(default, deserialize_with = "deserialize_allowed")]
	wheelchair_boarding: Option<bool>,
	level_id: Option<LevelId>,
	platform_code: Option<String>,
}
id!(ZoneId);

r#enum! {LocationType
	Stop! = 0 "0",
	Station = 1 "1",
	EntranceExit = 2 "2",
	GenericNode = 3 "3",
	BoardingArea = 4 "4",
}

id!(RouteId);
row! {Route
	"route_id" id: RouteId,
	agency_id: Option<AgencyId>,
	"route_short_name" short_name: Option<String>,
	"route_long_name" long_name: Option<String>,
	"route_desc" desc: Option<String>,
	"route_type" r#type: RouteType,
	"route_url" url: Option<Url>,
	"route_color" color: Option<Color>,
	"route_text_color" text_color: Option<Color>,
	"route_sort_order" sort_order: Option<usize>,
	#[serde(default = "PickupDropOff::no", deserialize_with = "PickupDropOff::deserialize_no")]
	continuous_pickup: PickupDropOff,
	#[serde(default = "PickupDropOff::no", deserialize_with = "PickupDropOff::deserialize_no")]
	continuous_drop_off: PickupDropOff,
	network_id: Option<NetworkId>,
}

r#enum! {RouteType
	Tram = 0 "0" | "900",
	Subway = 1 "1",
	Rail = 2 "2" | "100",
	Bus = 3 "3" | "700",
	Ferry = 4 "4" | "1200",
	CableTram = 5 "5",
	AerialLift = 6 "6" | "1300",
	Funicular = 7 "7" | "1400",
	Trolleybus = 11 "11" | "800",
	Monorail = 12 "12" | "405",

	HighSpeedRail = 101 "101",
	LongDistanceRail = 102 "102",
	InterRegionalRail = 103 "103",
	CarTransportRail = 104 "104",
	SleeperRail = 105 "105",
	RegionalRail = 106 "106",
	TouristRail = 107 "107",
	RailShuttle = 108 "108",
	SururbanRail = 109 "109",
	ReplacementRail = 110 "110",
	SpecialRail = 111 "111",
	LorryTransportRail = 112 "112",
	AllRail = 113 "113",
	CrossCountryRail = 114 "114",
	VehicleTransportRail = 115 "115",
	RackAndPinionRail = 116 "116",
	AdditionalRail = 117 "117",

	Coach = 200 "200",
	InternationalCoach = 201 "201",
	NationalCoach = 202 "202",
	ShuttleCoach = 203 "203",
	RegionalCoach = 204 "204",
	SpecialCoach = 205 "205",
	SightseeingCoach = 206 "206",
	TouristCoach = 207 "207",
	CommuterCoach = 208 "208",
	AllCoach = 209 "209",

	UrbanRailway = 400 "400",
	Metro = 401 "401",
	Underground = 402 "402",
	UrbanRail = 403 "403",
	AllUrbanRail = 404 "404",

	RegionalBus = 701 "701",
	ExpressBus = 702 "702",
	StoppingBus = 703 "703",
	LocalBus = 704 "704",
	NightBus = 705 "705",
	PostBus = 706 "706",
	SpecialNeedsBus = 707 "707",
	MobilityBus = 708 "708",
	MobilityBusForRegisteredDisabled = 709 "709",
	SightseeingBus = 710 "710",
	ShuttleBus = 711 "711",
	SchoolBus = 712 "712",
	SchoolAndPublicServiceBus = 713 "713",
	RailReplacementBus = 714 "714",
	DemandAndResponseBus = 715 "715",
	AllBus = 716 "716",

	CityTram = 901 "901",
	LocalTram = 902 "902",
	RegionalTram = 903 "903",
	SightseeingTram = 904 "904",
	ShuttleTram = 905 "905",
	AllTram = 906 "906",

	WaterTransport = 1000 "1000",

	Air = 1100 "1100",

	Telecabin = 1301 "1301",
	CableCar = 1302 "1302",
	Elevator = 1303 "1303",
	ChairLift = 1304 "1304",
	DragLift = 1305 "1305",
	SmallTelecabin = 1306 "1306",
	AllTelecabin = 1307 "1307",

	Taxi = 1500 "1500",
	CommunalTaxi = 1501 "1501",
	WaterTaxi = 1502 "1502",
	RailTaxi = 1503 "1503",
	BikeTaxi = 1504 "1504",
	LicensedTaxi = 1505 "1505",
	PrivateHireVehicle = 1506 "1506",
	AllTaxi = 1507 "1507",

	Miscellaneous = 1700 "1700",
	HorseDrawnCarriage = 1702 "1702",
}

r#enum! {PickupDropOff
	Yes = 0 "0",
	No = 1 "1",
	Phone = 2 "2",
	Coordinate = 3 "3",
}

impl PickupDropOff {
	const fn yes() -> Self {
		Self::Yes
	}
	const fn no() -> Self {
		Self::No
	}

	fn deserialize_yes<'de, D: Deserializer<'de>>(
		deserializer: D,
	) -> Result<PickupDropOff, D::Error> {
		Self::deserialize(deserializer, PickupDropOff::Yes)
	}

	fn deserialize_no<'de, D: Deserializer<'de>>(
		deserializer: D,
	) -> Result<PickupDropOff, D::Error> {
		Self::deserialize(deserializer, PickupDropOff::No)
	}

	fn deserialize<'de, D: Deserializer<'de>>(
		deserializer: D,
		default: PickupDropOff,
	) -> Result<PickupDropOff, D::Error> {
		let s: &str = Deserialize::deserialize(deserializer)?;
		match s {
			"" => Ok(default),
			"0" => Ok(PickupDropOff::Yes),
			"1" => Ok(PickupDropOff::No),
			"2" => Ok(PickupDropOff::Phone),
			"3" => Ok(PickupDropOff::Coordinate),
			_ => Err(de::Error::unknown_variant(s, &["0", "1", "2", "3"])),
		}
	}
}

id!(TripId);
row! {Trip
	route_id: RouteId,
	service_id: ServiceId,
	"trip_id" id: TripId,
	"trip_headsign" headsign: Option<String>,
	"trip_short_name" short_name: Option<String>,
	"direction_id" direction: Option<Direction>,
	block_id: Option<BlockId>,
	shape_id: Option<ShapeId>,
	#[serde(default, deserialize_with = "deserialize_allowed")]
	wheelchair_accessible: Option<bool>,
	#[serde(default, deserialize_with = "deserialize_allowed")]
	bikes_allowed: Option<bool>,
}
id!(BlockId);

r#enum! {Direction
	Forward = 0 "0",
	Backward = 1 "1",
}

row! {StopTime
	trip_id: TripId,
	arrival_time: Option<Time>,
	departure_time: Option<Time>,
	stop_id: Option<StopId>,
	location_group_id: Option<LocationGroupId>,
	location_id: Option<LocationId>,
	stop_sequence: usize,
	stop_headsign: Option<String>,
	start_pickup_drop_off_window: Option<Time>,
	end_pickup_drop_off_window: Option<Time>,
	#[serde(default = "PickupDropOff::yes", deserialize_with = "PickupDropOff::deserialize_yes")]
	pickup_type: PickupDropOff,
	#[serde(default = "PickupDropOff::yes", deserialize_with = "PickupDropOff::deserialize_yes")]
	drop_off_type: PickupDropOff,
	#[serde(default = "PickupDropOff::no", deserialize_with = "PickupDropOff::deserialize_no")]
	continuous_pickup: PickupDropOff,
	#[serde(default = "PickupDropOff::no", deserialize_with = "PickupDropOff::deserialize_no")]
	continuous_drop_off: PickupDropOff,
	shape_dist_traveled: Option<f64>,
	#[serde(default)]
	timepoint: Timepoint,
	pickup_booking_rule_id: Option<BookingRuleId>,
	drop_off_booking_rule_id: Option<BookingRuleId>,
}
id!(LocationId);

r#enum! {Timepoint
	Approximate = 0 "0",
	Exact! = 1 "1",
}

id!(ServiceId);
row! {Service
	"service_id" id: ServiceId,

	#[serde(flatten)]
	weekdays: Weekdays,

	start_date: Date,
	end_date: Date,
}

row! {ServiceDate
	service_id: ServiceId,
	date: Date,
	exception_type: ExceptionType,
}

r#enum! {ExceptionType
	Added = 1 "1",
	Removed = 2 "2",
}

id!(FareId);
row! {Fare
	"fare_id" id: FareId,
	price: f64,
	currency_type: CurrencyCode,
	payment_method: PaymentMethod,
	transfers: Option<usize>,
	agency_id: Option<AgencyId>,
	transfer_duration: Option<usize>,
}

r#enum! {PaymentMethod
	OnBoard = 0 "0",
	BeforeBoarding = 1 "1",
}

row! {FareRule
	fare_id: FareId,
	route_id: Option<RouteId>,
	origin_id: Option<ZoneId>,
	destination_id: Option<ZoneId>,
	contains_id: Option<ZoneId>,
}

id!(TimeframeGroupId);
row! {Timeframe
	"timeframe_group_id" group_id: TimeframeGroupId,
	start_time: Option<Time>,
	end_time: Option<Time>,
	service_id: ServiceId,
}

id!(FareMediaId);
row! {FareMedia
	"fare_media_id" id: FareMediaId,
	"fare_media_name" name: Option<String>,
	"fare_media_type" r#type: FareMediaType,
}

r#enum! {FareMediaType
	None = 0 "0",
	PaperTicket = 1 "1",
	TransitCard = 2 "2",
	Contactless = 3 "3",
	MobileApp = 4 "4",
}

id!(FareProductId);
row! {FareProduct
	"fare_product_id" id: FareProductId,
	"fare_product_name" name: Option<String>,
	fare_media_id: Option<FareMediaId>,
	amount: CurrencyAmount,
	currency: CurrencyCode,
}

row! {FareLegRule
	leg_group_id: Option<FareLegGroupId>,
	network_id: Option<NetworkId>,
	from_area_id: Option<AreaId>,
	to_area_id: Option<AreaId>,
	from_timeframe_group_id: Option<TimeframeGroupId>,
	to_timeframe_group_id: Option<TimeframeGroupId>,
	fare_product_id: FareProductId,
}
id!(FareLegGroupId);

row! {FareTransferRule
	from_leg_group_id: Option<FareLegGroupId>,
	to_leg_group_id: Option<FareLegGroupId>,
	transfer_count: Option<NonZeroIsize>,
	duration_limit: Option<NonZeroUsize>,
	duration_limit_type: Option<DurationLimitType>,
	// fare_transfer_type
	fare_product_id: Option<FareProductId>,
}

r#enum! {DurationLimitType
	DepartureArrival = 0 "0",
	DepartureDeparture = 1 "1",
	ArrivalDeparture = 2 "2",
	ArrivalArrival = 3 "3",
}

id!(AreaId);
row! {Area
	"area_id" id: AreaId,
	"area_name" name: Option<String>,
}

row! {StopArea
	area_id: AreaId,
	stop_id: StopId,
}

id!(NetworkId);
row! {Network
	"network_id" id: NetworkId,
	"network_name" name: Option<String>,
}

row! {RouteNetwork
	network_id: NetworkId,
	route_id: RouteId,
}

id!(ShapeId);
row! {Shape
	"shape_id" id: ShapeId,
	"shape_pt_lat" point_lat: f64,
	"shape_pt_lon" point_lon: f64,
	"shape_pt_sequence" point_sequence: usize,
	"shape_dist_traveled" dist_traveled: Option<f64>,
}

row! {Frequency
	trip_id: TripId,
	start_time: Time,
	end_time: Time,
	headway_secs: NonZeroUsize,
	#[serde(default)]
	exact_times: ServiceType,
}

r#enum! {ServiceType
	FrequencyBased! = 0 "0",
	ScheduleBased = 1 "1",
}

row! {Transfer
	from_stop_id: Option<StopId>,
	to_stop_id: Option<StopId>,
	from_route_id: Option<RouteId>,
	to_route_id: Option<RouteId>,
	from_trip_id: Option<TripId>,
	to_trip_id: Option<TripId>,
	#[serde(default)]
	"transfer_type" r#type: TransferType,
	"min_transfer_time" min_time: Option<usize>,
}

r#enum! {TransferType
	Recommended! = 0 "0",
	Timed = 1 "1",
	MinimumTime = 2 "2",
	NotPossible = 3 "3",
	Linked = 4 "4",
	LinkedReboard = 5 "5",
}

id!(PathwayId);
row! {Pathway
	"pathway_id" id: PathwayId,
	from_stop_id: StopId,
	to_stop_id: StopId,
	"pathway_mode" mode: PathwayMode,
	is_bidirectional: bool,
	length: Option<f64>,
	traversal_time: Option<NonZeroUsize>,
	stair_count: Option<usize>,
	max_slope: Option<f64>,
	min_width: Option<f64>,
	signposted_as: Option<String>,
	reversed_signposted_as: Option<String>,
}

r#enum! {PathwayMode
	Walkway = 1 "1",
	Stairs = 2 "2",
	Travelator = 3 "3",
	Escalator = 4 "4",
	Elevator = 5 "5",
	FareGate = 6 "6",
	ExitGate = 7 "7",
}

id!(LevelId);
row! {Level
	"level_id" id: LevelId,
	"level_index" index: f64,
	"level_name" name: Option<String>,
}

id!(LocationGroupId);
row! {LocationGroup
	"location_group_id" id: LocationGroupId,
	"location_group_name" name: Option<String>,
}

row! {LocationGroupStop
	location_group_id: LocationGroupId,
	stop_id: StopId,
}

id!(BookingRuleId);
row! {BookingRule
	"booking_rule_id" id: BookingRuleId,
	booking_type: BookingType,
	prior_notice_duration_min: Option<isize>,
	prior_notice_duration_max: Option<isize>,
	prior_notice_last_day: Option<isize>,
	prior_notice_last_time: Option<Time>,
	prior_notice_start_day: Option<isize>,
	prior_notice_start_time: Option<Time>,
	prior_notice_service_id: Option<ServiceId>,
	message: Option<String>,
	pickup_message: Option<String>,
	drop_off_message: Option<String>,
	phone_number: Option<PhoneNumber>,
	info_url: Option<Url>,
	booking_url: Option<Url>,
}

r#enum! {BookingType
	RealTime = 0 "0",
	SameDay = 1 "1",
	PriorDays = 2 "2",
}

row! {Translation
	table_name: TableName,
	field_name: String,
	language: LanguageCode,
	translation: String,
	record_id: Option<String>,
	record_sub_id: Option<String>,
	field_value: Option<String>,
}

r#enum! {
#[serde(rename_all = "snake_case")]
TableName
	Agency,
	Stops,
	Routes,
	Trips,
	StopTimes,
	Pathways,
	Levels,
	FeedInfo,
	Attributions,
}

row! {FeedInfo
	"feed_publisher_name" publisher_name: String,
	"feed_publisher_url" publisher_url: Url,
	"feed_lang" lang: LanguageCode,
	default_lang: Option<LanguageCode>,
	"feed_start_date" start_date: Option<Date>,
	"feed_end_date" end_date: Option<Date>,
	"feed_version" version: Option<String>,
	"feed_contact_email" contact_email: Option<Email>,
	"feed_contact_url" contact_url: Option<Url>,
}

id!(AttributionId);
row! {Attribution
	"attribution_id" id: Option<AttributionId>,
	agency_id: Option<AgencyId>,
	route_id: Option<RouteId>,
	trip_id: Option<TripId>,
	organization_name: String,
	#[serde(default)]
	is_producer: bool,
	#[serde(default)]
	is_operator: bool,
	#[serde(default)]
	is_authority: bool,
	"attribution_url" url: Option<Url>,
	"attribution_email" email: Option<Email>,
	"attribution_phone" phone: Option<PhoneNumber>,
}
