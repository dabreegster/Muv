use chrono::{NaiveDate, Weekday};
use serde::{
	de::{self, Unexpected},
	Deserialize, Deserializer, Serialize, Serializer,
};

macro_rules! id {
	($name:ident) => {
		pub type $name = String;
	};
}

macro_rules! row {
	($name:ident $(($($body:tt)*))? ) => {
		#[derive(Debug, Clone, PartialEq, serde::Serialize, serde::Deserialize)]
		pub struct $name {
            $($($body)*)?
        }
	};
    ($name:ident $(($($body:tt)*))? $(#[$($attr:tt)*])* $($rename:literal)? $key:ident : bool, $($rest:tt)* ) => {
        row! {$name (
            $($($body)*)?
            $(#[$($attr)*])*
            $(#[serde(rename = $rename)])?
            #[serde(
	            serialize_with = "crate::gtfs::serialize_bool",
	            deserialize_with = "crate::gtfs::deserialize_bool"
            )]
            pub $key: bool,
        ) $($rest)* }
	};
    ($name:ident $(($($body:tt)*))? $(#[$($attr:tt)*])* $($rename:literal)? $key:ident : Option<Date>, $($rest:tt)* ) => {
        row! {$name (
            $($($body)*)?
            $(#[$($attr)*])*
            $(#[serde(rename = $rename)])?
            #[serde(
	            serialize_with = "crate::gtfs::serialize_option_date",
	            deserialize_with = "crate::gtfs::deserialize_option_date"
            )]
            pub $key: Option<chrono::NaiveDate>,
        ) $($rest)* }
	};
    ($name:ident $(($($body:tt)*))? $(#[$($attr:tt)*])* $($rename:literal)? $key:ident : Date, $($rest:tt)* ) => {
        row! {$name (
            $($($body)*)?
            $(#[$($attr)*])*
            $(#[serde(rename = $rename)])?
            #[serde(
	            serialize_with = "crate::gtfs::serialize_date",
	            deserialize_with = "crate::gtfs::deserialize_date"
            )]
            pub $key: chrono::NaiveDate,
        ) $($rest)* }
	};
    ($name:ident $(($($body:tt)*))? $(#[$($attr:tt)*])* $($rename:literal)? $key:ident : $type:path, $($rest:tt)* ) => {
        row! {$name (
            $($($body)*)?
            $(#[$($attr)*])*
            $(#[serde(rename = $rename)])?
            pub $key: $type,
        ) $($rest)* }
	};
}

macro_rules! r#enum {
	($(#[$($attr:tt)*])* $name:ident $(($($body:tt)*))? ) => {
        #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
        $(#[$($attr)*])*
        pub enum $name {
            $($($body)*)?
        }
    };
	($(#[$($attr:tt)*])* $name:ident $(($($body:tt)*))? $(#[$($vattr:tt)*])* $variant:ident! = $value:literal $valuestr:literal, $($rest:tt)* ) => {
        r#enum! { $(#[$($attr)*])* $name (
            $($($body)*)?
            $(#[$($vattr)*])*
            #[serde(rename = $valuestr, alias = "")]
            $variant = $value,
        ) $($rest)* }

        impl Default for $name {
            fn default() -> Self {
                Self::$variant
            }
        }
    };
    ($(#[$($attr:tt)*])* $name:ident $(($($body:tt)*))? $(#[$($vattr:tt)*])* $variant:ident, $($rest:tt)* ) => {
        r#enum! { $(#[$($attr)*])* $name (
            $($($body)*)?
            $(#[$($vattr)*])*
            $variant,
        ) $($rest)* }
    };
    ($(#[$($attr:tt)*])* $name:ident $(($($body:tt)*))? $(#[$($vattr:tt)*])* $variant:ident = $value:literal $valuestr:literal $(| $alias:literal)* , $($rest:tt)* ) => {
        r#enum! { $(#[$($attr)*])* $name (
            $($($body)*)?
            $(#[$($vattr)*])*
            #[serde(rename = $valuestr, $(alias = $alias),* )]
            $variant = $value,
        ) $($rest)* }
    };
}

pub fn serialize_bool<S: Serializer>(value: &bool, serializer: S) -> Result<S::Ok, S::Error> {
	serializer.serialize_u8((*value).into())
}

pub fn deserialize_bool<'de, D: Deserializer<'de>>(deserializer: D) -> Result<bool, D::Error> {
	let n = u8::deserialize(deserializer)?;
	match n {
		1 => Ok(true),
		0 => Ok(false),
		_ => Err(de::Error::invalid_value(
			Unexpected::Unsigned(n.into()),
			&"1 or 0",
		)),
	}
}

pub fn deserialize_allowed<'de, D: Deserializer<'de>>(
	deserializer: D,
) -> Result<Option<bool>, D::Error> {
	let s: &str = Deserialize::deserialize(deserializer)?;
	match s {
		"" | "0" => Ok(None),
		"1" => Ok(Some(true)),
		"2" => Ok(Some(false)),
		_ => Err(de::Error::unknown_variant(s, &["0", "1", "2"])),
	}
}

pub type Color = String;
pub type CurrencyCode = String;
pub type CurrencyAmount = String;
pub type Email = String;
pub type LanguageCode = String;
pub type PhoneNumber = String;
pub type Url = String;

pub fn deserialize_date<'de, D: Deserializer<'de>>(deserializer: D) -> Result<NaiveDate, D::Error> {
	let s: &str = Deserialize::deserialize(deserializer)?;
	NaiveDate::parse_from_str(s, "%Y%m%d").map_err(de::Error::custom)
}

pub fn serialize_date<S: Serializer>(d: &NaiveDate, serializer: S) -> Result<S::Ok, S::Error> {
	d.format("%Y%m%d").to_string().serialize(serializer)
}

pub fn deserialize_option_date<'de, D: Deserializer<'de>>(
	deserializer: D,
) -> Result<Option<NaiveDate>, D::Error> {
	let s: &str = Deserialize::deserialize(deserializer)?;
	if s.is_empty() {
		Ok(None)
	} else {
		NaiveDate::parse_from_str(s, "%Y%m%d")
			.map_err(de::Error::custom)
			.map(Some)
	}
}

pub fn serialize_option_date<S: Serializer>(
	d: &Option<NaiveDate>,
	serializer: S,
) -> Result<S::Ok, S::Error> {
	d.map(|d| d.format("%Y%m%d").to_string())
		.serialize(serializer)
}

row! {Weekdays
	monday: bool,
	tuesday: bool,
	wednesday: bool,
	thursday: bool,
	friday: bool,
	saturday: bool,
	sunday: bool,
}

impl Copy for Weekdays {}

impl Weekdays {
	pub fn get(self, w: Weekday) -> bool {
		match w {
			Weekday::Mon => self.monday,
			Weekday::Tue => self.tuesday,
			Weekday::Wed => self.wednesday,
			Weekday::Thu => self.thursday,
			Weekday::Fri => self.friday,
			Weekday::Sat => self.saturday,
			Weekday::Sun => self.sunday,
		}
	}
}
