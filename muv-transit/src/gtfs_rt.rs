use std::vec::IntoIter;

use gtfs_rt::VehiclePosition;
use structure::FeedEntity;

use crate::{Id, Time, Vehicle};

pub use gtfs_rt as structure;

pub struct FeedMessage(pub structure::FeedMessage);

fn convert(network: &'static str, v: VehiclePosition) -> Vehicle {
	let (id, label) = match v.vehicle {
		Some(v) => (v.id, v.label),
		None => (None, None),
	};
	let (location, speed) = match v.position {
		Some(p) => (Some((p.latitude.into(), p.longitude.into())), p.speed),
		None => (None, None),
	};

	let id = id.unwrap_or_else(|| {
		let t = v.trip.as_ref().unwrap();
		let trip_id = t.trip_id.as_ref().unwrap();
		if let Some(start_time) = &t.start_time {
			format!("{}-{}", trip_id, start_time)
		} else {
			trip_id.to_owned()
		}
	});

	let trip = v.trip.map(|t| crate::TripSelector {
		line: t.route_id,
		route: t.trip_id,
		start_time: t.start_time.as_deref().and_then(Time::parse),
	});

	Vehicle {
		id: Id::new(network, id),
		code: label,
		line_name: None,
		trip,

		location,
		heading: None,
		speed: speed.map(|s| f64::from(s) * 3.6),
	}
}

pub struct IntoVehicles {
	pub network: &'static str,
	iter: IntoIter<FeedEntity>,
}

impl Iterator for IntoVehicles {
	type Item = Vehicle;

	fn next(&mut self) -> Option<Self::Item> {
		loop {
			let e = self.iter.next()?;
			if let Some(v) = e.vehicle {
				return Some(convert(self.network, v));
			}
		}
	}

	fn size_hint(&self) -> (usize, Option<usize>) {
		(0, Some(self.iter.len()))
	}
}

impl FeedMessage {
	pub fn vehicles(self, network: &'static str) -> IntoVehicles {
		IntoVehicles {
			network,
			iter: self.0.entity.into_iter(),
		}
	}
}
