mod crypto;
use std::{iter::Map, vec::IntoIter};

pub use crypto::*;

mod data;
pub use data::*;

use crate::{Id, Vehicle};

mod fetch;

impl From<Feature> for Vehicle {
	fn from(value: Feature) -> Self {
		let loc = value.geometry.coordinates;
		let props = value.properties;

		Self {
			id: Id::new("amtrak", props.gx_id),
			code: Some(props.train_num),
			line_name: Some(props.route_name),
			trip: None,

			location: Some((loc.1, loc.0)),
			heading: props.heading.map(|h| u16::from(h).into()),
			speed: props.velocity.map(|v| v * 1.609344),
		}
	}
}

pub type IntoVehicles = Map<IntoIter<Feature>, fn(Feature) -> Vehicle>;

impl TrainsData {
	pub fn vehicles(self) -> IntoVehicles {
		self.features.into_iter().map(Into::into)
	}
}
