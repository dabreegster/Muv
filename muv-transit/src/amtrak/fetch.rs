use std::time::Duration;

use crate::amtrak::TrainsData;

#[cfg(any(feature = "ureq", feature = "reqwest"))]
use crate::amtrak::{decrypt_body, Error};

impl TrainsData {
	pub const URL: &'static str =
		"https://maps.amtrak.com/services/MapDataService/trains/getTrainsData";

	pub const UPDATE_CYCLE: Duration = Duration::from_secs(3 * 60);

	#[cfg(feature = "ureq")]
	pub fn fetch_ureq() -> Result<Self, Error> {
		let mut body = Vec::new();
		ureq::get(Self::URL)
			.call()?
			.into_reader()
			.read_to_end(&mut body)?;

		let content = decrypt_body(&mut body)?;
		Ok(serde_json::from_slice(content)?)
	}

	#[cfg(feature = "reqwest")]
	pub async fn fetch_reqwest() -> Result<Self, Error> {
		let mut body: Vec<_> = reqwest::get(Self::URL)
			.await?
			.bytes()
			.await?
			.into_iter()
			.collect();
		let content = decrypt_body(&mut body)?;
		Ok(serde_json::from_slice(content)?)
	}
}
