use chrono::NaiveDateTime;
use chrono_tz::{Tz, US};
use serde::{
	de::{self, MapAccess, Visitor},
	Deserialize, Deserializer,
};

use std::{
	collections::BTreeMap,
	fmt::{self, Formatter},
};

#[derive(Debug, Deserialize)]
pub struct TrainsData {
	pub features: Vec<Feature>,
}

#[derive(Debug, Deserialize)]
pub struct Feature {
	pub geometry: Geometry,
	pub properties: Properties,
}

#[derive(Debug, Deserialize)]
pub struct Geometry {
	pub coordinates: (f64, f64),
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Properties {
	#[serde(rename = "gx_id")]
	pub gx_id: String,
	pub status_msg: String,
	pub heading: Option<Heading>,
	#[serde(rename = "LastValTS", deserialize_with = "deserialize_datetime_am")]
	pub last_val_ts: NaiveDateTime,
	pub event_code: Option<String>,
	pub dest_code: String,
	pub orig_code: String,
	pub route_name: String,
	#[serde(rename = "OriginTZ")]
	pub origin_tz: Timezone,
	#[serde(deserialize_with = "deserialize_datetime_am")]
	pub orig_sch_dep: NaiveDateTime,

	#[serde(rename = "CMSID")]
	pub cmsid: String,
	#[serde(rename = "ID")]
	pub id: u32,
	pub train_num: String,
	#[serde(deserialize_with = "deserialize_velocity")]
	pub velocity: Option<f64>,

	#[serde(flatten, deserialize_with = "deserialize_stations")]
	pub stations: BTreeMap<u8, Station>,
}

fn deserialize_velocity<'de, D: Deserializer<'de>>(
	deserializer: D,
) -> Result<Option<f64>, D::Error> {
	if let Some(s) = Option::<String>::deserialize(deserializer)? {
		s.parse().map(Some).map_err(de::Error::custom)
	} else {
		Ok(None)
	}
}

#[derive(Debug, Deserialize)]
pub struct Station {
	pub code: String,
	pub tz: Timezone,
	pub bus: bool,

	#[serde(
		rename = "scharr",
		default,
		deserialize_with = "deserialize_optional_datetime"
	)]
	pub sch_arr: Option<NaiveDateTime>,
	#[serde(
		rename = "schdep",
		default,
		deserialize_with = "deserialize_optional_datetime"
	)]
	pub sch_dep: Option<NaiveDateTime>,
	#[serde(rename = "schcmnt")]
	pub sch_cmnt: String,

	#[serde(rename = "autoarr")]
	pub auto_arr: bool,
	#[serde(rename = "autodep")]
	pub auto_dep: bool,

	#[serde(
		rename = "estarr",
		default,
		deserialize_with = "deserialize_optional_datetime"
	)]
	pub est_arr: Option<NaiveDateTime>,
	#[serde(
		rename = "estdep",
		default,
		deserialize_with = "deserialize_optional_datetime"
	)]
	pub est_dep: Option<NaiveDateTime>,
	#[serde(rename = "estarrcmnt")]
	pub est_arr_cmnt: Option<String>,
	#[serde(rename = "estdepcmnt")]
	pub est_dep_cmnt: Option<String>,

	#[serde(
		rename = "postarr",
		default,
		deserialize_with = "deserialize_optional_datetime"
	)]
	pub post_arr: Option<NaiveDateTime>,
	#[serde(
		rename = "postdep",
		default,
		deserialize_with = "deserialize_optional_datetime"
	)]
	pub post_dep: Option<NaiveDateTime>,
	#[serde(rename = "postcmnt")]
	pub post_cmnt: Option<String>,
}

struct StationsVisitor;

impl<'de> Visitor<'de> for StationsVisitor {
	type Value = BTreeMap<u8, Station>;

	fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
		formatter.write_str("map keys starting with 'Station'")
	}

	fn visit_map<A: MapAccess<'de>>(self, mut map: A) -> Result<Self::Value, A::Error> {
		let mut res = BTreeMap::new();
		while let Some(key) = map.next_key::<String>()? {
			let Some(key) = key.strip_prefix("Station") else {
				continue;
			};
			let id: u8 = key.parse().map_err(de::Error::custom)?;

			let Some(val) = map.next_value::<Option<String>>()? else {
				continue;
			};
			let station: Station = serde_json::from_str(&val).map_err(de::Error::custom)?;

			res.insert(id, station);
		}
		Ok(res)
	}
}

fn deserialize_stations<'de, D: Deserializer<'de>>(
	deserializer: D,
) -> Result<BTreeMap<u8, Station>, D::Error> {
	deserializer.deserialize_map(StationsVisitor)
}

fn deserialize_optional_datetime<'de, D: Deserializer<'de>>(
	deserializer: D,
) -> Result<Option<NaiveDateTime>, D::Error> {
	if let Some(s) = Deserialize::deserialize(deserializer)? {
		NaiveDateTime::parse_from_str(s, "%m/%d/%Y %H:%M:%S")
			.map(Some)
			.map_err(de::Error::custom)
	} else {
		Ok(None)
	}
}

fn deserialize_datetime_am<'de, D: Deserializer<'de>>(
	deserializer: D,
) -> Result<NaiveDateTime, D::Error> {
	let s: &str = Deserialize::deserialize(deserializer)?;
	NaiveDateTime::parse_from_str(s, "%_m/%_d/%Y %_I:%M:%S %p").map_err(de::Error::custom)
}

#[derive(Debug, Deserialize)]
pub enum Heading {
	#[serde(rename = "NW")]
	NorthWest,
	#[serde(rename = "N")]
	North,
	#[serde(rename = "NE")]
	NorthEast,
	#[serde(rename = "E")]
	East,
	#[serde(rename = "SE")]
	SouthEast,
	#[serde(rename = "S")]
	South,
	#[serde(rename = "SW")]
	SouthWest,
	#[serde(rename = "W")]
	West,
}

impl From<Heading> for u16 {
	fn from(value: Heading) -> Self {
		match value {
			Heading::North => 0,
			Heading::NorthEast => 45,
			Heading::East => 90,
			Heading::SouthEast => 135,
			Heading::South => 180,
			Heading::SouthWest => 225,
			Heading::West => 270,
			Heading::NorthWest => 315,
		}
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
pub enum Timezone {
	#[serde(rename = "P")]
	Pacific,
	#[serde(rename = "M")]
	Mountain,
	#[serde(rename = "C")]
	Central,
	#[serde(rename = "E")]
	Eastern,
}

impl From<Timezone> for Tz {
	fn from(value: Timezone) -> Self {
		match value {
			Timezone::Pacific => US::Pacific,
			Timezone::Mountain => US::Mountain,
			Timezone::Central => US::Central,
			Timezone::Eastern => US::Eastern,
		}
	}
}
