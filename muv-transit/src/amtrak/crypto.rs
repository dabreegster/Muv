use aes::{
	cipher::{
		block_padding::{Pkcs7, UnpadError},
		BlockDecryptMut, KeyIvInit,
	},
	Aes128,
};
use base64::{engine::general_purpose::STANDARD, DecodeError, Engine};
use cbc::Decryptor;
use pbkdf2::pbkdf2_hmac_array;
use sha1::Sha1;

use std::{
	error,
	fmt::{self, Display, Formatter},
	io,
};

const MASTER_SEGMENT: usize = 88;
const PUBLIC_KEY: &str = "69af143c-e8cf-47f8-bf09-fc1f61e5cc33";
const GUID_LEN: usize = 36;

const SALT: [u8; 4] = [0x9a, 0x36, 0x86, 0xac];
const ITERATIONS: u32 = 1000;
const KEY_LEN: usize = 16;

const IV: [u8; 16] = [
	0xc6, 0xeb, 0x2f, 0x7f, 0x5c, 0x47, 0x40, 0xc1, 0xa2, 0xf7, 0x08, 0xfe, 0xfd, 0x94, 0x7d, 0x39,
];

pub fn decrypt_body(body: &mut [u8]) -> Result<&mut [u8], Error> {
	let (content, private_key_hash) = body.split_at_mut(body.len() - MASTER_SEGMENT);

	let private_key = decrypt(private_key_hash, PUBLIC_KEY.as_bytes())?;
	let private_key = &private_key[..GUID_LEN];

	let content = decrypt(content, private_key)?;
	Ok(content)
}

pub fn decrypt<'a>(content: &'a mut [u8], password: &[u8]) -> Result<&'a mut [u8], Error> {
	let mut raw_content = STANDARD.decode(&content)?; // TODO: In place

	let key = pbkdf2_hmac_array::<Sha1, KEY_LEN>(password, &SALT, ITERATIONS);

	let decryptor = Decryptor::<Aes128>::new(&key.into(), &IV.into());
	let decrypted_content = decryptor.decrypt_padded_mut::<Pkcs7>(&mut raw_content)?;

	let content = &mut content[..decrypted_content.len()];
	content.copy_from_slice(decrypted_content);

	Ok(content)
}

#[derive(Debug)]
#[non_exhaustive]
pub enum Error {
	Base64(DecodeError),
	Aes(UnpadError),
	Io(io::Error),
	SerdeJson(serde_json::Error),
	#[cfg(feature = "ureq")]
	Ureq(Box<ureq::Error>),
	#[cfg(feature = "reqwest")]
	Reqwest(reqwest::Error),
}

impl From<DecodeError> for Error {
	fn from(value: DecodeError) -> Self {
		Self::Base64(value)
	}
}

impl From<UnpadError> for Error {
	fn from(value: UnpadError) -> Self {
		Self::Aes(value)
	}
}

impl From<io::Error> for Error {
	fn from(value: io::Error) -> Self {
		Self::Io(value)
	}
}

impl From<serde_json::Error> for Error {
	fn from(value: serde_json::Error) -> Self {
		Self::SerdeJson(value)
	}
}

#[cfg(feature = "ureq")]
impl From<ureq::Error> for Error {
	fn from(value: ureq::Error) -> Self {
		Self::Ureq(Box::new(value))
	}
}

#[cfg(feature = "reqwest")]
impl From<reqwest::Error> for Error {
	fn from(value: reqwest::Error) -> Self {
		Self::Reqwest(value)
	}
}

impl Display for Error {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::Base64(e) => write!(f, "failed to decode base64: {e}"),
			Self::Aes(_) => write!(f, "invalid padding in AES data"),
			Self::Io(e) => write!(f, "{e}"),
			Self::SerdeJson(e) => write!(f, "failed to parse JSON because of {e}"),
			#[cfg(feature = "ureq")]
			Self::Ureq(e) => write!(f, "{e}"),
			#[cfg(feature = "reqwest")]
			Self::Reqwest(e) => write!(f, "{e}"),
		}
	}
}

impl error::Error for Error {}
