use std::{
	collections::HashMap,
	fmt::{self, Debug, Display, Formatter},
};

#[cfg(not(any(feature = "gtfs", feature = "netex")))]
use std::marker::PhantomData;

use chrono::{DateTime, NaiveDate, NaiveTime, TimeDelta, TimeZone};

#[cfg(feature = "gtfs")]
use chrono::Datelike;
#[cfg(feature = "gtfs")]
use std::collections::HashSet;

#[cfg(feature = "serde")]
use serde::{de, Deserialize, Deserializer, Serialize, Serializer};

#[cfg(feature = "amtrak")]
pub mod amtrak;
#[cfg(feature = "gtfs")]
pub mod gtfs;
#[cfg(feature = "gtfs-rt")]
pub mod gtfs_rt;
#[cfg(feature = "netex")]
pub mod netex;
#[cfg(feature = "viarail")]
pub mod viarail;

#[derive(Debug, Default, Clone, PartialEq)]
pub struct Network {
	pub organizations: HashMap<OrganizationId, Organization>,
	pub stops: HashMap<StopId, Stop>,
	pub routes: HashMap<RouteId, Route>,
	pub services: HashMap<ServiceId, Service>,
	pub lines: HashMap<LineId, Line>,
	pub shapes: HashMap<ShapeId, Vec<Location>>,
}

pub type Location = (f64, f64);

pub type OrganizationId = String;
#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Organization {
	pub name: Option<String>,
	pub url: Option<String>,
	pub email: Option<String>,
	pub phone: Option<String>,
}

pub type StopId = String;
#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Stop {
	pub name: Option<String>,
	pub code: Option<String>,
	pub r#ref: Option<String>,
	pub ifopt: Option<String>,

	pub location: Option<Location>,

	pub parent_stop: Option<StopId>,
}

pub type LineId = String;
#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Line {
	pub authority: Option<OrganizationId>,
	pub operators: Vec<OrganizationId>,
	pub mode: Option<Mode>,
	pub name: String,
	pub color: Option<String>,
	pub text_color: Option<String>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum Mode {
	Bus,
	TrolleyBus,
	Coach,

	Tram,
	Metro,

	UrbanRail,
	RegionalRail,
	LongDistanceRail,

	Monorail,
	Cablecar,
	Funicular,
	HorseDrawnCarriage,
	Ferry,
	Taxi,
	Air,
}

pub type RouteId = String;
#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Route {
	pub service: ServiceId,
	pub line: Option<LineId>,
	pub shape: Option<ShapeId>,
	pub stops: Vec<RouteStop>,
}

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct RouteStop {
	pub id: StopId,

	pub arrival: Option<Time>,
	pub departure: Option<Time>,

	pub board: bool,
	pub alight: bool,
}

pub type ServiceId = String;
#[non_exhaustive]
#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(Deserialize))]
pub enum Service {
	#[cfg(feature = "gtfs")]
	Gtfs {
		start_date: NaiveDate,
		end_date: NaiveDate,
		weekdays: gtfs::Weekdays,

		added: HashSet<NaiveDate>,
		removed: HashSet<NaiveDate>,
	},
	#[cfg(feature = "netex")]
	Netex {
		from_date: NaiveDate,
		to_date: Option<NaiveDate>,
		valid_days: netex::DayBits,
	},
}

impl Service {
	pub fn allowed(&self, date: NaiveDate) -> bool {
		#[cfg(any(feature = "gtfs", feature = "netex"))]
		match self {
			#[cfg(feature = "gtfs")]
			Self::Gtfs {
				start_date,
				end_date,
				weekdays,

				added,
				removed,
			} => {
				if date < *start_date || date > *end_date {
					return false;
				}

				let mut allowed = weekdays.get(start_date.weekday());

				let exceptions = if allowed { &removed } else { &added };
				if exceptions.contains(start_date) {
					allowed = !allowed;
				}

				allowed
			}
			#[cfg(feature = "netex")]
			Self::Netex {
				from_date,
				to_date: _,
				valid_days,
			} => {
				let days = date.signed_duration_since(*from_date).num_days();
				days.try_into().is_ok_and(|i| valid_days.contains(i))
			}
		}
		#[cfg(not(any(feature = "gtfs", feature = "netex")))]
		{
			let _ = date;
			false
		}
	}
}

impl<'a> IntoIterator for &'a Service {
	type Item = NaiveDate;
	type IntoIter = ServiceIter<'a>;

	fn into_iter(self) -> Self::IntoIter {
		#[cfg(any(feature = "gtfs", feature = "netex"))]
		match self {
			#[cfg(feature = "gtfs")]
			Service::Gtfs {
				start_date,
				end_date,
				weekdays,
				added,
				removed,
			} => ServiceIter {
				inner: InnerServiceIter::Gtfs {
					start_date: *start_date,
					end_date: *end_date,
					weekdays: *weekdays,
					added,
					removed,
				},
			},
			#[cfg(feature = "netex")]
			Service::Netex {
				from_date,
				to_date,
				valid_days,
			} => ServiceIter {
				inner: InnerServiceIter::Netex {
					from_date: *from_date,
					to_date: *to_date,
					valid_days_iter: valid_days.into_iter(),
				},
			},
		}
		#[cfg(not(any(feature = "gtfs", feature = "netex")))]
		ServiceIter {
			phantom: PhantomData,
		}
	}
}

#[derive(Debug)]
pub struct ServiceIter<'a> {
	#[cfg(any(feature = "gtfs", feature = "netex"))]
	inner: InnerServiceIter<'a>,
	#[cfg(not(any(feature = "gtfs", feature = "netex")))]
	phantom: PhantomData<&'a ()>,
}

#[derive(Debug)]
#[cfg(any(feature = "gtfs", feature = "netex"))]
enum InnerServiceIter<'a> {
	#[cfg(feature = "gtfs")]
	Gtfs {
		start_date: NaiveDate,
		end_date: NaiveDate,
		weekdays: gtfs::Weekdays,

		added: &'a HashSet<NaiveDate>,
		removed: &'a HashSet<NaiveDate>,
	},
	#[cfg(feature = "netex")]
	Netex {
		from_date: NaiveDate,
		to_date: Option<NaiveDate>,

		valid_days_iter: netex::IterDayBits<'a>,
	},
}

impl Iterator for ServiceIter<'_> {
	type Item = NaiveDate;

	fn next(&mut self) -> Option<Self::Item> {
		#[cfg(any(feature = "gtfs", feature = "netex"))]
		match &mut self.inner {
			#[cfg(feature = "gtfs")]
			InnerServiceIter::Gtfs {
				start_date,
				end_date,
				weekdays,

				added,
				removed,
			} => {
				while start_date <= end_date {
					let mut allowed = weekdays.get(start_date.weekday());

					let exceptions = if allowed { &removed } else { &added };
					if exceptions.contains(start_date) {
						allowed = !allowed;
					}

					let now = *start_date;
					*start_date = now.succ_opt().unwrap();
					if allowed {
						return Some(now);
					}
				}
			}
			#[cfg(feature = "netex")]
			InnerServiceIter::Netex {
				from_date,
				to_date,
				valid_days_iter,
			} => {
				while to_date.map_or(true, |end_date| *from_date <= end_date) {
					if valid_days_iter.next()? {
						return Some(*from_date);
					}

					*from_date = from_date.succ_opt().unwrap();
				}
			}
		};
		None
	}

	fn size_hint(&self) -> (usize, Option<usize>) {
		#[cfg(any(feature = "gtfs", feature = "netex"))]
		match self.inner {
			#[cfg(feature = "gtfs")]
			InnerServiceIter::Gtfs {
				start_date,
				end_date,
				..
			} => {
				let max = end_date.signed_duration_since(start_date).num_days();
				(0, Some(max as usize))
			}
			#[cfg(feature = "netex")]
			InnerServiceIter::Netex { .. } => {
				(0, None) // TODO
			}
		}
		#[cfg(not(any(feature = "gtfs", feature = "netex")))]
		(0, None)
	}
}

pub type ShapeId = String;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Id {
	pub namespace: &'static str,
	pub id: String,
}

impl Id {
	pub const fn new(namespace: &'static str, id: String) -> Self {
		Self { namespace, id }
	}
}

impl Display for Id {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(f, "{}:{}", self.namespace, self.id)
	}
}

#[cfg(feature = "serde")]
impl Serialize for Id {
	fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
		serializer.serialize_str(&self.to_string())
	}
}

pub type VehicleId = Id;
#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize))]
pub struct Vehicle {
	pub id: VehicleId,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
	pub code: Option<String>,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
	pub trip: Option<TripSelector>,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
	pub line_name: Option<String>,

	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
	pub location: Option<Location>,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
	pub heading: Option<f64>,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
	pub speed: Option<f64>,
}

#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct TripSelector {
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
	pub line: Option<LineId>,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
	pub route: Option<RouteId>,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
	pub start_time: Option<Time>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Time {
	pub hours: u8,
	pub minutes: u8,
	pub seconds: u8,
}

#[cfg(any(feature = "serde", feature = "gtfs-rt"))]
impl Time {
	pub(crate) fn parse(s: &str) -> Option<Self> {
		let mut i = s.splitn(3, ':');

		let hours = i.next()?.parse().ok()?;
		let minutes = i.next()?.parse().ok()?;
		let seconds = i.next()?.parse().ok()?;

		Some(Self {
			hours,
			minutes,
			seconds,
		})
	}

	fn after_midnight(self) -> TimeDelta {
		TimeDelta::hours(self.hours.into())
			+ TimeDelta::minutes(self.minutes.into())
			+ TimeDelta::seconds(self.seconds.into())
	}

	pub fn on<Tz: TimeZone>(self, tz: Tz, date: NaiveDate) -> DateTime<Tz> {
		let noon = NaiveTime::from_hms_opt(12, 0, 0).unwrap();
		let midnight = date.and_time(noon).and_local_timezone(tz).single().unwrap();
		midnight + self.after_midnight()
	}
}

impl Display for Time {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(
			f,
			"{:02}:{:02}:{:02}",
			self.hours, self.minutes, self.seconds
		)
	}
}

#[cfg(feature = "serde")]
impl Serialize for Time {
	fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
		self.to_string().serialize(serializer)
	}
}

#[cfg(feature = "serde")]
impl<'de> Deserialize<'de> for Time {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		let s: &str = Deserialize::deserialize(deserializer)?;
		Self::parse(s).ok_or_else(|| de::Error::custom("failed to parse Time"))
	}
}
