use std::{
	collections::HashSet,
	fs::{read, File},
	io::{stdout, BufReader},
	path::PathBuf,
};

use geojson::{Feature, FeatureCollection, Value};
use muv_transit::{amtrak, gtfs, gtfs_rt, netex, viarail, Network};
use prost::Message;

use crate::{Result, TransitFormat};

pub fn run(paths: Vec<PathBuf>, input: TransitFormat) -> Result<()> {
	let mut n = Network::default();
	let mut vehicles = Vec::new();

	match input {
		TransitFormat::Gtfs => {
			for path in paths {
				let f = File::open(path)?;
				let r = gtfs::Reader::new(f)?;
				r.insert_into(&mut n, false)?;
			}
		}
		TransitFormat::GtfsRt => {
			for path in paths {
				let b = read(path)?;
				let m = gtfs_rt::FeedMessage(gtfs_rt::structure::FeedMessage::decode(&*b)?);
				vehicles.extend(m.vehicles(""));
			}
		}
		TransitFormat::Netex => {
			for path in paths {
				let f = File::open(path)?;
				let pd: netex::PublicationDelivery = quick_xml::de::from_reader(BufReader::new(f))?;
				pd.insert_into(&mut n);
			}
		}
		TransitFormat::Amtrak => {
			vehicles.extend(amtrak::TrainsData::fetch_ureq()?.vehicles());
		}
		TransitFormat::Viarail => {
			vehicles.extend(viarail::AllData::fetch_ureq()?.vehicles());
		}
	}

	let mut features = Vec::new();

	let mut seen = HashSet::new();
	for (route_id, route) in n.routes {
		let Some(line_id) = route.line else {
			continue;
		};
		let line = n.lines.get(&line_id).unwrap();
		let Some(line_color) = line.color.as_ref() else {
			continue;
		};

		let shape_id = route.shape;

		let line_string: Vec<_> = if let Some(shape_id) = shape_id {
			let shape = n.shapes.get(&shape_id).unwrap();
			if !seen.insert((line_id, shape_id)) {
				continue;
			}

			shape.iter().map(|(lat, lon)| vec![*lon, *lat]).collect()
		} else {
			if !seen.insert((line_id, route_id)) {
				continue;
			}

			route
				.stops
				.into_iter()
				.map(|id| {
					let loc = n.stops.get(&id).unwrap().location.unwrap();
					vec![loc.1, loc.0]
				})
				.collect()
		};

		let properties = [
			("name".into(), line.name.to_owned().into()),
			("stroke".into(), format!("#{line_color}").into()),
		];
		features.push(Feature {
			bbox: None,
			geometry: Some(Value::LineString(line_string).into()),
			id: None,
			properties: Some(properties.into_iter().collect()),
			foreign_members: None,
		});
	}

	for (stop_id, stop) in n.stops {
		let Some(loc) = stop.location else {
			continue;
		};

		let properties = [
			("id".into(), stop_id.into()),
			("name".into(), stop.name.into()),
			("code".into(), stop.code.into()),
			("ifopt".into(), stop.ifopt.into()),
		];
		features.push(Feature {
			bbox: None,
			geometry: Some(Value::Point(vec![loc.1, loc.0]).into()),
			id: None,
			properties: Some(properties.into_iter().collect()),
			foreign_members: None,
		});
	}

	for vehicle in vehicles {
		let Some(loc) = vehicle.location else {
			continue;
		};

		let properties = [
			("id".into(), vehicle.id.to_string().into()),
			("code".into(), vehicle.code.into()),
			("lineName".into(), vehicle.line_name.into()),
		];
		features.push(Feature {
			bbox: None,
			geometry: Some(Value::Point(vec![loc.1, loc.0]).into()),
			id: None,
			properties: Some(properties.into_iter().collect()),
			foreign_members: None,
		});
	}

	let collection = FeatureCollection {
		bbox: None,
		features,
		foreign_members: None,
	};

	serde_json::to_writer_pretty(stdout().lock(), &collection)?;

	Ok(())
}
