use muv_osm::{Tag, Taglike};

use crate::{
	element::{tags, Id},
	OsmFormat, Result,
};

fn tree_tags(tags: &Tag, pad: usize) {
	for (key, subtags) in &tags.subtags {
		print!("{: <1$}{key}", "", pad);
		if let Some(value) = subtags.value() {
			print!(" = {value}");
		}
		println!();
		tree_tags(subtags, pad + 4);
	}
}

pub fn run(
	input: Id,
	input_format: Option<OsmFormat>,
	output_format: Option<OsmFormat>,
) -> Result<()> {
	let tags = tags(input, input_format)?;

	match output_format {
		None => {
			println!();
			tree_tags(&tags, 0)
		}
		Some(format) => format.write_to_output(&tags)?,
	}
	Ok(())
}
