use std::fmt::Debug;

use muv_osm::{
	lanes::{lanes, parking::ParkingLane, travel::TravelLaneDirection, LaneVariant},
	Conditional, TModes,
};

use crate::{
	element::{tags, Id},
	OsmFormat, Result,
};

const LINE_SEPERATOR: &str = "===============";

pub fn run(
	input: Id,
	input_format: Option<OsmFormat>,
	output_format: Option<OsmFormat>,
	mut regions: Vec<String>,
) -> Result<()> {
	for region in &mut regions {
		region.make_ascii_uppercase();
	}
	let tags = tags(input, input_format)?;

	let lanes = lanes(&tags, &regions).ok_or("No lanes found in the tags")?;

	match output_format {
		None => {
			println!();
			println!("centre: {}", lanes.centre);
			println!("centre_line: {}", lanes.centre_line);
			println!("kerb_left: {:?}", lanes.kerb_left);
			println!("kerb_right: {:?}", lanes.kerb_right);

			for (i, lane) in lanes.lanes.into_iter().enumerate() {
				println!();
				match lane.variant {
					LaneVariant::Travel(t) => {
						println!("{LINE_SEPERATOR} travel {i} {LINE_SEPERATOR}");
						println!("forward:");
						print_travel_direction(&t.forward);
						println!("backward:");
						print_travel_direction(&t.backward);
					}
					LaneVariant::Parking(p) => {
						println!("{LINE_SEPERATOR} parking {i} {LINE_SEPERATOR}");
						print_parking(&p)
					}
				}

				println!("is_sidepath: {:?}", lane.is_sidepath);
				println!("width: {:?}", lane.width);
				println!("surface: {:?}", lane.surface);
				println!("smoothness: {:?}", lane.smoothness);
			}
		}
		Some(format) => format.write_to_output(&lanes)?,
	}
	Ok(())
}

fn print_tmodes<T: Debug>(name: &str, v: &TModes<Conditional<T>>) {
	if v.iter().len() > 0 {
		println!("    {name}:");
	}
	for (mode, c) in v {
		print!("        {mode:?}");
		if c.len() == 1 {
			let restriction = c.first().unwrap();
			println!(" {:?} = {:?}", restriction.conditions, restriction.value);
		} else {
			println!();
			for restriction in c {
				println!(
					"            {:?} = {:?}",
					restriction.conditions, restriction.value
				);
			}
		}
	}
}

fn print_travel_direction(t: &TravelLaneDirection) {
	print_tmodes("access", &t.access);

	print_tmodes("turn", &t.turn);
	print_tmodes("change", &t.change);
	print_tmodes("overtaking", &t.overtaking);

	print_tmodes("minspeed", &t.minspeed);
	print_tmodes("maxspeed", &t.maxspeed);

	print_tmodes("maxweight", &t.maxweight);
	print_tmodes("maxaxleload", &t.maxaxleload);
	print_tmodes("maxlength", &t.maxlength);
	print_tmodes("maxwidth", &t.maxwidth);
	print_tmodes("maxheight", &t.maxheight);
	print_tmodes("maxdraught", &t.maxdraught);
}

fn print_parking(p: &ParkingLane) {
	print_tmodes("access", &p.access);

	print_tmodes("restriction", &p.restriction);

	println!("    bays: {:?}", p.bays);
	println!("    orientation: {:?}", p.orientation);
	println!("    direction: {:?}", p.direction);
	println!("    markings: {:?}", p.markings);
	println!("    capacity: {:?}", p.capacity);
	println!("    zone: {:?}", p.zone);

	print_tmodes("maxstay", &p.maxstay);
}
