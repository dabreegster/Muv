use std::{
	collections::BTreeMap,
	io::{stdin, Read},
};

use clap::Parser;
use muv_osm::Tag;
use serde::Deserialize;
use ureq::get;

use crate::{OsmFormat, Result};

#[derive(Debug, Parser)]
pub struct Id {
	#[arg(
		short,
		long,
		group = "id",
		help_heading = "Tags",
		value_name = "NODE ID",
		help = "Use the tags from this node id"
	)]
	pub node: Option<i64>,

	#[arg(
		short,
		long,
		group = "id",
		help_heading = "Tags",
		value_name = "WAY ID",
		help = "Use the tags from this way id"
	)]
	pub way: Option<i64>,

	#[arg(
		short,
		long,
		group = "id",
		help_heading = "Tags",
		value_name = "RELATION ID",
		help = "Use the tags from this relation id"
	)]
	pub relation: Option<i64>,
}

#[derive(Deserialize)]
struct ApiRes {
	elements: (Element,),
}

#[derive(Deserialize)]
struct Element {
	tags: BTreeMap<String, String>,
}

pub fn tags(input: Id, format: Option<OsmFormat>) -> Result<Tag<'static>> {
	let id = match (input.node, input.way, input.relation) {
		(Some(id), _, _) => Some(("node", id)),
		(_, Some(id), _) => Some(("way", id)),
		(_, _, Some(id)) => Some(("relation", id)),
		_ => None,
	};
	if let Some((typ, id)) = id {
		let res: ApiRes = get(&format!(
			"https://www.openstreetmap.org/api/0.6/{typ}/{id}.json"
		))
		.call()?
		.into_json()?;
		let tags: Tag = res.elements.0.tags.into_iter().collect();
		return Ok(tags);
	}

	let mut s = String::new();
	stdin().read_to_string(&mut s)?;
	let s = s.leak();

	match format {
		Some(OsmFormat::Json) => return Ok(serde_json::from_str(s)?),
		Some(OsmFormat::Yaml) => return Ok(serde_yaml::from_str(s)?),
		None => {}
	}

	s.split('\n')
		.map(|line| line.trim())
		.enumerate()
		.filter(|(_, line)| !line.is_empty())
		.map(|(i, line)| {
			let (key, val) = line
				.split_once('=')
				.ok_or_else(|| format!("Tag in line {} is missing a '='", i + 1))?;
			Ok((key.trim(), val.trim()))
		})
		.collect::<Result<Tag>>()
}
