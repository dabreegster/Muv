use std::{error::Error, fmt::Debug, io::stdout, path::PathBuf, result};

use clap::{Parser, ValueEnum};
use element::Id;
use serde::Serialize;

mod element;
mod lanes;
mod tags;
mod transit;

#[derive(Debug, Parser)]
enum Args {
	Osm {
		#[arg(short, long, global = true, help = "Change the input format")]
		input: Option<OsmFormat>,
		#[arg(short, long, global = true, help = "Change the output format")]
		output: Option<OsmFormat>,

		#[clap(subcommand)]
		command: OsmCommand,
	},

	Transit {
		#[arg(short, long, help = "Change the input format")]
		input: TransitFormat,

		paths: Vec<PathBuf>,
	},
}

#[derive(Debug, Clone, Copy, Parser, ValueEnum)]
enum OsmFormat {
	Json,
	Yaml,
}

#[derive(Debug, Clone, Copy, Parser, ValueEnum)]
enum TransitFormat {
	Gtfs,
	GtfsRt,
	Netex,
	Amtrak,
	Viarail,
}

impl OsmFormat {
	fn write_to_output(&self, v: &impl Serialize) -> Result<()> {
		match self {
			Self::Json => serde_json::to_writer_pretty(stdout().lock(), v)?,
			Self::Yaml => serde_yaml::to_writer(stdout().lock(), v)?,
		};
		Ok(())
	}
}

#[derive(Debug, Parser)]
enum OsmCommand {
	#[command(about = "Format tags")]
	Tags {
		#[clap(flatten)]
		id: Id,
	},

	#[command(about = "Parse lanes of a way")]
	Lanes {
		#[clap(flatten)]
		id: Id,

		#[arg(short = 'R', long, help = "Region codes the lanes are parsed with")]
		regions: Vec<String>,
	},
}

type Result<T> = result::Result<T, Box<dyn Error>>;

fn main() -> Result<()> {
	let args = Args::parse();

	match args {
		Args::Osm {
			input,
			output,
			command,
		} => match command {
			OsmCommand::Tags { id } => tags::run(id, input, output),
			OsmCommand::Lanes { id, regions } => lanes::run(id, input, output, regions),
		},
		Args::Transit { input, paths } => transit::run(paths, input),
	}
}
