use proc_macro2::TokenStream;
use quote::{quote, ToTokens};
use std::{collections::BTreeMap, error::Error};
use ureq::get;

use serde::Deserialize;

use crate::utils::save_formatted;

#[derive(Deserialize)]
struct Response {
	results: Results,
}

#[derive(Deserialize)]
struct Results {
	bindings: Vec<Binding>,
}

#[derive(Deserialize)]
struct Binding {
	area: Value<String>,
	iso_code: Value<String>,
	driving_side: Option<Value<Side>>,
}

#[derive(Deserialize)]
struct Value<T> {
	value: T,
}

#[derive(Debug, Clone, Copy, PartialEq, Deserialize)]
enum Side {
	#[serde(rename = "http://www.wikidata.org/entity/Q13196750")]
	Left,
	#[serde(rename = "http://www.wikidata.org/entity/Q14565199")]
	Right,
}

const QUERY: &str = r#"
SELECT ?area ?iso_code ?driving_side WHERE {
    ?area wdt:P297 ?iso_code.
    OPTIONAL { ?area wdt:P1622 ?driving_side. }
    FILTER(NOT EXISTS { ?area wdt:P576 ?abolished. })
}"#;

const USER_AGENT: &str = "muv-osm-generate/0.1";

pub(crate) fn build() -> Result<(), Box<dyn Error>> {
	let res = get("https://query.wikidata.org/sparql")
		.query("query", QUERY)
		.set("User-Agent", USER_AGENT)
		.set("Accept", "application/sparql-results+json")
		.call()?;
	let data: Response = serde_json::from_reader(res.into_reader())?;

	let res: BTreeMap<_, _> = data
		.results
		.bindings
		.into_iter()
		.filter_map(|e| {
			if let Some(driving_side) = e.driving_side {
				Some((e.iso_code.value, driving_side.value))
			} else {
				// Exclude areas without any roads
				if !matches!(&*e.iso_code.value, "AQ" | "BV" | "HM") {
					println!("\tmissing {} ({})", e.iso_code.value, e.area.value);
				}
				None
			}
		})
		.collect();

	let mut case_left = TokenStream::new();
	let mut case_right = TokenStream::new();

	for (iso_code, side) in res {
		let case = match side {
			Side::Left => &mut case_left,
			Side::Right => &mut case_right,
		};

		if !case.is_empty() {
			case.extend(quote!( | ));
		}
		case.extend(iso_code.into_token_stream());
	}

	let ast = quote! {
		use crate::lanes::Side;

		pub(crate) fn driving_side(region: &str) -> Option<Side> {
			match region {
				#case_left => Some(Side::Left),
				#case_right => Some(Side::Right),
				_ => None,
			}
		}
	};

	save_formatted(ast, "lanes/highway/driving_side.rs")
}
