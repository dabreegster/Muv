use muv_osm::{
	conditional::{Condition, ConditionCompare, ConditionOperator, Restriction},
	units::{Distance, Duration, Number, Quantity, Speed, WalkSpeed, Weight},
	AccessLevel, Conditional, FromOsm, TMode, TModes, Tag, Taglike,
};
use proc_macro2::{Ident, Span, TokenStream};
use quote::{quote, TokenStreamExt};
use serde::Deserialize;
use smallvec::{Array, SmallVec};
use std::{collections::BTreeMap, error::Error, fmt::Debug, fs::File, io, process::Command};
use ureq::get;

use crate::utils::save_formatted;

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct LegalDefaultSpeeds {
	road_types_by_name: BTreeMap<String, RoadType>,
	speed_limits_by_country_code: BTreeMap<String, Vec<SpeedLimitTag>>,
}

impl LegalDefaultSpeeds {
	fn unwrap_subfilter<'a>(&'a self, name: &'a str) -> Vec<&'a str> {
		let mut name = name;
		loop {
			let Some(road_type) = self.road_types_by_name.get(name) else {
				break;
			};

			let mut new_names = RoadType::one_subfilter(road_type);
			match new_names.len() {
				0 => break,
				1 => name = new_names.remove(0),
				_ => return new_names,
			}
		}
		vec![name]
	}
}

#[derive(Deserialize)]
struct RoadType {
	filter: Option<String>,
}

impl RoadType {
	fn one_subfilter(&self) -> Vec<&str> {
		let Some(filter) = self.filter.as_ref() else {
			return Vec::new();
		};

		if let Some(middle) = filter.strip_prefix('{').and_then(|f| f.strip_suffix('}')) {
			if middle.contains(['{', '}']) {
				return Vec::new();
			}

			vec![middle]
		} else {
			if !filter.contains(' ') {
				if let Some(body) = filter.strip_prefix("highway~") {
					return body.split('|').collect();
				}
			}

			Vec::new()
		}
	}
}

#[derive(Deserialize)]
struct SpeedLimitTag {
	name: Option<String>,
	tags: BTreeMap<String, String>,
}

trait ToCode {
	fn to_code(&self) -> TokenStream;
}

impl<T: ToCode> ToCode for Option<T> {
	fn to_code(&self) -> TokenStream {
		match self {
			Some(t) => {
				let t = t.to_code();
				quote! { Some(#t) }
			}
			None => quote! { None },
		}
	}
}

fn to_code_enum<T: Debug>(base: &str, data: &T) -> TokenStream {
	let base = Ident::new(base, Span::call_site());
	let str = format!("{data:?}");
	let ident = Ident::new(&str, Span::call_site());
	quote! { #base::#ident }
}

impl ToCode for TMode {
	fn to_code(&self) -> TokenStream {
		to_code_enum("TMode", self)
	}
}

impl ToCode for AccessLevel {
	fn to_code(&self) -> TokenStream {
		to_code_enum("AccessLevel", self)
	}
}

impl ToCode for ConditionOperator {
	fn to_code(&self) -> TokenStream {
		to_code_enum("ConditionOperator", self)
	}
}

impl ToCode for u32 {
	fn to_code(&self) -> TokenStream {
		quote! { #self }
	}
}

impl<T: ToCode> ToCode for Quantity<T> {
	fn to_code(&self) -> TokenStream {
		let value = self.value.to_code();
		let unit = self.unit.to_code();
		quote! { Quantity::new(#value, #unit) }
	}
}

impl ToCode for Number {
	fn to_code(&self) -> TokenStream {
		let num: f32 = self.into();
		if num == f32::INFINITY {
			quote! { Number::INFINITY }
		} else {
			quote! { Number::new(#num) }
		}
	}
}

impl ToCode for Weight {
	fn to_code(&self) -> TokenStream {
		to_code_enum("Weight", self)
	}
}

impl ToCode for Distance {
	fn to_code(&self) -> TokenStream {
		to_code_enum("Distance", self)
	}
}

impl ToCode for Duration {
	fn to_code(&self) -> TokenStream {
		to_code_enum("Duration", self)
	}
}

impl ToCode for Speed {
	fn to_code(&self) -> TokenStream {
		to_code_enum("Speed", self)
	}
}

impl ToCode for WalkSpeed {
	fn to_code(&self) -> TokenStream {
		match self {
			WalkSpeed::Speed(speed) => {
				let speed = speed.to_code();
				quote! { WalkSpeed::Speed(#speed) }
			}
			WalkSpeed::Walk => {
				quote! { WalkSpeed::Walk }
			}
		}
	}
}

impl ToCode for ConditionCompare {
	fn to_code(&self) -> TokenStream {
		let (str, body) = match self {
			Self::Weight(u) => ("Weight", u.to_code()),
			Self::Weightrating(u) => ("Weightrating", u.to_code()),
			Self::Weightcapacity(u) => ("Weightcapacity", u.to_code()),
			Self::Emptyweight(u) => ("Emptyweight", u.to_code()),
			Self::Axleload(u) => ("Axleload", u.to_code()),
			Self::Length(u) => ("Length", u.to_code()),
			Self::Width(u) => ("Width", u.to_code()),
			Self::Height(u) => ("Height", u.to_code()),
			Self::Wheels(n) => ("Wheels", n.to_code()),
			Self::Draught(u) => ("Draught", u.to_code()),
			Self::Occupants(n) => ("Occupants", n.to_code()),
			Self::Stay(n) => ("Stay", n.to_code()),
			Self::Seats(n) => ("Seats", n.to_code()),
			Self::Axles(n) => ("Axles", n.to_code()),
			Self::Trailers(n) => ("Trailers", n.to_code()),
			Self::TrailerWeight(u) => ("TrailerWeight", u.to_code()),
		};
		let ident = Ident::new(str, Span::call_site());
		quote! { ConditionCompare::#ident(#body) }
	}
}

impl ToCode for Condition {
	fn to_code(&self) -> TokenStream {
		let end = match self {
			Condition::DateTimeRange(r) => {
				let s = r.to_string();
				quote! { DateTimeRange(Box::new(DateTimeRange::from_osm_str(#s).unwrap())) }
			}
			Condition::Winter => quote! { Winter },
			Condition::Summer => quote! { Summer },
			Condition::Wet => quote! { Wet },
			Condition::Snow => quote! { Snow },
			Condition::Compare(op, comp) => {
				let op = op.to_code();
				let comp = comp.to_code();
				quote! { Compare(#op, #comp) }
			}
			Condition::AccessLevel(access) => {
				let access = access.to_code();
				quote! { AccessLevel(#access) }
			}
			Condition::Trailer => quote! { Trailer },
			Condition::Caravan => quote! { Caravan },
			Condition::Articulated => quote! { Articulated },
		};
		quote! { Condition::#end }
	}
}

impl<T: ToCode> ToCode for Vec<T> {
	fn to_code(&self) -> TokenStream {
		let body = self
			.iter()
			.map(ToCode::to_code)
			.reduce(|a, b| quote! { #a,#b });
		quote! { vec![#body] }
	}
}

impl<T: ToCode, A> ToCode for SmallVec<A>
where
	A: Array<Item = T>,
{
	fn to_code(&self) -> TokenStream {
		let body = self
			.iter()
			.map(ToCode::to_code)
			.reduce(|a, b| quote! { #a,#b });
		quote! { smallvec![#body] }
	}
}

impl<T: ToCode> ToCode for Restriction<T> {
	fn to_code(&self) -> TokenStream {
		let value = self.value.to_code();
		let conditions = self.conditions.to_code();
		quote! { Restriction { value: #value, conditions: #conditions } }
	}
}

impl<T: ToCode> ToCode for Conditional<T> {
	fn to_code(&self) -> TokenStream {
		let restrictions = self.0.to_code();
		quote! { Conditional(#restrictions) }
	}
}

impl SpeedLimitTag {
	fn speed(&self, tags: &Tag, tag: &str) -> TokenStream {
		let empty = Tag::new();
		let tags = tags.get(tag).unwrap_or(&empty);
		let mut data: TModes<Conditional<Quantity<WalkSpeed>>> = TMode::iter_tags_with_all(tags)
			.filter_map(|(mode, tag)| Some((mode, Conditional::from_osm(tag)?)))
			.collect();
		if tags.value().is_none() && tags.get("advisory").is_some() {
			data.get_raw_mut(TMode::All).unwrap().0.insert(
				0,
				Restriction {
					value: Quantity::new(Number::INFINITY, Speed::KilometresPerHour.into()),
					conditions: Vec::new(),
				},
			)
		}

		let data_s: TokenStream = data
			.into_iter()
			.map(|(mode, data)| {
				let mode = mode.to_code();
				let data = data.to_code();
				quote! { (#mode, #data), }
			})
			.collect();
		quote! { TModes::from([ #data_s ]) }
	}
}

fn add_tag(
	ast: &mut TokenStream,
	ctx: &mut BTreeMap<String, TokenStream>,
	country: &str,
	data: &LegalDefaultSpeeds,
	tag: &SpeedLimitTag,
) {
	let Some(mut name) = tag.name.as_deref() else {
		return;
	};

	if country.starts_with("GB") {
		name = match name {
			"United Kingdom: restricted road" => "nsl_restricted",
			"rural" => "nsl_single",
			"dual carriageway" => "nsl_dual",
			name => name,
		};
	}

	match name {
		"service road" => name = "service",
		"pedestrian area" => name = "pedestrian",
		_ => {}
	};

	let names = data.unwrap_subfilter(name);

	let tags: Tag = tag.tags.iter().collect();

	let minspeed = tag.speed(&tags, "minspeed");
	let maxspeed = tag.speed(&tags, "maxspeed");

	for name in names {
		let name = name.replace(' ', "_");

		if !matches!(
			&*name,
			"urban"
				| "rural" | "motorway"
				| "motorroad" | "trunk"
				| "nsl_single" | "nsl_dual"
				| "nsl_restricted"
				| "alley" | "living_street"
				| "bicycle_road" | "service"
				| "pedestrian"
		) {
			continue;
		}

		let mut country_fn = country.replace('-', "_");
		country_fn.make_ascii_lowercase();
		let name_country = Ident::new(&format!("{name}_{country_fn}"), Span::call_site());
		ast.append_all(quote! {
			fn #name_country() -> (DefaultSpeed, DefaultSpeed) {
				(#minspeed, #maxspeed)
			}
		});

		ctx.entry(name.clone())
			.or_default()
			.append_all(quote! { #country => #name_country(), });
	}
}

const SOURCE_REPO: &str = "https://github.com/westnordost/osm-legal-default-speeds";
const SOURCE_URL: &str = "https://raw.githubusercontent.com/westnordost/osm-legal-default-speeds/master/demo/distribution/legal_default_speeds.json";

pub fn build() -> Result<(), Box<dyn Error>> {
	let r: Box<dyn io::Read> = if let Ok(f) = File::open("legal_default_speeds.json") {
		Box::new(f)
	} else if cfg!(feature = "python") {
		println!("\tgit clone");
		Command::new("git")
			.arg("clone")
			.arg(SOURCE_REPO)
			.arg("--depth=1")
			.spawn()?
			.wait()?;

		println!("\tpip install");
		Command::new("python")
			.arg("-m")
			.arg("pip")
			.arg("install")
			.arg("-r")
			.arg("osm-legal-default-speeds/parser/requirements.txt")
			.spawn()?
			.wait()?;

		println!("\tpython");
		Command::new("python")
			.arg("osm-legal-default-speeds/parser/main.py")
			.spawn()?
			.wait()?;

		Box::new(File::open("legal_default_speeds.json")?)
	} else {
		get(SOURCE_URL).call()?.into_reader()
	};

	let lds: LegalDefaultSpeeds = serde_json::from_reader(r)?;

	let mut ast = quote! {
		#![allow(clippy::too_many_lines)]

		use crate::{
			units::{Distance, Speed, Number, Quantity, WalkSpeed, Weight},
			conditional::{Condition, Conditional, ConditionCompare, ConditionOperator, DateTimeRange, Restriction},
			FromOsmStr, TMode, TModes,
		};
		use smallvec::smallvec;

		pub(crate) type DefaultSpeed = TModes<Conditional<Quantity<WalkSpeed>>>;

	};

	let mut ctx = BTreeMap::new();
	for (country, tags) in lds.speed_limits_by_country_code.iter() {
		for tag in tags {
			add_tag(&mut ast, &mut ctx, country, &lds, tag);
		}
	}

	let mut default = TokenStream::new();
	for class in ctx.keys() {
		let class_ident = Ident::new(class, Span::call_site());
		default.append_all(quote! { #class => #class_ident(country), });
	}

	for (class, body) in ctx {
		let class_ident = Ident::new(&class, Span::call_site());
		ast.append_all(quote! {
			pub(crate) fn #class_ident(country: &str) -> Option<(DefaultSpeed, DefaultSpeed)> {
				Some(match country {
					#body
					_ => return None,
				})
			}
		});
	}

	ast.append_all(quote! {
		pub(crate) fn default(country: &str, class: &str) -> Option<(DefaultSpeed, DefaultSpeed)> {
			match class {
				#default
				_ => None,
			}
		}
	});

	save_formatted(ast, "lanes/highway/speed_default.rs")
}
