if [[ $# -ne 1 ]]; then
	echo "Requires one argument. Try it with 'hazmat' or 'ramp'"
	exit 2
fi

curl "https://taginfo.openstreetmap.org/api/4/keys/all?query=$1" | jq ".data | map({
	data: .,
	match: .key | capture(\"^((?<prefix>.+):)?$1(:(?<suffix>.+?))??((:description|:note)(:[a-z]{2})?)?(:left|:right|:both)?(:oneway)?(:signed)?(:type)?(:lanes)?(:forward|:backward|:both_ways)?(:conditional|:condtional)?(:start_date|:end_date|:-?202[0-9])?(:check_date)?$\")
}) |
	map(.match.prefix //= \"\" | .match.suffix //= \"\") |
	map(select(.match.suffix)) |
	reduce .[] as \$i ({}; .[\$i.match.suffix] += \$i.data.count_all) |
	to_entries |
	sort_by(.value) |
	reverse |
	from_entries"
