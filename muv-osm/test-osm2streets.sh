OSM2STREETS_PATH="../../osm2streets"

MUV_OSM_PATH="$(pwd)"

cd "$OSM2STREETS_PATH"
cd osm2lanes

CARGO_TOML_TMP="$(mktemp)"
cp Cargo.toml $CARGO_TOML_TMP
cargo add --path "$MUV_OSM_PATH"
cargo test
mv "$CARGO_TOML_TMP" Cargo.toml
