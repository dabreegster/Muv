#![no_main]

use libfuzzer_sys::{
	arbitrary::{self, Arbitrary},
	fuzz_target,
};
use muv_osm::{lanes::lanes, Tag};

#[derive(Debug, Arbitrary)]
struct Input<'a> {
	tag: Tag<'a>,
	regions: Vec<String>,
}

fuzz_target!(|input: Input| {
	if let Some(lanes) = lanes(&input.tag, &input.regions) {
		let double_lanes = lanes.count() * 2;
		assert!(double_lanes >= lanes.centre);
		assert!(double_lanes >= lanes.centre_line);
	};
});
