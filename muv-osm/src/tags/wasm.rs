use js_sys::{try_iter, Array, Object, Reflect};
use wasm_bindgen::{prelude::wasm_bindgen, JsValue};

use crate::{Tag, Taglike};

#[wasm_bindgen(js_name = Tag)]
pub struct JsTag {
	pub(crate) tag: Tag<'static>,
}

impl Tag<'_> {
	fn serialize_to_js(&self, o: &mut Object, name: &str) {
		if let Some(value) = self.value() {
			Reflect::set(o, &JsValue::from_str(name), &JsValue::from_str(value)).unwrap();
		}
		for (key, tag) in &self.subtags {
			if name.is_empty() {
				tag.serialize_to_js(o, key);
			} else {
				tag.serialize_to_js(o, format!("{name}:{key}").as_str());
			}
		}
	}
}

#[wasm_bindgen(js_class = Tag)]
impl JsTag {
	/// # Errors
	/// Errors if `iterable` is not a JS Iterator yielding arrays containing two strings
	#[allow(clippy::use_self)]
	#[wasm_bindgen(constructor)]
	pub fn new(iterable: &JsValue) -> Result<JsTag, JsValue> {
		if iterable.is_undefined() {
			return Ok(Self { tag: Tag::new() });
		}

		let iter = try_iter(iterable)?.ok_or("expect iterable or undefined")?;

		let tag = iter
			.map(|v| {
				let a = Array::from(&v?);

				let key = a.get(0).as_string().ok_or("expected keys to be strings")?;
				let val = a
					.get(1)
					.as_string()
					.ok_or("expected values to be strings")?;

				Ok((key, val))
			})
			.collect::<Result<Tag, JsValue>>()?;
		Ok(Self { tag })
	}

	pub fn set(&mut self, key: String, val: String) {
		self.tag.set(key, val);
	}

	/// # Errors
	/// Errors if the provided path is not a JS Iterator of strings
	#[wasm_bindgen(variadic)]
	pub fn get(&self, path: &JsValue) -> Result<Option<String>, JsValue> {
		let iter = try_iter(path)?.ok_or("expect arguments to be iterable")?;

		let mut res = &self.tag;
		for v in iter {
			if let Some(next) = res.get(
				&v?.as_string()
					.ok_or("expected path to consist of strings")?,
			) {
				res = next;
			} else {
				return Ok(None);
			}
		}
		Ok(res.value().map(ToOwned::to_owned))
	}

	#[must_use]
	#[wasm_bindgen(js_name = toJSON)]
	pub fn to_json(&self) -> JsValue {
		let mut o = Object::new();
		self.tag.serialize_to_js(&mut o, "");
		o.into()
	}
}
