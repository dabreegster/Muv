use std::{
	borrow::Cow,
	collections::{hash_map::Iter, HashMap},
	hash::BuildHasherDefault,
	iter::Map,
};

use rustc_hash::FxHasher;

#[cfg(feature = "arbitrary")]
use arbitrary::Arbitrary;

pub type TagHasher = BuildHasherDefault<FxHasher>;

/// Tree of OSM tags (keys and values)
#[derive(Debug, Default, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "arbitrary", derive(Arbitrary))]
pub struct Tag<'a> {
	pub value: Option<Cow<'a, str>>,
	pub subtags: HashMap<Cow<'a, str>, Tag<'a>, TagHasher>,
}

impl Tag<'_> {
	#[inline]
	#[must_use]
	pub fn new() -> Self {
		Self::default()
	}

	#[inline]
	#[must_use]
	pub(crate) fn with_capacity(capacity: usize) -> Self {
		Self {
			value: None,
			subtags: HashMap::with_capacity_and_hasher(capacity, TagHasher::default()),
		}
	}
}

impl<'a> Tag<'a> {
	#[inline]
	#[must_use]
	pub fn value_mut(&mut self) -> &mut Option<Cow<'a, str>> {
		&mut self.value
	}

	#[inline]
	pub fn set<K, V>(&mut self, key: K, val: V)
	where
		K: Into<Cow<'a, str>>,
		V: Into<Cow<'a, str>>,
	{
		self.subtags.entry(key.into()).or_default().value = Some(val.into());
	}

	#[must_use]
	pub fn into_owned(self) -> Tag<'static> {
		let value = self.value.map(|val| val.into_owned().into());
		let subtags: HashMap<_, Tag<'static>, TagHasher> = self
			.subtags
			.into_iter()
			.map(|(key, subtags)| (key.into_owned().into(), subtags.into_owned()))
			.collect();
		Tag { value, subtags }
	}
}

pub trait Taglike<'a>: Copy + 'a {
	type Iter: Iterator<Item = (&'a str, Self)>;

	#[must_use]
	fn value(self) -> Option<&'a str>;

	#[must_use]
	fn get(self, key: &str) -> Option<Self>;

	#[must_use]
	fn get_value(self, key: &str) -> Option<&'a str> {
		self.get(key)?.value()
	}

	#[must_use]
	fn iter_subtags(self) -> Self::Iter;
}

impl<'a> Taglike<'a> for &'a Tag<'a> {
	type Iter = Map<
		Iter<'a, Cow<'a, str>, Tag<'a>>,
		fn((&'a Cow<'a, str>, &'a Tag<'a>)) -> (&'a str, &'a Tag<'a>),
	>;

	#[inline]
	fn value(self) -> Option<&'a str> {
		self.value.as_deref()
	}

	#[inline]
	fn get(self, key: &str) -> Option<Self> {
		self.subtags.get(key)
	}

	#[inline]
	fn iter_subtags(self) -> Self::Iter {
		self.subtags.iter().map(|(k, v)| (k, v))
	}
}

impl<'a, I> From<I> for Tag<'a>
where
	I: Into<Cow<'a, str>>,
{
	#[inline]
	fn from(value: I) -> Self {
		Self {
			value: Some(value.into()),
			subtags: HashMap::default(),
		}
	}
}

impl<'a> Tag<'a> {
	#[inline]
	pub(crate) fn insert_raw(
		&mut self,
		key: impl Into<Cow<'a, str>>,
		val: impl Into<Cow<'a, str>>,
	) {
		let mut tag = self;
		let key: Cow<_> = key.into();
		for part in key.as_ref().split(':') {
			tag = tag.subtags.entry(part.to_owned().into()).or_default();
		}
		tag.value = Some(val.into());
	}

	fn for_each<F: FnMut((Cow<'a, str>, Cow<'a, str>))>(self, mut f: F, name: Cow<'a, str>) -> F {
		for (key, tag) in self.subtags {
			f = if name.is_empty() {
				tag.for_each(f, key)
			} else {
				tag.for_each(f, Cow::Owned(format!("{name}:{key}")))
			}
		}

		if let Some(value) = self.value {
			f((name, value));
		}

		f
	}
}

impl<'a, K, V> FromIterator<(K, V)> for Tag<'a>
where
	K: Into<Cow<'a, str>>,
	V: Into<Cow<'a, str>>,
{
	fn from_iter<T: IntoIterator<Item = (K, V)>>(iter: T) -> Self {
		let iter = iter.into_iter();
		let (lower, _) = iter.size_hint();
		let mut tag = Tag::with_capacity(lower / 2);
		for (key, val) in iter {
			tag.insert_raw(key, val);
		}
		tag
	}
}

impl<'a> IntoIterator for Tag<'a> {
	type Item = (Cow<'a, str>, Cow<'a, str>);
	type IntoIter = IntoTagsIter<'a>;

	#[inline]
	fn into_iter(self) -> Self::IntoIter {
		IntoTagsIter { tags: self }
	}
}

pub struct IntoTagsIter<'a> {
	tags: Tag<'a>,
}

impl<'a> Iterator for IntoTagsIter<'a> {
	type Item = (Cow<'a, str>, Cow<'a, str>);

	fn next(&mut self) -> Option<Self::Item> {
		todo!()
	}

	#[inline]
	fn for_each<F>(self, f: F)
	where
		Self: Sized,
		F: FnMut(Self::Item),
	{
		self.tags.for_each(f, Cow::Borrowed(""));
	}
}

#[cfg(test)]
mod tests {
	use crate::Tag;

	#[test]
	fn parse_tags() {
		let list = vec![
			("highway", "primary"),
			("maxspeed", "70"),
			("maxspeed:bus", "60"),
			("maxspeed:hgv", "50"),
		];

		let tags: Tag = list.into_iter().collect();

		assert_eq!(
			tags,
			Tag {
				value: None,
				subtags: [
					("highway".into(), "primary".into()),
					(
						"maxspeed".into(),
						Tag {
							value: Some("70".into()),
							subtags: [("bus".into(), "60".into()), ("hgv".into(), "50".into())]
								.into_iter()
								.collect()
						}
					)
				]
				.into_iter()
				.collect()
			}
		);
	}
}
