/// Get subtags from `Tag`.
///
/// ```rust
/// use muv_osm::{get_tag, Tag, Taglike};
///
/// let tags: Tag = vec![
///     ("maxspeed", "50"),
///     ("maxspeed:hvg", "40"),
///     ("maxspeed:hvg:forward", "45"),
/// ].into_iter().collect();
///
/// assert_eq!(get_tag!(tags, maxspeed), Some("50"));
///
/// let hgv_tags = get_tag!(tags, maxspeed:hvg:).unwrap();
/// assert_eq!(hgv_tags.value(), Some("40"));
/// assert_eq!(get_tag!(hgv_tags, forward), Some("45"));
/// ```
#[macro_export]
macro_rules! get_tag {
    ($tag:expr, $name:ident) => {{
        #[allow(unused_imports)]
        use $crate::Taglike;
        ($tag).get_value(stringify!($name))
    }};
    ($tag:expr, $name:ident:) => {{
        #[allow(unused_imports)]
        use $crate::Taglike;
        ($tag).get(stringify!($name))
    }};
    ($tag:expr, $name:ident:$($rest:tt)+) => {{
        #[allow(unused_imports)]
        use $crate::Taglike;
        ($tag).get(stringify!($name)).and_then(|tags| get_tag!(tags, $($rest)+))
    }};
}

#[doc(hidden)]
#[macro_export]
macro_rules! set_tag_internal {
    ($tag:expr, $key:ident = $value:literal) => {
        *$tag.subtags.entry(stringify!($key).into()).or_default().value_mut() = Some(($value).into())
    };
    ($tag:expr, $key:ident:$($keyrest:ident):+ = $value:literal) => {
        $crate::set_tag_internal!($tag.subtags.entry(stringify!($key).into()).or_default(), $($keyrest):+ = $value)
    };
}

/// Set subtags of `Tag`
///
/// ```rust
/// use muv_osm::{get_tag, set_tag, Tag};
///
/// let mut tags = Tag::new();
///
/// set_tag!(tags, amenity = "drinking_water");
/// assert_eq!(get_tag!(tags, amenity), Some("drinking_water"));
///
/// set_tag! {tags,
///     drinking_water:legal = "yes",
///     opening_hours = "24/7",
/// };
/// assert_eq!(get_tag!(tags, drinking_water:legal), Some("yes"));
/// assert_eq!(get_tag!(tags, opening_hours), Some("24/7"));
/// ```
#[macro_export]
macro_rules! set_tag {
    ($tag:expr, $( $($key:ident):+ = $value:literal ),+ $(,)? ) => {{
        let tags = &mut $tag;
        $( $crate::set_tag_internal!(tags, $($key):+ = $value); )+
    }};
}

/// Create new `Tag` with subtags
///
/// ```rust
/// use muv_osm::{get_tag, new_tag};
///
/// let tags = new_tag!(shop = "supermarket");
/// assert_eq!(get_tag!(tags, shop), Some("supermarket"));
///
/// let tags = new_tag! {
///     shop = "bakery",
///     diet:gluten_free = "yes",
/// };
/// assert_eq!(get_tag!(tags, shop), Some("bakery"));
/// assert_eq!(get_tag!(tags, diet:gluten_free), Some("yes"));
/// ```
#[macro_export]
macro_rules! new_tag {
    ($( $($key:ident):+ = $value:literal ),+ $(,)? ) => {{
        let mut tags = $crate::Tag::new();
        $crate::set_tag!{tags, $( $($key):+ = $value ),+ };
        tags
    }};
}
