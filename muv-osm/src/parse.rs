use std::{borrow::Cow, collections::BTreeMap};

use crate::Taglike;

/// Parse a string into an OSM value
///
/// The trait is most of the time derived for enums using the derive macro.
/// ```
/// # use muv_osm::FromOsmStr;
/// #[derive(Debug, PartialEq, FromOsmStr)]
/// enum Lit {
///     Yes,
///     Limited,
///     #[osm_str("24/7")]
///     TwentyfourSeven,
///     StreetLight,
/// }
///
/// assert_eq!(Lit::from_osm_str("yes"), Some(Lit::Yes));
/// assert_eq!(Lit::from_osm_str("Limited"), Some(Lit::Limited));
/// assert_eq!(Lit::from_osm_str("24/7"), Some(Lit::TwentyfourSeven));
/// assert_eq!(Lit::from_osm_str("street_light"), Some(Lit::StreetLight));
/// assert_eq!(Lit::from_osm_str("unknown"), None);
/// ```
pub trait FromOsmStr: Sized {
	fn from_osm_str(s: &str) -> Option<Self>;
}

pub trait FromOsm<'a, T: Taglike<'a>>: Sized {
	fn from_osm(tag: T) -> Option<Self>;
}

pub trait ToOsmStr {
	fn to_osm_str(self) -> Option<Cow<'static, str>>;
}

impl FromOsmStr for bool {
	fn from_osm_str(s: &str) -> Option<Self> {
		match s {
			"yes" => Some(true),
			"no" => Some(false),
			_ => None,
		}
	}
}

impl ToOsmStr for bool {
	fn to_osm_str(self) -> Option<Cow<'static, str>> {
		Some(if self { "yes" } else { "no" }.into())
	}
}

impl FromOsmStr for u32 {
	fn from_osm_str(s: &str) -> Option<Self> {
		s.parse().ok()
	}
}

impl ToOsmStr for u32 {
	fn to_osm_str(self) -> Option<Cow<'static, str>> {
		Some(self.to_string().into())
	}
}

impl FromOsmStr for String {
	fn from_osm_str(s: &str) -> Option<Self> {
		Some(s.to_owned())
	}
}

impl ToOsmStr for String {
	fn to_osm_str(self) -> Option<Cow<'static, str>> {
		Some(self.into())
	}
}

impl FromOsmStr for Vec<String> {
	fn from_osm_str(s: &str) -> Option<Self> {
		Some(s.split(';').map(ToOwned::to_owned).collect())
	}
}

impl ToOsmStr for Vec<String> {
	fn to_osm_str(self) -> Option<Cow<'static, str>> {
		self.into_iter()
			.reduce(|mut res, v| {
				res.push(';');
				res.push_str(&v);
				res
			})
			.map(Cow::Owned)
	}
}

impl<'a, T: Taglike<'a>, F: FromOsm<'a, T>> FromOsm<'a, T> for BTreeMap<&'a str, F> {
	fn from_osm(tag: T) -> Option<Self> {
		tag.iter_subtags()
			.map(|(key, val)| F::from_osm(val).map(|val| (key, val)))
			.collect()
	}
}

macro_rules! simple_enum {
    ($name:ident $(($($derive:ident),+))? $(#[$($attr:tt)*])* { $($content:tt)+ }) => {
        $(#[$($attr)*])*
        #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, $crate::FromOsmStr, $crate::ToOsmStr, $($($derive),+)?)]
        #[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
        #[cfg_attr(feature = "wasm-bindgen", wasm_bindgen::prelude::wasm_bindgen)]
        pub enum $name {
            $($content)+
        }
    };
}
pub(crate) use simple_enum;
