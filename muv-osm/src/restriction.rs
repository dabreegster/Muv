use crate::{get_tag, simple_enum, Conditional, FromOsm, FromOsmStr, TMode, TModes, Tag, ToOsmStr};

simple_enum!(TurnRestriction {
	#[osm_str("no_right_turn" | "no_left_turn" | "no_u_turn" | "no_straight_on")]
	Not,
	#[osm_str("only_right_turn" | "only_left_turn" | "only_u_turn" | "only_straight_on")]
	Only,
	#[osm_str()]
	None
});

pub fn turn_restriction(tags: &Tag) -> Option<TModes<Conditional<TurnRestriction>>> {
	let mut res: TModes<Conditional<TurnRestriction>> =
		TMode::iter_tags_with_all(get_tag!(tags, restriction:)?)
			.filter_map(|(mode, tags)| Some((mode, Conditional::from_osm(tags)?)))
			.collect();

	if let Some(except) = get_tag!(tags, except) {
		for mode in except.split(';').filter_map(TMode::from_osm_str) {
			res.set(mode, TurnRestriction::None.into());
		}
	}

	Some(res)
}
