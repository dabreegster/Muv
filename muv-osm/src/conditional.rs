use std::{
	borrow::Cow,
	cmp::Ordering,
	error::Error,
	fmt::{self, Debug, Display, Formatter},
	iter::once,
	num::Saturating,
	ops::Deref,
	slice::{Iter, IterMut},
	str::FromStr,
};

use chrono::{Datelike, NaiveDateTime};
use opening_hours::{OpeningHours, ParserError};
use smallvec::{smallvec, IntoIter, SmallVec};

#[cfg(feature = "serde")]
use serde::{
	de::{self, Visitor},
	Deserialize, Serialize, Serializer,
};

use crate::{
	access::AccessLevel,
	get_tag,
	location::Location,
	units::{Distance, Duration, Quantity, Weight},
	vehicle::Vehicle,
	FromOsm, FromOsmStr, TModes, Taglike, ToOsmStr,
};

#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Conditional<T>(pub SmallVec<[Restriction<T>; 1]>);

impl<T> Default for Conditional<T> {
	fn default() -> Self {
		Self(SmallVec::default())
	}
}

impl<T> From<T> for Conditional<T> {
	fn from(value: T) -> Self {
		Self(smallvec![Restriction {
			value,
			conditions: Vec::new(),
		}])
	}
}

impl<T> Deref for Conditional<T> {
	type Target = [Restriction<T>];

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

impl<T> IntoIterator for Conditional<T> {
	type Item = Restriction<T>;
	type IntoIter = IntoIter<[Restriction<T>; 1]>;
	fn into_iter(self) -> Self::IntoIter {
		self.0.into_iter()
	}
}
impl<'a, T> IntoIterator for &'a mut Conditional<T> {
	type Item = &'a mut Restriction<T>;
	type IntoIter = IterMut<'a, Restriction<T>>;
	fn into_iter(self) -> Self::IntoIter {
		self.0.iter_mut()
	}
}
impl<'a, T> IntoIterator for &'a Conditional<T> {
	type Item = &'a Restriction<T>;
	type IntoIter = Iter<'a, Restriction<T>>;
	fn into_iter(self) -> Self::IntoIter {
		self.0.iter()
	}
}

impl<'a, T: Taglike<'a>, F: FromOsmStr> FromOsm<'a, T> for Conditional<F> {
	fn from_osm(tag: T) -> Option<Self> {
		Self::parse_with(tag, F::from_osm_str)
	}
}

impl<T> Conditional<T> {
	pub fn base(&self) -> Option<&T> {
		self.first()
			.filter(|r| r.conditions.is_empty())
			.map(|r| &r.value)
	}

	pub(crate) fn parse_with<'a, Tl: Taglike<'a>>(
		tags: Tl,
		mut f: impl FnMut(&'a str) -> Option<T>,
	) -> Option<Self> {
		let mut parens_depth: Saturating<u32> = Saturating(0);
		let mut in_quotes = false;
		let restrictions: SmallVec<_> = once(tags.value().and_then(&mut f))
			.flatten()
			.map(|default| Restriction {
				conditions: Vec::new(),
				value: default,
			})
			.chain(
				get_tag!(tags, conditional)
					.unwrap_or_default()
					.split(|c| {
						if c == '"' {
							in_quotes = !in_quotes;
						} else if !in_quotes {
							match c {
								'(' => parens_depth += 1,
								')' => parens_depth -= 1,
								';' if parens_depth.0 == 0 => return true,
								_ => {}
							};
						}
						false
					})
					.filter_map(|part| Restriction::parse_osm(part, &mut f)),
			)
			.take(50)
			.collect();

		if restrictions.is_empty() {
			None
		} else {
			Some(Self(restrictions))
		}
	}

	pub fn map<R>(&self, f: impl Fn(&T) -> R) -> Conditional<R> {
		Conditional(
			self.iter()
				.map(|c| Restriction {
					conditions: c.conditions.clone(),
					value: f(&c.value),
				})
				.collect(),
		)
	}

	pub fn into_map<R>(self, f: impl Fn(T) -> R) -> Conditional<R> {
		Conditional(
			self.into_iter()
				.map(|c| Restriction {
					conditions: c.conditions,
					value: f(c.value),
				})
				.collect(),
		)
	}

	#[must_use]
	pub fn merge<O, R>(&self, other: &Conditional<O>, f: impl Fn(&T, &O) -> R) -> Conditional<R> {
		Conditional(
			self.iter()
				.flat_map(|left| {
					other.iter().map(|right| Restriction {
						conditions: [left.conditions.as_slice(), right.conditions.as_slice()]
							.concat(),
						value: f(&left.value, &right.value),
					})
				})
				.collect(),
		)
	}

	#[must_use]
	pub fn evaluate_into(self, situation: &MatchSituation) -> Option<T> {
		self.into_iter()
			.rev()
			.find(|r| r.conditions.iter().all(|c| c.matches(situation)))
			.map(|r| r.value)
	}

	#[must_use]
	pub fn evaluate(&self, situation: &MatchSituation) -> Option<&T> {
		self.iter()
			.rev()
			.find(|r| r.conditions.iter().all(|c| c.matches(situation)))
			.map(|r| &r.value)
	}
}

impl<T> TModes<Conditional<T>> {
	#[must_use]
	pub fn evaluate(&self, situation: &MatchSituation) -> Option<&T> {
		situation
			.vehicle
			.mode
			.iter_hierarchy()
			.find_map(|mode| self.get_raw(mode)?.evaluate(situation))
	}
}

#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Restriction<T> {
	pub value: T,
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "Vec::is_empty"))]
	pub conditions: Vec<Condition>,
}

fn remove_parens(mut s: &str) -> &str {
	s = s.trim();

	let mut end_count = 0;
	let mut end_i = 0;
	for (i, c) in s.char_indices().rev() {
		if c == ')' {
			end_count += 1;
		} else {
			end_i = i;
			break;
		}
	}

	let mut chars = s.char_indices();

	let mut quotes = false;
	let mut start_count = Saturating(0);
	for (_, c) in &mut chars {
		match c {
			'(' => start_count += 1,
			')' => start_count -= 1,
			'"' => {
				quotes = true;
				break;
			}
			_ => break,
		}
	}

	let mut mid_count = 0;
	for (i, c) in chars {
		if i >= end_i {
			break;
		}

		if c == '"' {
			quotes = !quotes;
		} else if !quotes {
			match c {
				'(' => mid_count += 1,
				')' => {
					if mid_count > 0 {
						mid_count -= 1;
					} else {
						start_count -= 1;
					}
				}
				_ => {}
			}
		}
	}

	let i = end_count.min(start_count.0);
	&s[i..s.len() - i]
}

impl<T> Restriction<T> {
	fn parse_osm<'a, F: FnMut(&'a str) -> Option<T>>(part: &'a str, f: &mut F) -> Option<Self> {
		let (left, right) = part.split_once('@')?;
		let value = f(remove_parens(left))?;
		let conditions = remove_parens(right)
			.split(" AND ")
			.map(Condition::from_osm_str)
			.collect::<Option<_>>()?;
		Some(Self { value, conditions })
	}
}

#[derive(Clone)]
pub struct DateTimeRange {
	oh: OpeningHours,
	s: String,
}

pub struct ParseDateTimeRangeError(pub ParserError);

impl Error for ParseDateTimeRangeError {}

impl Debug for ParseDateTimeRangeError {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(f, "{:?}", self.0)
	}
}

impl Display for ParseDateTimeRangeError {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(f, "{:?}", self.0)
	}
}

impl From<ParserError> for ParseDateTimeRangeError {
	fn from(value: ParserError) -> Self {
		Self(value)
	}
}

impl FromStr for DateTimeRange {
	type Err = ParseDateTimeRangeError;
	fn from_str(s: &str) -> Result<Self, Self::Err> {
		OpeningHours::parse(s).map_err(Into::into).map(|oh| Self {
			oh,
			s: s.to_owned(),
		})
	}
}

impl FromOsmStr for DateTimeRange {
	fn from_osm_str(s: &str) -> Option<Self> {
		s.parse().ok()
	}
}

impl ToOsmStr for DateTimeRange {
	fn to_osm_str(self) -> Option<Cow<'static, str>> {
		Some(self.s.into())
	}
}

impl DateTimeRange {
	#[must_use]
	pub fn matches(&self, date_time: NaiveDateTime) -> bool {
		self.oh.is_open(date_time)
	}
}

impl Display for DateTimeRange {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(f, "{}", self.s)
	}
}

impl Debug for DateTimeRange {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{}", self.s)
	}
}

impl PartialEq for DateTimeRange {
	fn eq(&self, other: &Self) -> bool {
		self.s == other.s
	}
}

#[cfg(feature = "serde")]
impl Serialize for DateTimeRange {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: Serializer,
	{
		self.s.serialize(serializer)
	}
}

#[cfg(feature = "serde")]
struct DateTimeRangeVisitor;

#[cfg(feature = "serde")]
impl<'de> Visitor<'de> for DateTimeRangeVisitor {
	type Value = DateTimeRange;

	fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
		formatter.write_str("a datetime range in opening_hours format")
	}

	fn visit_string<E>(self, s: String) -> Result<Self::Value, E>
	where
		E: de::Error,
	{
		OpeningHours::parse(&s)
			.map_err(ParseDateTimeRangeError)
			.map_err(E::custom)
			.map(|oh| DateTimeRange { oh, s })
	}

	fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
	where
		E: de::Error,
	{
		OpeningHours::parse(s)
			.map_err(ParseDateTimeRangeError)
			.map_err(E::custom)
			.map(|oh| DateTimeRange {
				oh,
				s: s.to_owned(),
			})
	}
}

#[cfg(feature = "serde")]
impl<'de> Deserialize<'de> for DateTimeRange {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: serde::Deserializer<'de>,
	{
		deserializer.deserialize_string(DateTimeRangeVisitor)
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, FromOsmStr, ToOsmStr)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum ConditionOperator {
	#[osm_str("<")]
	Less,
	#[osm_str("<=")]
	LessEqual,
	#[osm_str("=")]
	Equal,
	#[osm_str(">")]
	Greater,
	#[osm_str(">=")]
	GreaterEqual,
}

impl ConditionOperator {
	fn parse(s: &str) -> Option<(&str, Self, &str)> {
		let mut iter = s.as_bytes().iter().enumerate();
		while let Some((i, b)) = iter.next() {
			match b {
				b'<' => {
					return Some(if let Some((i2, b'=')) = iter.next() {
						(&s[..i], Self::LessEqual, &s[i2 + 1..])
					} else {
						(&s[..i], Self::Less, &s[i + 1..])
					});
				}
				b'=' => {
					return Some((&s[..i], Self::Equal, &s[i + 1..]));
				}
				b'>' => {
					return Some(if let Some((i2, b'=')) = iter.next() {
						(&s[..i], Self::GreaterEqual, &s[i2 + 1..])
					} else {
						(&s[..i], Self::Greater, &s[i + 1..])
					});
				}
				_ => {}
			}
		}
		None
	}

	#[must_use]
	pub const fn is_ordering(&self, ord: &Ordering) -> bool {
		match self {
			Self::Less => ord.is_lt(),
			Self::LessEqual => ord.is_le(),
			Self::Equal => ord.is_eq(),
			Self::Greater => ord.is_gt(),
			Self::GreaterEqual => ord.is_ge(),
		}
	}
}

pub struct MatchSituation<'a> {
	pub date_time: NaiveDateTime,
	pub location: Location,
	pub vehicle: &'a Vehicle,
	pub stay: Option<Quantity<Duration>>,
	pub wet: bool,
	pub snow: bool,
	pub access_levels: Vec<AccessLevel>,
}

impl<'a> MatchSituation<'a> {
	fn is_summer(&self) -> bool {
		let northern_hemisphere = self.location.lat > 0.0;
		if northern_hemisphere {
			(4..11).contains(&self.date_time.month())
		} else {
			!(3..10).contains(&self.date_time.month())
		}
	}
}

#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum ConditionCompare {
	Weight(Quantity<Weight>),
	Weightrating(Quantity<Weight>),
	Weightcapacity(Quantity<Weight>),
	Emptyweight(Quantity<Weight>),
	Axleload(Quantity<Weight>),
	Length(Quantity<Distance>),
	Width(Quantity<Distance>),
	Height(Quantity<Distance>),
	Wheels(u32),
	Draught(Quantity<Distance>),
	Occupants(u32),
	Stay(Quantity<Duration>),
	Seats(u32),
	Axles(u32),
	Trailers(u32),
	TrailerWeight(Quantity<Weight>),
}

impl ConditionCompare {
	fn parse(unit: &str, value: &str) -> Option<Self> {
		match unit {
			"weight" => FromOsmStr::from_osm_str(value).map(ConditionCompare::Weight),
			"weightrating" => FromOsmStr::from_osm_str(value).map(ConditionCompare::Weightrating),
			"weightcapacity" => {
				FromOsmStr::from_osm_str(value).map(ConditionCompare::Weightcapacity)
			}
			"emptyweight" => FromOsmStr::from_osm_str(value).map(ConditionCompare::Emptyweight),
			"axleload" => FromOsmStr::from_osm_str(value).map(ConditionCompare::Axleload),
			"length" => FromOsmStr::from_osm_str(value).map(ConditionCompare::Length),
			"width" => FromOsmStr::from_osm_str(value).map(ConditionCompare::Width),
			"height" => FromOsmStr::from_osm_str(value).map(ConditionCompare::Height),
			"wheels" => FromOsmStr::from_osm_str(value).map(ConditionCompare::Wheels),
			"draught" => FromOsmStr::from_osm_str(value).map(ConditionCompare::Draught),
			"occupants" => FromOsmStr::from_osm_str(value).map(ConditionCompare::Occupants),
			"stay" => FromOsmStr::from_osm_str(value).map(ConditionCompare::Stay),
			"seats" => FromOsmStr::from_osm_str(value).map(ConditionCompare::Seats),
			"axles" => FromOsmStr::from_osm_str(value).map(ConditionCompare::Axles),
			"trailers" => FromOsmStr::from_osm_str(value).map(ConditionCompare::Trailers),
			"trailerweight" => FromOsmStr::from_osm_str(value).map(ConditionCompare::TrailerWeight),
			_ => None,
		}
	}

	fn to(&self) -> (&str, Option<Cow<'static, str>>) {
		match self {
			Self::Weight(value) => ("weight", value.to_osm_str()),
			Self::Weightrating(value) => ("weightrating", value.to_osm_str()),
			Self::Weightcapacity(value) => ("weightcapacity", value.to_osm_str()),
			Self::Emptyweight(value) => ("emptyweight", value.to_osm_str()),
			Self::Axleload(value) => ("axleload", value.to_osm_str()),
			Self::Length(value) => ("length", value.to_osm_str()),
			Self::Width(value) => ("width", value.to_osm_str()),
			Self::Height(value) => ("height", value.to_osm_str()),
			Self::Wheels(value) => ("wheels", value.to_osm_str()),
			Self::Draught(value) => ("draught", value.to_osm_str()),
			Self::Occupants(value) => ("occupants", value.to_osm_str()),
			Self::Stay(value) => ("stay", value.to_osm_str()),
			Self::Seats(value) => ("seats", value.to_osm_str()),
			Self::Axles(value) => ("axles", value.to_osm_str()),
			Self::Trailers(value) => ("trailers", value.to_osm_str()),
			Self::TrailerWeight(value) => ("trailerweight", value.to_osm_str()),
		}
	}

	fn situation_cmp(&self, situation: &MatchSituation) -> Option<Ordering> {
		Some(match self {
			Self::Weight(value) => situation.vehicle.weight?.cmp(value),
			Self::Weightrating(value) => situation.vehicle.weightrating?.cmp(value),
			Self::Weightcapacity(value) => situation.vehicle.weightcapacity?.cmp(value),
			Self::Emptyweight(value) => situation.vehicle.emptyweight?.cmp(value),
			Self::Axleload(value) => situation.vehicle.axleload?.cmp(value),
			Self::Length(value) => situation.vehicle.length?.cmp(value),
			Self::Width(value) => situation.vehicle.width?.cmp(value),
			Self::Height(value) => situation.vehicle.height?.cmp(value),
			Self::Wheels(value) => situation.vehicle.wheels?.cmp(value),
			Self::Draught(value) => situation.vehicle.draught?.cmp(value),
			Self::Occupants(value) => situation.vehicle.occupants?.cmp(value),
			Self::Stay(value) => situation.stay?.partial_cmp(value)?,
			Self::Seats(value) => situation.vehicle.seats?.cmp(value),
			Self::Axles(value) => situation.vehicle.axles?.cmp(value),
			Self::Trailers(value) => situation.vehicle.trailers?.cmp(value),
			Self::TrailerWeight(value) => situation.vehicle.trailerweight?.cmp(value),
		})
	}
}

#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum Condition {
	DateTimeRange(Box<DateTimeRange>),
	Winter,
	Summer,
	Wet,
	Snow,
	Compare(ConditionOperator, ConditionCompare),
	AccessLevel(AccessLevel),
	Trailer,
	Caravan,
	Articulated,
}

impl Condition {
	fn parse_compare(s: &str) -> Option<Self> {
		let (left, operator, right) = ConditionOperator::parse(s)?;
		let compare = ConditionCompare::parse(left.trim_end(), right.trim_start())?;
		Some(Self::Compare(operator, compare))
	}
}

impl FromOsmStr for Condition {
	fn from_osm_str(s: &str) -> Option<Self> {
		let s = remove_parens(s);
		Some(match s {
			"winter" => Self::Winter,
			"summer" => Self::Summer,
			"wet" => Self::Wet,
			"snow" => Self::Snow,
			"trailer" => Self::Trailer,
			"caravan" => Self::Caravan,
			"articulated" => Self::Articulated,
			_ => {
				if let Some(access_level) = AccessLevel::from_osm_str(s) {
					Self::AccessLevel(access_level)
				} else if let Some(range) = DateTimeRange::from_osm_str(s) {
					Self::DateTimeRange(Box::new(range))
				} else {
					Self::parse_compare(s)?
				}
			}
		})
	}
}

impl ToOsmStr for Condition {
	fn to_osm_str(self) -> Option<Cow<'static, str>> {
		Some(
			match self {
				Self::Winter => "winter",
				Self::Summer => "summer",
				Self::Wet => "wet",
				Self::Snow => "snow",
				Self::Trailer => "trailer",
				Self::Caravan => "caravan",
				Self::Articulated => "articulated",
				Self::DateTimeRange(range) => return range.to_osm_str(),
				Self::Compare(operator, compare) => {
					let (left, right) = compare.to();
					return Some(format!("{left}{}{}", operator.to_osm_str()?, right?).into());
				}
				Self::AccessLevel(access_level) => return access_level.to_osm_str(),
			}
			.into(),
		)
	}
}

impl Condition {
	#[must_use]
	pub fn matches(&self, situation: &MatchSituation) -> bool {
		match self {
			Self::DateTimeRange(date_times) => date_times.matches(situation.date_time),
			Self::Winter => !situation.is_summer(),
			Self::Summer => situation.is_summer(),
			Self::Wet => situation.wet,
			Self::Snow => situation.snow,
			Self::Compare(op, value) => value
				.situation_cmp(situation)
				.map_or(true, |cmp| op.is_ordering(&cmp)),
			Self::AccessLevel(access_level) => situation.access_levels.contains(access_level),
			Self::Trailer => situation.vehicle.trailers != Some(0),
			Self::Caravan => situation.vehicle.caravan.unwrap_or(true),
			Self::Articulated => situation.vehicle.articulated.unwrap_or(true),
		}
	}
}

#[cfg(test)]
mod tests {
	use chrono::{NaiveDate, Timelike};

	use crate::{
		conditional::{remove_parens, Conditional, MatchSituation},
		get_tag, new_tag, quantity,
		units::{Distance, Duration, Quantity, WalkSpeed, Weight},
		FromOsm, Location, TMode, Vehicle,
	};

	#[test]
	fn complex_conditional() {
		let tags = new_tag! {
			fee = "yes",
			fee:conditional = "no @ stay < 2 hours AND (Mo-Fr 07:30-22:30; Sa 07:30-20:30; Su,PH 11:30-22:30)",
		};

		let fee_tags = get_tag!(tags, fee:).unwrap();
		let conditional = Conditional::from_osm(fee_tags).unwrap();

		assert_eq!(conditional.len(), 2);
		assert_eq!(conditional[0].value, true);
		assert_eq!(conditional[0].conditions.len(), 0);
		assert_eq!(conditional[1].value, false);
		assert_eq!(conditional[1].conditions.len(), 2);

		let vehicle = Vehicle {
			mode: TMode::Vehicle,
			wheels: Some(4),
			occupants: Some(1),
			seats: Some(5),
			axles: Some(2),
			weight: Some(quantity!(2 Weight::Tonne)),
			weightrating: Some(quantity!(2.5 Weight::Tonne)),
			weightcapacity: Some(quantity!(2.5 Weight::Tonne)),
			emptyweight: Some(quantity!(1.8 Weight::Tonne)),
			axleload: Some(quantity!(1 Weight::Tonne)),
			length: Some(quantity!(5 Distance::Metre)),
			width: Some(quantity!(2 Distance::Metre)),
			height: Some(quantity!(2 Distance::Metre)),
			draught: Some(quantity!(0 Distance::Metre)),
			trailers: Some(0),
			trailerweight: Some(quantity!(0 Weight::Tonne)),
			caravan: Some(false),
			articulated: Some(false),
		};

		let mut situation = MatchSituation {
			date_time: NaiveDate::from_ymd_opt(2020, 4, 11)
				.unwrap()
				.and_hms_opt(12, 43, 54)
				.unwrap(),
			location: Location::new(0.0, 0.0),
			vehicle: &vehicle,
			stay: Some(quantity!(4 Duration::Hour)),
			snow: false,
			wet: false,
			access_levels: Vec::new(),
		};

		assert_eq!(conditional.evaluate(&situation), Some(&true));

		situation.stay = Some(quantity!(1 Duration::Hour));
		assert_eq!(conditional.evaluate(&situation), Some(&false));

		situation.date_time = situation.date_time.with_hour(23).unwrap();
		assert_eq!(conditional.evaluate(&situation), Some(&true));
	}

	#[test]
	fn parens_wrapping_and() {
		let tags = new_tag! {
			maxspeed = "100",
			maxspeed:conditional = "60 @ (22:00-05:00 AND weight>7.5)",
		};

		let maxspeed_tags = get_tag!(tags, maxspeed:).unwrap();
		let conditional: Conditional<Quantity<WalkSpeed>> =
			Conditional::from_osm(maxspeed_tags).unwrap();

		assert_eq!(conditional.len(), 2);
		assert_eq!(conditional[0].conditions.len(), 0);
		assert_eq!(conditional[1].conditions.len(), 2);
	}

	#[test]
	fn parens_inside_string() {
		let tags = new_tag! {
			maxspeed = "30",
			maxspeed:conditional = "20 @ (Sa,Su \"weekend );\")",
		};

		let maxspeed_tags = get_tag!(tags, maxspeed:).unwrap();
		let conditional: Conditional<Quantity<WalkSpeed>> =
			Conditional::from_osm(maxspeed_tags).unwrap();

		assert_eq!(conditional.len(), 2);
		assert_eq!(conditional[0].conditions.len(), 0);
		assert_eq!(conditional[1].conditions.len(), 1);
	}

	#[test]
	fn parens_closing() {
		let tags = new_tag! {
			maxspeed = "90",
			maxspeed:conditional = "80 @ wet)",
		};

		let maxspeed_tags = get_tag!(tags, maxspeed:).unwrap();
		let conditional: Conditional<Quantity<WalkSpeed>> =
			Conditional::from_osm(maxspeed_tags).unwrap();

		assert_eq!(conditional.len(), 1);
	}

	#[test]
	fn parens_removal() {
		assert_eq!(remove_parens("(ab)"), "ab");
		assert_eq!(remove_parens(" (ab) "), "ab");
		assert_eq!(remove_parens("((a)b(c))"), "(a)b(c)");
		assert_eq!(remove_parens("((ab))"), "ab");
		assert_eq!(remove_parens("ab)c"), "ab)c");
		assert_eq!(remove_parens("c)"), "c)");
		assert_eq!(remove_parens(")ab"), ")ab");
		assert_eq!(remove_parens("a)b"), "a)b");
		assert_eq!(remove_parens("a(b"), "a(b");
		assert_eq!(remove_parens("a("), "a(");
	}

	#[test]
	fn parens_closing_inside() {
		let tags = new_tag! {
			maxspeed = "70 mph",
			maxspeed:conditional = "65 mph @ ((sunset+01:30)-(sunrise-01:30))",
		};

		let maxspeed_tags = get_tag!(tags, maxspeed:).unwrap();
		let conditional: Conditional<Quantity<WalkSpeed>> =
			Conditional::from_osm(maxspeed_tags).unwrap();

		assert_eq!(conditional.len(), 2);
		assert_eq!(conditional[0].conditions.len(), 0);
		assert_eq!(conditional[1].conditions.len(), 1);
	}
}
