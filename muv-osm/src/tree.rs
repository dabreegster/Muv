#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

use crate::{
	get_tag, simple_enum,
	tags::Tag,
	units::{Distance, Unit, PI},
	FromOsm, FromOsmStr, ToOsmStr,
};

#[derive(Debug, Clone)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Tree {
	pub leaf_type: Option<LeafType>,
	pub leaf_cycle: Option<LeafCycle>,

	pub height: Option<Unit<Distance>>,

	pub diameter_trunk: Option<Unit<Distance>>,
	pub diameter_crown: Option<Unit<Distance>>,

	pub taxon: Option<Taxon>,
}

impl<'a> FromOsm<'a> for Tree {
	fn from_osm(tag: &'a Tag) -> Option<Self> {
		let taxon = Taxon::parse(tag);

		Some(Self {
			leaf_type: get_tag!(tag, leaf_type)
				.and_then(LeafType::from_osm_str)
				.or_else(|| taxon.as_ref().map(Taxon::leaf_type)),
			leaf_cycle: get_tag!(tag, leaf_cycle)
				.and_then(LeafCycle::from_osm_str)
				.or_else(|| taxon.as_ref().map(Taxon::leaf_cycle)),

			height: get_tag!(tag, height).and_then(Unit::from_osm_str),

			diameter_trunk: get_tag!(tag, diameter)
				.and_then(|s| Unit::from_osm_str_with_unit(s, Distance::Millimetre))
				.or_else(|| Some(Unit::from_osm_str(get_tag!(tag, circumference)?)? / PI)),
			diameter_crown: get_tag!(tag, diameter_crown).and_then(Unit::from_osm_str),

			taxon,
		})
	}
}

simple_enum!(LeafType {
	Broadleaved,
	Needleleaved,
	Mixed,
	Leafless,
});

simple_enum!(LeafCycle {
	Evergreen,
	Deciduous,
	SemiEvergreen,
	SemiDeciduous,
	Mixed,
});

macro_rules! defined_groups {
    ($name:ident { $($child:ident ,)* }) => {
        #[derive(Debug, Clone, FromOsmStr, ToOsmStr)]
        #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
        pub enum $name {
            $($child),*
        }
    };
    ($name:ident { $($child:ident { $($subchild:ident ,)* } ,)* }) => {
        defined_groups!(@internal $name { $($child { $($subchild ,)* } ,)* });

        impl FromOsmStr for $name {
            fn from_osm_str(s: &str) -> Option<Self> {
                let mut parts = s.split(' ');
                Some(match parts.next()? {
                    $(stringify!($child) => Self::$child(parts.next().and_then(FromOsmStr::from_osm_str)),)*
                    _ => return None
                })
            }
        }
    };
    ($name:ident { $($child:ident { $($rest:tt)* } ,)* }) => {
        defined_groups!(@internal $name { $($child { $($rest)* } ,)* });

        impl FromOsmStr for $name {
            fn from_osm_str(s: &str) -> Option<Self> {
                Some(match s {
                    $(stringify!($child) => $name::$child(None),)*
                    _ => {
                        $(if let Some(val) = $child::from_osm_str(s) {
                            $name::$child(Some(val))
                        } else)* {
                            return None;
                        }
                    }
                })
            }
        }
    };
    (@internal $name:ident { $($child:ident { $($rest:tt)* } ,)* }) => {
        #[derive(Debug, Clone)]
        #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
        pub enum $name {
            $($child(Option<$child>)),*
        }

        $(
        impl From<$child> for Taxon {
            fn from(value: $child) -> Self {
                $name::$child(Some(value)).into()
            }
        }

        defined_groups!($child { $($rest)* });
        )*
    };
}

defined_groups! { Taxon {

Angiosperms {
	Dicotyledons {
		Aquifoliaceae {
			Ilex {},
		},
		Betulaceae {
			Alnus {},
			Betula {},
			Carpinus {},
			Corylus {},
		},
		Cannabaceae {
			Celtis {
				Australis,
			},
		},
		Fabaceae {
			Robinia {},
		},
		Fagaceae {
			Castanea {},
			Fagus {},
			Quercus {
				Robur,
				Virginiana,
			},
		},
		Juglandaceae {
			Juglans {
				Regia,
			},
		},
		Malvaceae {
			Tilia {
				Cordata,
				Platyphyllos,
			},
		},
		Meliaceae {
			Khaya {},
		},
		Myrtaceae {
			Eucalyptus {},
		},
		Oleaceae {
			Fraxinus {
				Excelsior,
			},
		},
		Platanaceae {
			Platanus {},
		},
		Rosaceae {
			Malus {},
			Prunus {},
			Pyrus {},
			Sorbus {},
		},
		Salicaceae {
			Populus {},
		},
		Sapindaceae {
			Acer {
				Campestre,
				Negundo,
				Platanoides,
				Pseudoplatanus,
				Rubrum,
				Saccharinum,
				Saccharum,
			},
			Aesculus {
				Hippocastanum,
			},
		},
		Ulmaceae {
			Ulmus {},
		},
	},
	Monocotyledons {
		Agavaceae {},
		Arecaceae {
			Cocos {
				Nucifera,
			},
		},
		Poaceae {},
	},
},
Pinophyta {
	Araucariaceae {
		Araucaria {},
	},
	Pinaceae {
		Abies {},
		Cedrus {},
		Larix {},
		Picea {},
		Pinus {
			Mugo,
		},
		Pseudotsuga {},
	},
},

}}

impl Taxon {
	#[must_use]
	pub fn parse(tags: &Tag) -> Option<Self> {
		get_tag! {tags, species:}
			.and_then(Self::parse_species)
			.or_else(|| Self::parse_genus(get_tag! {tags, genus:}?))
	}

	#[must_use]
	pub fn parse_genus(tags: &Tag) -> Option<Self> {
		tags.value()
			.and_then(Self::parse_genus_str)
			.or_else(|| Self::parse_genus_str_en(get_tag!(tags, en)?))
			.or_else(|| Self::parse_genus_str_de(get_tag!(tags, de)?))
	}

	#[must_use]
	pub fn parse_genus_str(s: &str) -> Option<Self> {
		Some(match s {
			"Abies" => Pinaceae::Abies(None).into(),
			"Acer" => Sapindaceae::Acer(None).into(),
			"Aesculus" => Sapindaceae::Aesculus(None).into(),
			"Alnus" => Betulaceae::Alnus(None).into(),
			"Araucaria" => Araucariaceae::Araucaria(None).into(),
			"Betula" => Betulaceae::Betula(None).into(),
			"Carpinus" => Betulaceae::Carpinus(None).into(),
			"Castanea" => Fagaceae::Castanea(None).into(),
			"Cedrus" => Pinaceae::Cedrus(None).into(),
			"Celtis" => Cannabaceae::Celtis(None).into(),
			"Cocos" => Arecaceae::Cocos(None).into(),
			"Corylus" => Betulaceae::Corylus(None).into(),
			"Eucalyptus" => Myrtaceae::Eucalyptus(None).into(),
			"Fagus" => Fagaceae::Fagus(None).into(),
			"Fraxinus" => Oleaceae::Fraxinus(None).into(),
			"Ilex" => Aquifoliaceae::Ilex(None).into(),
			"Juglans" => Juglandaceae::Juglans(None).into(),
			"Khaya" => Meliaceae::Khaya(None).into(),
			"Larix" => Pinaceae::Larix(None).into(),
			"Malus" => Rosaceae::Malus(None).into(),
			"Picea" => Pinaceae::Picea(None).into(),
			"Pinus" => Pinaceae::Pinus(None).into(),
			"Platanus" => Platanaceae::Platanus(None).into(),
			"Populus" => Salicaceae::Populus(None).into(),
			"Prunus" => Rosaceae::Prunus(None).into(),
			"Pseudotsuga" => Pinaceae::Pseudotsuga(None).into(),
			"Pyrus" => Rosaceae::Pyrus(None).into(),
			"Quercus" => Fagaceae::Quercus(None).into(),
			"Robinia" => Fabaceae::Robinia(None).into(),
			"Sorbus" => Rosaceae::Sorbus(None).into(),
			"Tilia" => Malvaceae::Tilia(None).into(),
			"Ulmus" => Ulmaceae::Ulmus(None).into(),
			_ => return None,
		})
	}

	#[must_use]
	pub fn parse_genus_str_en(s: &str) -> Option<Self> {
		Some(match s {
			"Maple" => Sapindaceae::Acer(None).into(),
			"Birch" => Betulaceae::Betula(None).into(),
			"Oak" => Fagaceae::Quercus(None).into(),
			"Lime" => Malvaceae::Tilia(None).into(),
			_ => return None,
		})
	}

	#[must_use]
	pub fn parse_genus_str_de(s: &str) -> Option<Self> {
		Some(match s {
			"Linde" => Malvaceae::Tilia(None).into(),
			_ => return None,
		})
	}

	#[must_use]
	pub fn parse_species(tags: &Tag) -> Option<Self> {
		tags.value()
			.and_then(Self::parse_genus_str)
			.or_else(|| Self::parse_genus_str_en(get_tag!(tags, en)?))
	}

	#[must_use]
	pub fn parse_species_str(s: &str) -> Option<Self> {
		Some(match s {
			"Acer campestre" => Acer::Campestre.into(),
			"Acer negundo" => Acer::Negundo.into(),
			"Acer platanoides" => Acer::Platanoides.into(),
			"Acer pseudoplatanus" => Acer::Pseudoplatanus.into(),
			"Acer rubrum" => Acer::Rubrum.into(),
			"Acer saccharinum" => Acer::Saccharinum.into(),
			"Acer saccharum" => Acer::Saccharum.into(),

			"Aesculus hippocastanum" => Aesculus::Hippocastanum.into(),

			"Celtis australis" => Celtis::Australis.into(),

			"Cocos nucifera" => Cocos::Nucifera.into(),

			"Fraxinus excelsior" => Fraxinus::Excelsior.into(),

			"Juglans regia" => Juglans::Regia.into(),

			"Pinus mugo" => Pinus::Mugo.into(),

			"Tilia cordata" => Tilia::Cordata.into(),
			"Tilia platyphyllos" => Tilia::Platyphyllos.into(),

			"Quercus robur" => Quercus::Robur.into(),
			"Quercus virginiana" => Quercus::Virginiana.into(),

			_ => return None,
		})
	}

	#[must_use]
	pub fn parse_species_str_en(s: &str) -> Option<Self> {
		Some(match s.to_ascii_lowercase().as_str() {
			"field maple" => Acer::Campestre.into(),
			"box elder" | "boxelder maple" | "manitoba maple" => Acer::Negundo.into(),
			"norway maple" => Acer::Platanoides.into(),
			"sycamore" | "sycamore maple" => Acer::Pseudoplatanus.into(),
			"red maple" => Acer::Rubrum.into(),
			"silver maple" => Acer::Saccharinum.into(),
			"sugar maple" => Acer::Saccharum.into(),

			_ => return None,
		})
	}

	#[must_use]
	pub const fn leaf_cycle(&self) -> LeafCycle {
		match self {
			Self::Angiosperms(Some(
				Angiosperms::Dicotyledons(Some(
					Dicotyledons::Aquifoliaceae(_)
					| Dicotyledons::Fagaceae(Some(Fagaceae::Quercus(Some(Quercus::Virginiana))))
					| Dicotyledons::Myrtaceae(Some(Myrtaceae::Eucalyptus(_))),
				))
				| Angiosperms::Monocotyledons(Some(Monocotyledons::Arecaceae(_))),
			)) => LeafCycle::Evergreen,
			Self::Angiosperms(_)
			| Self::Pinophyta(Some(Pinophyta::Pinaceae(Some(Pinaceae::Larix(_))))) => LeafCycle::Deciduous,
			Self::Pinophyta(_) => LeafCycle::Evergreen,
		}
	}

	#[must_use]
	pub const fn leaf_type(&self) -> LeafType {
		match self {
			Self::Angiosperms(_) => LeafType::Broadleaved,
			Self::Pinophyta(_) => LeafType::Needleleaved,
		}
	}
}
