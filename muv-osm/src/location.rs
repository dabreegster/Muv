use std::fmt::Display;

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[cfg(feature = "geo-types")]
use geo_types::geometry::Coord;

#[derive(Debug, Clone, Copy, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Location {
	pub lat: f64,
	pub lon: f64,
}

impl Display for Location {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}, {}", self.lat, self.lon)
	}
}

impl Location {
	#[must_use]
	pub const fn new(lat: f64, lon: f64) -> Self {
		Self { lat, lon }
	}
}

impl From<[f64; 2]> for Location {
	fn from([lat, lon]: [f64; 2]) -> Self {
		Self { lat, lon }
	}
}

impl From<(f64, f64)> for Location {
	fn from((lat, lon): (f64, f64)) -> Self {
		Self { lat, lon }
	}
}

#[cfg(feature = "geo-types")]
impl From<Coord> for Location {
	fn from(value: Coord) -> Self {
		Self {
			lat: value.y,
			lon: value.x,
		}
	}
}
