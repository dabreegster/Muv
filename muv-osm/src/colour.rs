use std::borrow::Cow;

#[cfg(feature = "serde")]
use serde::{
	de::{Error, Visitor},
	Deserialize, Serialize,
};

use crate::{get_tag, simple_enum, FromOsmStr, Taglike, ToOsmStr};

#[cfg(feature = "wasm-bindgen")]
use wasm_bindgen::prelude::wasm_bindgen;

/// Colour represented by either a [8-bit RGB value](`Colour::Rgb`) or a [CSS named colour](`Colour::Name`)
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize), serde(untagged))]
pub enum Colour {
	Rgb(Rgb),
	Name(ColourName),
}

impl Colour {
	#[must_use]
	pub const fn to_rgb(self) -> Rgb {
		match self {
			Self::Rgb(rgb) => rgb,
			Self::Name(name) => name.to_rgb(),
		}
	}

	pub(crate) fn parse_tags<'a, T: Taglike<'a>>(tags: T) -> Option<Self> {
		Self::from_osm_str(get_tag!(tags, colour)?)
	}
}

impl FromOsmStr for Colour {
	fn from_osm_str(s: &str) -> Option<Self> {
		match s.strip_prefix('#') {
			Some(s) => Rgb::from_osm_str_without_prefix(s).map(Self::Rgb),
			None => ColourName::from_osm_str(s).map(Self::Name),
		}
	}
}

impl ToOsmStr for Colour {
	fn to_osm_str(self) -> Option<Cow<'static, str>> {
		match self {
			Self::Rgb(rgb) => rgb.to_osm_str(),
			Self::Name(name) => name.to_osm_str(),
		}
	}
}

/// 8-bit RGB colour value
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[cfg_attr(feature = "wasm-bindgen", wasm_bindgen)]
pub struct Rgb {
	pub red: u8,
	pub green: u8,
	pub blue: u8,
}

impl Rgb {
	#[inline]
	#[must_use]
	pub const fn new(red: u8, green: u8, blue: u8) -> Self {
		Self { red, green, blue }
	}
}

impl FromOsmStr for Rgb {
	fn from_osm_str(s: &str) -> Option<Self> {
		s.strip_prefix('#')
			.and_then(Self::from_osm_str_without_prefix)
	}
}

impl ToOsmStr for Rgb {
	fn to_osm_str(self) -> Option<Cow<'static, str>> {
		Some(format!("#{:02x}{:02x}{:02x}", self.red, self.blue, self.green).into())
	}
}

#[must_use]
const fn hex_digit(c: u8) -> Option<u8> {
	Some(match c {
		b'0'..=b'9' => c - b'0',
		b'A'..=b'F' => c - b'A' + 10,
		b'a'..=b'f' => c - b'a' + 10,
		_ => return None,
	})
}

impl Rgb {
	fn from_osm_str_without_prefix(s: &str) -> Option<Self> {
		Some(match s.as_bytes() {
			[r, g, b] => Self::new(
				hex_digit(*r)? * 17,
				hex_digit(*g)? * 17,
				hex_digit(*b)? * 17,
			),
			[r1, r2, g1, g2, b1, b2] => Self::new(
				hex_digit(*r1)? * 16 + hex_digit(*r2)?,
				hex_digit(*g1)? * 16 + hex_digit(*g2)?,
				hex_digit(*b1)? * 16 + hex_digit(*b2)?,
			),
			_ => return None,
		})
	}
}

#[cfg(feature = "serde")]
impl Serialize for Rgb {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: serde::Serializer,
	{
		if serializer.is_human_readable() {
			format!("#{:x}{:x}{:x}", self.red, self.green, self.blue).serialize(serializer)
		} else {
			(self.red, self.green, self.blue).serialize(serializer)
		}
	}
}

#[cfg(feature = "serde")]
struct RgbVisitor;

#[cfg(feature = "serde")]
impl<'de> Visitor<'de> for RgbVisitor {
	type Value = Rgb;

	fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
		formatter.write_str("a 6-digit hex string prefixed with # or a u8 tuple of length 3")
	}

	fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
	where
		E: serde::de::Error,
	{
		Rgb::from_osm_str(v).ok_or_else(|| E::custom("a 6-digit hex string prefixed with #"))
	}

	fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
	where
		A: serde::de::SeqAccess<'de>,
	{
		let red = seq
			.next_element()?
			.ok_or_else(|| Error::invalid_length(0, &"u8 tuple of length 3"))?;
		let green = seq
			.next_element()?
			.ok_or_else(|| Error::invalid_length(1, &"u8 tuple of length 3"))?;
		let blue = seq
			.next_element()?
			.ok_or_else(|| Error::invalid_length(2, &"u8 tuple of length 3"))?;

		Ok(Rgb { red, green, blue })
	}
}

#[cfg(feature = "serde")]
impl<'de> Deserialize<'de> for Rgb {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: serde::Deserializer<'de>,
	{
		if deserializer.is_human_readable() {
			deserializer.deserialize_str(RgbVisitor)
		} else {
			deserializer.deserialize_tuple(3, RgbVisitor)
		}
	}
}

simple_enum!(ColourName
/// A [CSS named colour](https://www.w3.org/TR/css-color-4/#named-colors)
#[allow(missing_docs)]
{
	Aliceblue,
	Antiquewhite,
	Aqua,
	Aquamarine,
	Azure,
	Beige,
	Bisque,
	Black,
	Blanchedalmond,
	Blue,
	Blueviolet,
	Brown,
	Burlywood,
	Cadetblue,
	Chartreuse,
	Chocolate,
	Coral,
	Cornflowerblue,
	Cornsilk,
	Crimson,
	Cyan,
	Darkblue,
	Darkcyan,
	Darkgoldenrod,
	Darkgray,
	Darkgreen,
	Darkgrey,
	Darkkhaki,
	Darkmagenta,
	Darkolivegreen,
	Darkorange,
	Darkorchid,
	Darkred,
	Darksalmon,
	Darkseagreen,
	Darkslateblue,
	Darkslategray,
	Darkslategrey,
	Darkturquoise,
	Darkviolet,
	Deeppink,
	Deepskyblue,
	Dimgray,
	Dimgrey,
	Dodgerblue,
	Firebrick,
	Floralwhite,
	Forestgreen,
	Fuchsia,
	Gainsboro,
	Ghostwhite,
	Gold,
	Goldenrod,
	Gray,
	Green,
	Greenyellow,
	Grey,
	Honeydew,
	Hotpink,
	Indianred,
	Indigo,
	Ivory,
	Khaki,
	Lavender,
	Lavenderblush,
	Lawngreen,
	Lemonchiffon,
	Lightblue,
	Lightcoral,
	Lightcyan,
	Lightgoldenrodyellow,
	Lightgray,
	Lightgreen,
	Lightgrey,
	Lightpink,
	Lightsalmon,
	Lightseagreen,
	Lightskyblue,
	Lightslategray,
	Lightslategrey,
	Lightsteelblue,
	Lightyellow,
	Lime,
	Limegreen,
	Linen,
	Magenta,
	Maroon,
	Mediumaquamarine,
	Mediumblue,
	Mediumorchid,
	Mediumpurple,
	Mediumseagreen,
	Mediumslateblue,
	Mediumspringgreen,
	Mediumturquoise,
	Mediumvioletred,
	Midnightblue,
	Mintcream,
	Mistyrose,
	Moccasin,
	Navajowhite,
	Navy,
	Oldlace,
	Olive,
	Olivedrab,
	Orange,
	Orangered,
	Orchid,
	Palegoldenrod,
	Palegreen,
	Paleturquoise,
	Palevioletred,
	Papayawhip,
	Peachpuff,
	Peru,
	Pink,
	Plum,
	Powderblue,
	Purple,
	Red,
	Rosybrown,
	Royalblue,
	Saddlebrown,
	Salmon,
	Sandybrown,
	Seagreen,
	Seashell,
	Sienna,
	Silver,
	Skyblue,
	Slateblue,
	Slategray,
	Slategrey,
	Snow,
	Springgreen,
	Steelblue,
	Tan,
	Teal,
	Thistle,
	Tomato,
	Turquoise,
	Violet,
	Wheat,
	White,
	Whitesmoke,
	Yellow,
	Yellowgreen,
});

impl ColourName {
	#[must_use]
	#[allow(clippy::match_same_arms, clippy::too_many_lines)]
	pub const fn to_rgb(self) -> Rgb {
		match self {
			Self::Aliceblue => Rgb::new(240, 248, 255),
			Self::Antiquewhite => Rgb::new(250, 235, 215),
			Self::Aqua => Rgb::new(0, 255, 255),
			Self::Aquamarine => Rgb::new(127, 255, 212),
			Self::Azure => Rgb::new(240, 255, 255),
			Self::Beige => Rgb::new(245, 245, 220),
			Self::Bisque => Rgb::new(255, 228, 196),
			Self::Black => Rgb::new(0, 0, 0),
			Self::Blanchedalmond => Rgb::new(255, 235, 205),
			Self::Blue => Rgb::new(0, 0, 255),
			Self::Blueviolet => Rgb::new(138, 43, 226),
			Self::Brown => Rgb::new(165, 42, 42),
			Self::Burlywood => Rgb::new(222, 184, 135),
			Self::Cadetblue => Rgb::new(95, 158, 160),
			Self::Chartreuse => Rgb::new(127, 255, 0),
			Self::Chocolate => Rgb::new(210, 105, 30),
			Self::Coral => Rgb::new(255, 127, 80),
			Self::Cornflowerblue => Rgb::new(100, 149, 237),
			Self::Cornsilk => Rgb::new(255, 248, 220),
			Self::Crimson => Rgb::new(220, 20, 60),
			Self::Cyan => Rgb::new(0, 255, 255),
			Self::Darkblue => Rgb::new(0, 0, 139),
			Self::Darkcyan => Rgb::new(0, 139, 139),
			Self::Darkgoldenrod => Rgb::new(184, 134, 11),
			Self::Darkgray => Rgb::new(169, 169, 169),
			Self::Darkgreen => Rgb::new(0, 100, 0),
			Self::Darkgrey => Rgb::new(169, 169, 169),
			Self::Darkkhaki => Rgb::new(189, 183, 107),
			Self::Darkmagenta => Rgb::new(139, 0, 139),
			Self::Darkolivegreen => Rgb::new(85, 107, 47),
			Self::Darkorange => Rgb::new(255, 140, 0),
			Self::Darkorchid => Rgb::new(153, 50, 204),
			Self::Darkred => Rgb::new(139, 0, 0),
			Self::Darksalmon => Rgb::new(233, 150, 122),
			Self::Darkseagreen => Rgb::new(143, 188, 143),
			Self::Darkslateblue => Rgb::new(72, 61, 139),
			Self::Darkslategray => Rgb::new(47, 79, 79),
			Self::Darkslategrey => Rgb::new(47, 79, 79),
			Self::Darkturquoise => Rgb::new(0, 206, 209),
			Self::Darkviolet => Rgb::new(148, 0, 211),
			Self::Deeppink => Rgb::new(255, 20, 147),
			Self::Deepskyblue => Rgb::new(0, 191, 255),
			Self::Dimgray => Rgb::new(105, 105, 105),
			Self::Dimgrey => Rgb::new(105, 105, 105),
			Self::Dodgerblue => Rgb::new(30, 144, 255),
			Self::Firebrick => Rgb::new(178, 34, 34),
			Self::Floralwhite => Rgb::new(255, 250, 240),
			Self::Forestgreen => Rgb::new(34, 139, 34),
			Self::Fuchsia => Rgb::new(255, 0, 255),
			Self::Gainsboro => Rgb::new(220, 220, 220),
			Self::Ghostwhite => Rgb::new(248, 248, 255),
			Self::Gold => Rgb::new(255, 215, 0),
			Self::Goldenrod => Rgb::new(218, 165, 32),
			Self::Gray => Rgb::new(128, 128, 128),
			Self::Green => Rgb::new(0, 128, 0),
			Self::Greenyellow => Rgb::new(173, 255, 47),
			Self::Grey => Rgb::new(128, 128, 128),
			Self::Honeydew => Rgb::new(240, 255, 240),
			Self::Hotpink => Rgb::new(255, 105, 180),
			Self::Indianred => Rgb::new(205, 92, 92),
			Self::Indigo => Rgb::new(75, 0, 130),
			Self::Ivory => Rgb::new(255, 255, 240),
			Self::Khaki => Rgb::new(240, 230, 140),
			Self::Lavender => Rgb::new(230, 230, 250),
			Self::Lavenderblush => Rgb::new(255, 240, 245),
			Self::Lawngreen => Rgb::new(124, 252, 0),
			Self::Lemonchiffon => Rgb::new(255, 250, 205),
			Self::Lightblue => Rgb::new(173, 216, 230),
			Self::Lightcoral => Rgb::new(240, 128, 128),
			Self::Lightcyan => Rgb::new(224, 255, 255),
			Self::Lightgoldenrodyellow => Rgb::new(250, 250, 210),
			Self::Lightgray => Rgb::new(211, 211, 211),
			Self::Lightgreen => Rgb::new(144, 238, 144),
			Self::Lightgrey => Rgb::new(211, 211, 211),
			Self::Lightpink => Rgb::new(255, 182, 193),
			Self::Lightsalmon => Rgb::new(255, 160, 122),
			Self::Lightseagreen => Rgb::new(32, 178, 170),
			Self::Lightskyblue => Rgb::new(135, 206, 250),
			Self::Lightslategray => Rgb::new(119, 136, 153),
			Self::Lightslategrey => Rgb::new(119, 136, 153),
			Self::Lightsteelblue => Rgb::new(176, 196, 222),
			Self::Lightyellow => Rgb::new(255, 255, 224),
			Self::Lime => Rgb::new(0, 255, 0),
			Self::Limegreen => Rgb::new(50, 205, 50),
			Self::Linen => Rgb::new(250, 240, 230),
			Self::Magenta => Rgb::new(255, 0, 255),
			Self::Maroon => Rgb::new(128, 0, 0),
			Self::Mediumaquamarine => Rgb::new(102, 205, 170),
			Self::Mediumblue => Rgb::new(0, 0, 205),
			Self::Mediumorchid => Rgb::new(186, 85, 211),
			Self::Mediumpurple => Rgb::new(147, 112, 219),
			Self::Mediumseagreen => Rgb::new(60, 179, 113),
			Self::Mediumslateblue => Rgb::new(123, 104, 238),
			Self::Mediumspringgreen => Rgb::new(0, 250, 154),
			Self::Mediumturquoise => Rgb::new(72, 209, 204),
			Self::Mediumvioletred => Rgb::new(199, 21, 133),
			Self::Midnightblue => Rgb::new(25, 25, 112),
			Self::Mintcream => Rgb::new(245, 255, 250),
			Self::Mistyrose => Rgb::new(255, 228, 225),
			Self::Moccasin => Rgb::new(255, 228, 181),
			Self::Navajowhite => Rgb::new(255, 222, 173),
			Self::Navy => Rgb::new(0, 0, 128),
			Self::Oldlace => Rgb::new(253, 245, 230),
			Self::Olive => Rgb::new(128, 128, 0),
			Self::Olivedrab => Rgb::new(107, 142, 35),
			Self::Orange => Rgb::new(255, 165, 0),
			Self::Orangered => Rgb::new(255, 69, 0),
			Self::Orchid => Rgb::new(218, 112, 214),
			Self::Palegoldenrod => Rgb::new(238, 232, 170),
			Self::Palegreen => Rgb::new(152, 251, 152),
			Self::Paleturquoise => Rgb::new(175, 238, 238),
			Self::Palevioletred => Rgb::new(219, 112, 147),
			Self::Papayawhip => Rgb::new(255, 239, 213),
			Self::Peachpuff => Rgb::new(255, 218, 185),
			Self::Peru => Rgb::new(205, 133, 63),
			Self::Pink => Rgb::new(255, 192, 203),
			Self::Plum => Rgb::new(221, 160, 221),
			Self::Powderblue => Rgb::new(176, 224, 230),
			Self::Purple => Rgb::new(128, 0, 128),
			Self::Red => Rgb::new(255, 0, 0),
			Self::Rosybrown => Rgb::new(188, 143, 143),
			Self::Royalblue => Rgb::new(65, 105, 225),
			Self::Saddlebrown => Rgb::new(139, 69, 19),
			Self::Salmon => Rgb::new(250, 128, 114),
			Self::Sandybrown => Rgb::new(244, 164, 96),
			Self::Seagreen => Rgb::new(46, 139, 87),
			Self::Seashell => Rgb::new(255, 245, 238),
			Self::Sienna => Rgb::new(160, 82, 45),
			Self::Silver => Rgb::new(192, 192, 192),
			Self::Skyblue => Rgb::new(135, 206, 235),
			Self::Slateblue => Rgb::new(106, 90, 205),
			Self::Slategray => Rgb::new(112, 128, 144),
			Self::Slategrey => Rgb::new(112, 128, 144),
			Self::Snow => Rgb::new(255, 250, 250),
			Self::Springgreen => Rgb::new(0, 255, 127),
			Self::Steelblue => Rgb::new(70, 130, 180),
			Self::Tan => Rgb::new(210, 180, 140),
			Self::Teal => Rgb::new(0, 128, 128),
			Self::Thistle => Rgb::new(216, 191, 216),
			Self::Tomato => Rgb::new(255, 99, 71),
			Self::Turquoise => Rgb::new(64, 224, 208),
			Self::Violet => Rgb::new(238, 130, 238),
			Self::Wheat => Rgb::new(245, 222, 179),
			Self::White => Rgb::new(255, 255, 255),
			Self::Whitesmoke => Rgb::new(245, 245, 245),
			Self::Yellow => Rgb::new(255, 255, 0),
			Self::Yellowgreen => Rgb::new(154, 205, 50),
		}
	}
}
