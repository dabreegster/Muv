use std::{io::Read, iter::FusedIterator};

use osmpbf::{DenseTagIter, ElementReader, TagIter};

use crate::Location;

pub struct Reader<R>(R);

impl<R: Read + Send> Reader<R> {
	pub const fn new(r: R) -> Self {
		Self(r)
	}

	pub fn for_each<F: FnMut(Element)>(self, mut f: F) -> Result<(), osmpbf::Error> {
		let r = ElementReader::new(self.0);
		r.for_each(|e| f(e.into()))
	}
}

pub enum Element<'a> {
	Node(Node<'a>),
	Way(Way<'a>),
	Relation(Relation<'a>),
}

impl<'a> From<osmpbf::Element<'a>> for Element<'a> {
	fn from(value: osmpbf::Element<'a>) -> Self {
		match value {
			osmpbf::Element::DenseNode(n) => Self::Node(n.into()),
			osmpbf::Element::Node(n) => Self::Node(n.into()),
			osmpbf::Element::Way(w) => Self::Way(w.into()),
			osmpbf::Element::Relation(r) => Self::Relation(r.into()),
		}
	}
}

#[derive(Debug, Clone)]
pub struct Node<'a> {
	pub id: i64,

	pub lat: i64,
	pub lon: i64,

	pub tags: NodeTags<'a>,
}

impl<'a> Node<'a> {
	#[must_use]
	#[allow(clippy::cast_precision_loss, clippy::as_conversions)]
	pub fn location(&self) -> Location {
		Location {
			lat: 1e-9 * self.lat as f64,
			lon: 1e-9 * self.lon as f64,
		}
	}
}

impl<'a> From<osmpbf::Node<'a>> for Node<'a> {
	fn from(value: osmpbf::Node<'a>) -> Self {
		Self {
			id: value.id(),
			lat: value.nano_lat(),
			lon: value.nano_lon(),
			tags: NodeTags::Node(value.tags()),
		}
	}
}

impl<'a> From<osmpbf::DenseNode<'a>> for Node<'a> {
	fn from(value: osmpbf::DenseNode<'a>) -> Self {
		Self {
			id: value.id,
			lat: value.nano_lat(),
			lon: value.nano_lon(),
			tags: NodeTags::DenseNode(value.tags()),
		}
	}
}

#[derive(Debug, Clone)]
pub enum NodeTags<'a> {
	Node(TagIter<'a>),
	DenseNode(DenseTagIter<'a>),
}

impl<'a> Iterator for NodeTags<'a> {
	type Item = (&'a str, &'a str);

	fn next(&mut self) -> Option<Self::Item> {
		match self {
			Self::Node(n) => n.next(),
			Self::DenseNode(n) => n.next(),
		}
	}

	fn size_hint(&self) -> (usize, Option<usize>) {
		match self {
			Self::Node(n) => n.size_hint(),
			Self::DenseNode(n) => n.size_hint(),
		}
	}
}

impl ExactSizeIterator for NodeTags<'_> {}

impl FusedIterator for NodeTags<'_> {}

#[derive(Debug, Clone)]
pub struct Way<'a> {
	pub id: i64,

	pub tags: TagIter<'a>,
}

impl<'a> From<osmpbf::Way<'a>> for Way<'a> {
	fn from(value: osmpbf::Way<'a>) -> Self {
		Self {
			id: value.id(),
			tags: value.tags(),
		}
	}
}

#[derive(Debug, Clone)]
pub struct Relation<'a> {
	pub id: i64,

	pub tags: TagIter<'a>,
}

impl<'a> From<osmpbf::Relation<'a>> for Relation<'a> {
	fn from(value: osmpbf::Relation<'a>) -> Self {
		Self {
			id: value.id(),
			tags: value.tags(),
		}
	}
}
