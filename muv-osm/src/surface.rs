use crate::{simple_enum, Colour, FromOsm, FromOsmStr, Taglike, ToOsmStr};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Surface {
	pub kind: SurfaceKind,
	pub colour: Option<Colour>,
}

impl<'a, T: Taglike<'a>> FromOsm<'a, T> for Surface {
	fn from_osm(tag: T) -> Option<Self> {
		let kind = tag.value().and_then(SurfaceKind::from_osm_str)?;
		Some(Self {
			kind,
			colour: Colour::parse_tags(tag),
		})
	}
}

simple_enum!(SurfaceKind {
	Paved,
	Asphalt,
	Chipseal,
	Concrete,
	#[osm_str("concrete:lanes")]
	ConcreteLanes,
	#[osm_str("concrete:plates")]
	ConcretePlates,
	PavingStones,
	Sett,
	UnhewnCobblestone,
	Cobblestone,
	Brick,
	Metal,
	Wood,
	SteppingStones,
	Rubber,

	Unpaved,
	Compacted,
	FineGravel,
	Gravel,
	Rock,
	Pebblestone,
	Ground,
	#[osm_str("dirt" | "earth")]
	Dirt,
	Grass,
	GrassPaver,
	MetalGrid,
	Mud,
	Sand,
	Woodchips,
	Snow,
	Ice,
	Salt,

	Clay,
	Tartan,
	ArtificialTurf,
	Acrylic,
	Carpet,
});

impl From<SurfaceKind> for Surface {
	fn from(value: SurfaceKind) -> Self {
		Self {
			kind: value,
			colour: None,
		}
	}
}
