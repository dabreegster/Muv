#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

use crate::{
	get_tag,
	lanes::{travel::AllParser, DoubleTag, Lane, LaneVariant},
	simple_enum,
	tags::Tag,
	units::{Duration, Quantity},
	AccessLevel, Conditional, FromOsm, FromOsmStr, TMode, TModes, Taglike, ToOsmStr,
};

#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct ParkingLane {
	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "TModes::is_empty"))]
	pub access: TModes<Conditional<AccessLevel>>,

	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "TModes::is_empty"))]
	pub restriction: TModes<Conditional<ParkingRestriction>>,

	/// Lane consists of bays, adjacent to the roadway, which could not easily
	/// be turned into a [`TravelLane`](`super::travel::TravelLane`) in the future
	pub bays: Option<bool>,
	pub orientation: Option<ParkingOrientation>,
	pub direction: Option<ParkingDirection>,
	pub markings: Option<bool>,
	pub capacity: Option<Capacity>,
	/// Local parking zones this lane belongs to, like `A`, `II`, or `23`.
	pub zone: Vec<String>,

	#[cfg_attr(feature = "serde", serde(skip_serializing_if = "TModes::is_empty"))]
	pub maxstay: TModes<Conditional<Quantity<Duration>>>,
}

impl TryFrom<LaneVariant> for ParkingLane {
	type Error = ();
	fn try_from(value: LaneVariant) -> Result<Self, Self::Error> {
		match value {
			LaneVariant::Parking(p) => Ok(*p),
			LaneVariant::Travel(_) => Err(()),
		}
	}
}
impl<'a> TryFrom<&'a mut LaneVariant> for &'a mut ParkingLane {
	type Error = ();
	fn try_from(value: &'a mut LaneVariant) -> Result<Self, Self::Error> {
		match value {
			LaneVariant::Parking(p) => Ok(p),
			LaneVariant::Travel(_) => Err(()),
		}
	}
}
impl<'a> TryFrom<&'a LaneVariant> for &'a ParkingLane {
	type Error = ();
	fn try_from(value: &'a LaneVariant) -> Result<Self, Self::Error> {
		match value {
			LaneVariant::Parking(p) => Ok(p),
			LaneVariant::Travel(_) => Err(()),
		}
	}
}

impl From<ParkingLane> for LaneVariant {
	fn from(value: ParkingLane) -> Self {
		Self::Parking(Box::new(value))
	}
}

#[derive(FromOsmStr, Clone, Copy, PartialEq)]
pub(crate) enum ParkingPosition {
	#[osm_str("lane" | "on_street" | "on_lane")]
	Lane,
	StreetSide,
	LayBy,
	OnKerb,
	HalfOnKerb,
	Shoulder,
	Yes,
}

impl ParkingPosition {
	const fn bays(self) -> bool {
		matches!(self, Self::StreetSide | Self::LayBy)
	}

	pub(crate) const fn kerb(self) -> bool {
		matches!(self, Self::OnKerb | Self::HalfOnKerb)
	}
}

simple_enum!(ParkingOrientation
/// The orientation of vehicles parking in relation to the way
///
/// [OSM Wiki](https://wiki.openstreetmap.org/wiki/Street_parking#Parking_orientation)
{
	/// Vehicles parked parallel to the road
	///
	/// ```text
	///  ______   ______   ______
	/// |______| |______| |______|
	/// ```
	Parallel,
	/// Vehicles parked diagonally to the road
	///
	/// ```text
	///    ___ ___ ___ ___
	///   /  //  //  //  /
	///  /  //  //  //  /
	/// /__//__//__//__/
	/// ```
	Diagonal,
	/// Vehicles parked with the front or back pointing straight towards the center of the road
	///
	/// ```text
	///  __  __  __  __
	/// |  ||  ||  ||  |
	/// |  ||  ||  ||  |
	/// |__||__||__||__|
	/// ```
	Perpendicular,
});

simple_enum!(ParkingDirection { BackIn, HeadIn });

simple_enum!(ParkingRestriction {
	NoParking,
	NoStanding,
	NoStopping,
	#[osm_str("loading_only" | "loading" | "loading_zone")]
	OnlyLoading,
	#[osm_str("charging_only")]
	OnlyCharging,
});

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize), serde(untagged))]
pub enum CapacityValue {
	Yes,
	Exact(u32),
}

impl From<bool> for CapacityValue {
	fn from(value: bool) -> Self {
		if value {
			Self::Yes
		} else {
			Self::Exact(0)
		}
	}
}

impl From<CapacityValue> for bool {
	fn from(value: CapacityValue) -> Self {
		match value {
			CapacityValue::Yes => true,
			CapacityValue::Exact(n) => n != 0,
		}
	}
}

impl FromOsmStr for CapacityValue {
	fn from_osm_str(s: &str) -> Option<Self> {
		match s {
			"yes" => Some(Self::Yes),
			"no" => Some(Self::Exact(0)),
			_ => s.parse().ok().map(CapacityValue::Exact),
		}
	}
}

impl ToOsmStr for CapacityValue {
	fn to_osm_str(self) -> Option<std::borrow::Cow<'static, str>> {
		Some(match self {
			Self::Yes => "yes".into(),
			Self::Exact(0) => "no".into(),
			Self::Exact(n) => n.to_string().into(),
		})
	}
}

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Capacity {
	#[cfg_attr(feature = "serde", serde(flatten))]
	pub modes: TModes<CapacityValue>,
	pub women: Option<CapacityValue>,
	pub parent: Option<CapacityValue>,
	pub charging: Option<CapacityValue>,
}

impl Capacity {
	fn parse_osm<'a, T: Taglike<'a>>(tags: T) -> Self {
		let mut modes = TModes::all(CapacityValue::Exact(0));

		for (mode, val) in TMode::iter_tags_with_all(tags)
			.filter_map(|(mode, tag)| Some((mode, CapacityValue::from_osm_str(tag.value()?)?)))
		{
			modes.set(mode, val);
		}

		Self {
			modes,
			women: get_tag!(tags, women).and_then(CapacityValue::from_osm_str),
			parent: get_tag!(tags, parent).and_then(CapacityValue::from_osm_str),
			charging: get_tag!(tags, charging).and_then(CapacityValue::from_osm_str),
		}
	}
}

fn parse_legacy_access(
	tags: &Tag,
) -> (
	Option<Conditional<AccessLevel>>,
	Option<Conditional<ParkingRestriction>>,
) {
	let mut restrictions = false;

	let access = Conditional::parse_with(tags, |s| {
		Some(match s {
			"free" | "ticket" | "disc" => AccessLevel::Yes,
			"residents" | "private" => AccessLevel::Private,
			"customers" => AccessLevel::Customers,
			"disabled" | "no" => AccessLevel::No,
			_ => {
				restrictions = restrictions || ParkingRestriction::from_osm_str(s).is_some();
				return None;
			}
		})
	});

	let restrictions = if restrictions {
		Conditional::from_osm(tags)
	} else {
		None
	};

	(access, restrictions)
}

fn parse_legacy_lane(tags: &Tag, side: &str) -> Option<(Lane, ParkingPosition)> {
	let lane_subtag = get_tag!(tags, lane:)?;
	let lane = DoubleTag::maybe_new(lane_subtag.subtags.get(side), get_tag!(lane_subtag, both:))?;

	let orientation_str = lane.value()?;
	let (orientation, markings) = match orientation_str {
		"yes" => (None, None),
		"marked" => (None, Some(true)),
		_ => {
			// if let Some(restriction) = ParkingRestriction::from_osm_str(orientation_str) {
			// 	(None, Some(restriction), None)
			// } else {
			let _position = lane.get_value(orientation_str);
			let orientation = ParkingOrientation::from_osm_str(orientation_str)?;
			(Some(orientation), None)
			// }
		}
	};

	let position = lane
		.get_value(orientation_str)
		.and_then(ParkingPosition::from_osm_str);

	let mut access = TModes::from([
		(TMode::All, AccessLevel::No.into()),
		(TMode::LandBased, AccessLevel::Yes.into()),
	]);

	let mut restriction = TModes::default();

	let condition = get_tag!(tags, condition:).and_then(|tags| tags.subtags.get(side));
	if let Some(condition) = condition {
		let (condition_access, condition_restriction) = parse_legacy_access(condition);
		if let Some(condition_access) = condition_access {
			access.set(TMode::LandBased, condition_access);
		}
		if let Some(condition_restriction) = condition_restriction {
			restriction.set(TMode::All, condition_restriction);
		}

		for (mode, (condition_access, condition_restriction)) in
			TMode::iter_tags(tags).map(|(mode, tags)| (mode, parse_legacy_access(tags)))
		{
			if let Some(condition_access) = condition_access {
				access.set(mode, condition_access);
			}
			if let Some(condition_restriction) = condition_restriction {
				restriction.set(mode, condition_restriction);
			}
		}
	}

	let variant = ParkingLane {
		access,

		restriction,

		bays: position.map(ParkingPosition::bays),
		orientation,
		direction: None,
		markings,
		capacity: get_tag!(lane, capacity:).map(Capacity::parse_osm),
		zone: get_tag!(lane, residents)
			.and_then(Vec::from_osm_str)
			.unwrap_or_default(),

		maxstay: condition
			.and_then(|condition| get_tag!(condition, maxstay:))
			.and_then(Conditional::from_osm)
			.map(TModes::all)
			.unwrap_or_default(),
	};

	Some((variant.into(), position.unwrap_or(ParkingPosition::Yes)))
}

fn parse_lane(tags: &Tag, side: &str) -> Option<(Lane, ParkingPosition)> {
	let tags = DoubleTag::maybe_new(tags.subtags.get(side), get_tag!(tags, both:))?;
	let position = ParkingPosition::from_osm_str(tags.value()?)?;

	let mut parking = ParkingLane {
		access: TModes::from([
			(TMode::All, AccessLevel::No.into()),
			(TMode::LandBased, AccessLevel::Yes.into()),
		]),

		restriction: TModes::default(),

		bays: Some(position.bays()),
		orientation: get_tag!(tags, orientation).and_then(FromOsmStr::from_osm_str),
		direction: get_tag!(tags, direction).and_then(FromOsmStr::from_osm_str),
		markings: get_tag!(tags, markings).and_then(FromOsmStr::from_osm_str),
		capacity: get_tag!(tags, capacity:).map(Capacity::parse_osm),
		zone: get_tag!(tags, zone)
			.and_then(FromOsmStr::from_osm_str)
			.unwrap_or_default(),

		maxstay: TModes::default(),
	};

	let access = get_tag!(tags, access:);
	let restriction = get_tag!(tags, restriction:);
	let maxstay = get_tag!(tags, maxstay:);

	let mut all = AllParser {
		dirty: false,
		direction: Some,
	};

	all.access(access, tags, false, &mut parking.access);

	all.base(restriction, &mut parking.restriction);
	all.base(maxstay, &mut parking.maxstay);

	Some((Lane::from(parking).add_parse(tags), position))
}

pub(crate) fn parse_parking_lane(tags: &Tag, side: &str) -> Option<(Lane, ParkingPosition)> {
	parse_legacy_lane(tags, side).or_else(|| parse_lane(tags, side))
}

#[cfg(test)]
mod tests {
	use crate::{
		get_tag,
		lanes::parking::{parse_parking_lane, ParkingDirection, ParkingLane, ParkingOrientation},
		new_tag, quantity,
		units::Duration,
		TMode,
	};

	#[test]
	fn legacy_parking() {
		let tags = new_tag! {
			parking:lane:right = "parallel",
			parking:lane:right:parallel = "street_side",
			parking:condition:right:maxstay = "5 minutes"
		};
		let parking_tags = get_tag!(tags, parking:).unwrap();

		assert!(parse_parking_lane(parking_tags, "left").is_none());

		let right_lane = parse_parking_lane(parking_tags, "right").unwrap();
		let right: ParkingLane = right_lane.0.variant.try_into().unwrap();
		assert_eq!(right.orientation, Some(ParkingOrientation::Parallel));
		assert_eq!(right.bays, Some(true));
		assert_eq!(
			right.maxstay.get(TMode::All),
			Some(&quantity!(5 Duration::Minute).into())
		);
	}

	#[test]
	fn parking() {
		let tags = new_tag! {
			parking:left = "lane",
			parking:left:orientation = "parallel",
			parking:left:markings = "yes",
			parking:left:direction = "head_in",
			parking:right = "no"
		};
		let parking_tags = get_tag!(tags, parking:).unwrap();

		let left_lane = parse_parking_lane(parking_tags, "left").unwrap();
		let left: ParkingLane = left_lane.0.variant.try_into().unwrap();
		assert_eq!(left.orientation, Some(ParkingOrientation::Parallel));
		assert_eq!(left.bays, Some(false));
		assert_eq!(left.markings, Some(true));
		assert_eq!(left.direction, Some(ParkingDirection::HeadIn));

		assert!(parse_parking_lane(parking_tags, "right").is_none());
	}

	#[test]
	fn mixed_sides() {
		let tags = new_tag! {
			parking:both = "lane",
			parking:left:orientation = "parallel",
			parking:right:orientation = "diagonal",
		};
		let parking_tags = get_tag!(tags, parking:).unwrap();

		let left_lane = parse_parking_lane(parking_tags, "left").unwrap();
		let left: ParkingLane = left_lane.0.variant.try_into().unwrap();
		assert_eq!(left.orientation, Some(ParkingOrientation::Parallel));

		let right_lane = parse_parking_lane(parking_tags, "right").unwrap();
		let right: ParkingLane = right_lane.0.variant.try_into().unwrap();
		assert_eq!(right.orientation, Some(ParkingOrientation::Diagonal));
	}

	#[test]
	fn only_both() {
		let tags = new_tag! {
			parking:both = "lane"
		};
		let parking_tags = get_tag!(tags, parking:).unwrap();

		assert!(parse_parking_lane(parking_tags, "left").is_some());
		assert!(parse_parking_lane(parking_tags, "right").is_some());
	}

	#[test]
	fn only_both_legacy() {
		let tags = new_tag! {
			parking:lane:both = "parallel"
		};
		let parking_tags = get_tag!(tags, parking:).unwrap();

		assert!(parse_parking_lane(parking_tags, "left").is_some());
		assert!(parse_parking_lane(parking_tags, "right").is_some());
	}
}
