use crate::{
	lanes::{aeroway_lanes, highway::highway_lanes, railway_lanes, waterway_lanes, Lane},
	simple_enum, FromOsmStr, Lifecycle, Tag, ToOsmStr,
};
use std::ops::Not;

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[cfg(feature = "wasm-bindgen")]
use crate::JsTag;
#[cfg(feature = "wasm-bindgen")]
use wasm_bindgen::{prelude::wasm_bindgen, JsValue};

pub(crate) const MAX_DIRECTION_LANES: u8 = 20;
pub(crate) const MAX_LANES: u8 = 120; // Required to be smaller than u8::MAX/2

/// An index that represents the centre or the edge of a lane
///
/// The left edge of the left-most lane is indexed with the number 0.
/// From there on this index is incremented towards the right in half a lane steps.
///
/// ```text
/// 0 1 2 3 4 5 6
/// |   |   |   |
/// |           |
/// |   |   |   |
/// |           |
/// |   |   |   |
/// ```
///
/// Therefore even indices are on the edge between lanes `(i/2)-1` and `i/2`.
/// Odd numbers in contrast are located in the centre of the lane `(i-1)/2`.
pub type LaneIndex = u8;

#[derive(Debug, Clone)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Lanes {
	/// Lifecycle the lanes are in currently
	pub lifecycle: Lifecycle,

	/// Individual lanes the way consists of in left-to-right order
	pub lanes: Vec<Lane>,
	/// Position the OSM centre line is located at
	///
	/// [OSM Wiki](https://wiki.openstreetmap.org/wiki/Proposal:Placement)
	pub centre: LaneIndex,
	/// Physical, often painted, centre line of the way.
	pub centre_line: LaneIndex,

	/// Kerb limiting the roadway on the left side
	pub kerb_left: Option<LaneIndex>,
	/// Kerb limiting the roadway on the right side
	pub kerb_right: Option<LaneIndex>,
}

impl Lanes {
	pub(crate) const fn new_empty(lifecycle: Lifecycle) -> Self {
		Self {
			lifecycle,
			lanes: vec![],
			centre: 0,
			centre_line: 0,
			kerb_left: None,
			kerb_right: None,
		}
	}

	pub(crate) fn new_single(lane: Lane, lifecycle: Lifecycle) -> Self {
		Self {
			lifecycle,
			lanes: vec![lane],
			centre: 1,
			centre_line: 1,
			kerb_left: None,
			kerb_right: None,
		}
	}

	#[inline]
	#[must_use]
	pub(crate) fn count(&self) -> u8 {
		self.lanes.len().try_into().unwrap()
	}

	pub(crate) fn double_len(&self) -> u8 {
		self.count() * 2
	}

	pub(crate) fn add_lane_left(&mut self, lane: Lane) {
		self.lanes.push(lane);
		self.centre += 2;
		self.centre_line += 2;
	}

	pub(crate) fn add_lane_right(&mut self, lane: Lane) {
		self.lanes.push(lane);
	}
}

simple_enum!(Side { Left, Right });

impl Not for Side {
	type Output = Self;
	fn not(self) -> Self::Output {
		match self {
			Self::Left => Self::Right,
			Self::Right => Self::Left,
		}
	}
}

/// Parse tags into [highway](`highway_lanes`), [railway](`railway_lanes`), [waterway](`waterway_lanes`) or [aeroway](`aeroway_lanes`) lanes
#[must_use]
pub fn lanes(tags: &Tag, regions: &[impl AsRef<str>]) -> Option<Lanes> {
	highway_lanes(tags, regions)
		.or_else(|| railway_lanes(tags))
		.or_else(|| waterway_lanes(tags))
		.or_else(|| aeroway_lanes(tags))
}

#[cfg(feature = "wasm-bindgen")]
#[wasm_bindgen(js_name = lanes)]
#[allow(clippy::missing_errors_doc)]
pub fn js_lanes(tags: &JsTag) -> Result<JsValue, serde_wasm_bindgen::Error> {
	let res = lanes(&tags.tag, &["DE"]);
	let ser = serde_wasm_bindgen::Serializer::new().serialize_maps_as_objects(true);
	res.serialize(&ser)
}

#[cfg(test)]
mod tests {
	use crate::{lanes::lanes, new_tag};

	#[test]
	fn direction_lanes_limit() {
		let tags = new_tag! {
			highway = "motorway",
			lanes = "100",
		};

		let lanes = lanes(&tags, &["US"]).unwrap();
		assert_eq!(lanes.lanes.len(), 20);
		assert_eq!(lanes.centre, 20);
		assert_eq!(lanes.centre_line, 20);
	}

	#[test]
	fn global_lanes_limit() {
		let tags = new_tag! {
			highway = "primary",
			sidewalk = "both",
			sidewalk:both:lanes = "30",
			cycleway:both = "lane",
			cycleway:both:lanes = "40",
			busway:both = "lane",
			busway:both:lanes = "50",
			lanes:backward = "60",
			turn:lanes:forward = "||||||||||||||||||||||||||||||||||||||||||||||||||",
		};

		let lanes = lanes(&tags, &["US"]).unwrap();
		assert_eq!(lanes.lanes.len(), 120);
		assert_eq!(lanes.centre, 160);
		assert_eq!(lanes.centre_line, 160);
	}
}
