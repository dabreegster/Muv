use crate::{
	get_tag,
	lanes::{
		highway::{
			distribute_full_width, driving_side, parse_bus_bay_lanes, parse_busway_lanes,
			parse_cycleway_lanes, parse_oneway, parse_shoulder_lanes, parse_sidewalk_lanes,
			BaseLanes, HighwayClassification, OnewayDirection,
		},
		parking::{parse_parking_lane, ParkingPosition},
		Lanes, Side,
	},
	units::{Distance, Quantity},
	FromOsmStr, Lifecycle, Tag, Taglike,
};

/// Parse tags of a highway into lanes
///
/// Regions passed in are used for defaults such as the driving side and speed limits.
/// They are assumed to be passed as uppercase [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2)
/// or [ISO 3166-2](https://en.wikipedia.org/wiki/ISO_3166-2) region codes
/// sorted descending by importance.
///
/// Returns [`None`] in case the tags aren't a highway.
#[must_use]
#[allow(clippy::too_many_lines, clippy::cognitive_complexity)]
pub fn highway_lanes(tags: &Tag, regions: &[impl AsRef<str>]) -> Option<Lanes> {
	let driving_side = get_tag!(tags, driving_side)
		.and_then(Side::from_osm_str)
		.or_else(|| regions.iter().find_map(|s| driving_side(s.as_ref())))
		.unwrap_or(Side::Right);

	let (lifecycle, class, _) = Lifecycle::from_osm(tags, "highway")?;

	let platform = class == Some(HighwayClassification::Platform)
		|| get_tag!(tags, public_transport) == Some("platform");

	let (oneway, oneway_vehicle) = parse_oneway(
		class,
		tags,
		get_tag!(tags, oneway:),
		if matches!(
			class,
			Some(
				HighwayClassification::Cycleway
					| HighwayClassification::Footway
					| HighwayClassification::Pedestrian
					| HighwayClassification::Steps
					| HighwayClassification::Path
			)
		) {
			OnewayDirection::BothWays
		} else {
			OnewayDirection::Both
		},
	);

	let mut lanes = Lanes::new_empty(lifecycle);

	let (mut has_sidewalk, mut sidewalk_left, mut sidewalk_right) = (class
		!= Some(HighwayClassification::Footway))
	.then(|| {
		Some(parse_sidewalk_lanes(
			get_tag!(tags, sidewalk:)?,
			regions,
			driving_side,
			oneway_vehicle,
		))
	})
	.flatten()
	.unwrap_or_default();
	has_sidewalk = has_sidewalk || sidewalk_left.is_some() || sidewalk_right.is_some();

	let (mut cycleway_left, mut cycleway_right) = if class == Some(HighwayClassification::Cycleway)
	{
		None
	} else {
		get_tag!(tags, cycleway:).map(|cycleway_tags| {
			parse_cycleway_lanes(
				cycleway_tags,
				get_tag!(tags, bicycle:lanes:),
				regions,
				driving_side,
				oneway_vehicle,
			)
		})
	}
	.unwrap_or_default();

	if let (Some(sidewalk), Some(cycleway)) = (sidewalk_left.as_ref(), cycleway_left.as_ref()) {
		if cycleway.segregated.is_some() {
			sidewalk_left = None;
		} else if sidewalk.segregated.is_some() {
			cycleway_left = None;
		}
	}
	if let (Some(sidewalk), Some(cycleway)) = (sidewalk_right.as_ref(), cycleway_right.as_ref()) {
		if cycleway.segregated.is_some() {
			sidewalk_right = None;
		} else if sidewalk.segregated.is_some() {
			cycleway_right = None;
		}
	}

	let segregated = if sidewalk_left.is_none() && sidewalk_right.is_none() {
		get_tag!(tags, segregated).and_then(bool::from_osm_str)
	} else {
		None
	};

	let (mut parking_left, mut parking_right) = get_tag!(tags, parking:)
		.map(|tags| {
			(
				parse_parking_lane(tags, "left"),
				parse_parking_lane(tags, "right"),
			)
		})
		.unwrap_or_default();
	let (bus_bay_left, bus_bay_right) = get_tag!(tags, bus_bay:)
		.map(|tags| parse_bus_bay_lanes(tags, regions, driving_side, oneway_vehicle))
		.unwrap_or_default();
	let (shoulder_left, shoulder_right) = get_tag!(tags, shoulder:)
		.map(|tags| parse_shoulder_lanes(tags, regions, driving_side, oneway_vehicle))
		.unwrap_or_default();

	let (mut busway_left, mut busway_right) = get_tag!(tags, busway:)
		.map(|tags| parse_busway_lanes(tags, regions, driving_side, oneway_vehicle))
		.unwrap_or_default();
	let (mut busway_forward, mut busway_backward) = match driving_side {
		Side::Left => (busway_left.is_some(), busway_right.is_some()),
		Side::Right => (busway_right.is_some(), busway_left.is_some()),
	};

	if busway_forward || busway_backward {
		let bus_lanes = get_tag!(tags, bus:lanes:);
		let psv_lanes = get_tag!(tags, psv:lanes:);

		if bus_lanes.and_then(Taglike::value).is_some()
			|| psv_lanes.and_then(Taglike::value).is_some()
		{
			busway_left = None;
			busway_right = None;
		} else if busway_forward
			&& (bus_lanes.and_then(|tags| get_tag!(tags, forward)).is_some()
				|| psv_lanes.and_then(|tags| get_tag!(tags, forward)).is_some())
		{
			match driving_side {
				Side::Left => busway_left = None,
				Side::Right => busway_right = None,
			}
			busway_forward = false;
		} else if busway_backward
			&& (bus_lanes
				.and_then(|tags| get_tag!(tags, backward))
				.is_some() || psv_lanes
				.and_then(|tags| get_tag!(tags, backward))
				.is_some())
		{
			match driving_side {
				Side::Left => busway_right = None,
				Side::Right => busway_left = None,
			}
			busway_backward = false;
		}
	}

	if let Some(left) = &sidewalk_left {
		left.add(&mut lanes, Side::Left);
	}

	if let Some(left) = &cycleway_left {
		if left.kerb {
			left.add(&mut lanes, Side::Left);
			cycleway_left = None;
		}
	}

	let mut kerb_left = lanes.double_len();
	if parking_left.as_ref().is_some_and(|left| left.1.kerb()) {
		if let Some((left, position)) = parking_left.take() {
			lanes.add_lane_left(left);
			kerb_left += 2 - u8::from(position == ParkingPosition::HalfOnKerb);
		}
	}

	let carriageway_start = lanes.lanes.len();

	if let Some(left) = parking_left {
		lanes.add_lane_left(left.0);
	}
	if let Some(left) = bus_bay_left {
		left.add(&mut lanes, Side::Left);
	}
	if let Some(left) = shoulder_left {
		left.add(&mut lanes, Side::Left);
	}
	if let Some(left) = &cycleway_left {
		left.add(&mut lanes, Side::Left);
	}
	if let Some(left) = busway_left {
		left.add(&mut lanes, Side::Left);
	}

	let base_lanes = BaseLanes {
		regions,
		driving_side,

		tags,
		lanes: &mut lanes.lanes,
		is_sidepath: false,
		return_center: true,
		segregated,

		class,
		oneway: &oneway,
		oneway_vehicle,
		platform,
		has_sidewalk,
		busway_forward: busway_forward.into(),
		busway_backward: busway_backward.into(),
	};
	let (centre_line, centre) = base_lanes.add();
	lanes.centre += centre;
	lanes.centre_line += centre_line;

	if let Some(right) = busway_right {
		right.add(&mut lanes, Side::Right);
	}
	if let Some(right) = &cycleway_right {
		if !right.kerb {
			right.add(&mut lanes, Side::Right);
			cycleway_right = None;
		}
	}
	if let Some(right) = shoulder_right {
		right.add(&mut lanes, Side::Right);
	}
	if let Some(right) = bus_bay_right {
		right.add(&mut lanes, Side::Right);
	}
	if parking_right.as_ref().is_some_and(|right| !right.1.kerb()) {
		if let Some(right) = parking_right.take() {
			lanes.add_lane_right(right.0);
		}
	}

	let mut kerb_right = lanes.double_len();
	if let Some(carriageway_width) = carriageway_width(tags) {
		distribute_full_width(&mut lanes.lanes[carriageway_start..], carriageway_width);
	}

	if let Some((right, position)) = parking_right {
		kerb_right += u8::from(position == ParkingPosition::HalfOnKerb);
		lanes.add_lane_right(right);
	}
	if let Some(right) = &cycleway_right {
		right.add(&mut lanes, Side::Right);
	}
	if let Some(right) = &sidewalk_right {
		right.add(&mut lanes, Side::Right);
	}

	lanes.kerb_left = (kerb_left != 0).then_some(kerb_left);
	lanes.kerb_right = (kerb_right != lanes.double_len()).then_some(kerb_right);

	Some(lanes)
}

fn carriageway_width(tags: &Tag) -> Option<Quantity<Distance>> {
	if let Some(width) = get_tag!(tags, width:) {
		if let Some(carriageway_width) =
			get_tag!(tags, carriageway).and_then(Quantity::from_osm_str)
		{
			return Some(carriageway_width);
		} else if let Some(width) = width.value().and_then(Quantity::from_osm_str) {
			return Some(width);
		}
	}

	get_tag!(tags, est_width).and_then(Quantity::from_osm_str)
}

#[cfg(test)]
mod tests {
	use crate::{
		lanes::{assert_access, lanes, travel::TravelLane},
		new_tag, quantity,
		units::Distance,
		AccessLevel, TMode,
	};

	#[cfg(feature = "lanes-default-speeds")]
	use crate::lanes::highway::speed_default::{default, motorway};

	#[test]
	fn even_lane_count() {
		let tags = new_tag! {
			highway = "primary",
			lanes = "4",
		};

		let lanes = lanes(&tags, &["DE"]).unwrap();
		assert_eq!(lanes.lanes.len(), 4);
	}

	#[test]
	fn odd_lane_count() {
		let tags = new_tag! {
			highway = "primary",
			lanes = "5",
			lanes:backward = "2",
		};

		let lanes = lanes(&tags, &["DE"]).unwrap();
		assert_eq!(lanes.lanes.len(), 5);

		let travel_lane: &TravelLane = (&lanes.lanes[2].variant).try_into().unwrap();
		assert_eq!(
			travel_lane.forward.access.get(TMode::LandBased),
			Some(&AccessLevel::Yes.into())
		);
		assert_eq!(
			travel_lane.backward.access.get(TMode::LandBased),
			Some(&AccessLevel::No.into())
		);
	}

	#[cfg(feature = "lanes-default-speeds")]
	#[test]
	fn maxspeed_defaults() {
		assert_eq!(default("IT", "motorway"), motorway("IT"));
		assert!(default("FR", "urban").is_some());
		assert!(default("GB", "nsl_restricted").is_some());
		assert!(default("GB", "nsl_single").is_some());
	}

	#[test]
	fn segregated() {
		let tags = new_tag! {
			highway = "cycleway",
			oneway = "yes",
			foot = "permissive",
			segregated = "yes",
		};

		let lanes = lanes(&tags, &["DE"]).unwrap();
		assert_eq!(lanes.lanes.len(), 2);

		assert_access(
			&lanes.lanes[0],
			TMode::Bicycle,
			AccessLevel::Designated,
			AccessLevel::No,
		);
		assert_access(
			&lanes.lanes[0],
			TMode::Foot,
			AccessLevel::No,
			AccessLevel::No,
		);

		assert_access(
			&lanes.lanes[1],
			TMode::Bicycle,
			AccessLevel::No,
			AccessLevel::No,
		);
		assert_access(
			&lanes.lanes[1],
			TMode::Foot,
			AccessLevel::Permissive,
			AccessLevel::Permissive,
		);
	}

	#[test]
	fn segregated_no_foot() {
		let tags = new_tag! {
			highway = "cycleway",
			oneway = "yes",
			segregated = "yes",
		};

		let lanes = lanes(&tags, &["GB"]).unwrap();
		assert_eq!(lanes.lanes.len(), 2);

		assert_access(
			&lanes.lanes[0],
			TMode::Bicycle,
			AccessLevel::No,
			AccessLevel::No,
		);
		assert_access(
			&lanes.lanes[0],
			TMode::Foot,
			AccessLevel::Yes,
			AccessLevel::Yes,
		);

		assert_access(
			&lanes.lanes[1],
			TMode::Bicycle,
			AccessLevel::Designated,
			AccessLevel::No,
		);
		assert_access(
			&lanes.lanes[1],
			TMode::Foot,
			AccessLevel::No,
			AccessLevel::No,
		);
	}

	#[test]
	fn segregated_sidewalk() {
		let tags = new_tag! {
			highway = "cycleway",
			oneway = "yes",
			segregated = "yes",
			sidewalk = "left",
		};

		let lanes = lanes(&tags, &["DE"]).unwrap();
		assert_eq!(lanes.lanes.len(), 2);
		assert_eq!(lanes.kerb_left, Some(2));
		assert_eq!(lanes.kerb_right, None);
	}

	#[test]
	fn unsegregated_sidewalk() {
		let tags = new_tag! {
			highway = "residential",
			sidewalk = "right",
			sidewalk:right:surface = "paving_stones",
			sidewalk:right:bicycle = "yes",
			sidewalk:right:segregated = "no",
			cycleway:right = "track",
			cycleway:right:surface = "asphalt",
		};

		let lanes = lanes(&tags, &["NL"]).unwrap();
		assert_eq!(lanes.lanes.len(), 3);
	}

	#[test]
	fn unsegregated_cycleway() {
		let tags = new_tag! {
			highway = "residential",
			cycleway:left = "track",
			cycleway:left:surface = "asphalt",
			cycleway:left:foot = "designated",
			cycleway:left:segregated = "no",
			sidewalk = "left",
		};

		let lanes = lanes(&tags, &["BE"]).unwrap();
		assert_eq!(lanes.lanes.len(), 3);
	}

	#[test]
	fn kerb() {
		let tags = new_tag! {
			highway = "residential",
			lanes = "2",
			sidewalk = "both",
			parking:left = "half_on_kerb",
		};

		let lanes = lanes(&tags, &["ES"]).unwrap();

		assert_eq!(lanes.lanes.len(), 5);
		assert_eq!(lanes.centre, 6);
		assert_eq!(lanes.kerb_left, Some(3));
		assert_eq!(lanes.kerb_right, Some(8));
	}

	#[test]
	fn no_kerb() {
		let tags = new_tag! {highway = "trunk"};

		let lanes = lanes(&tags, &["ES"]).unwrap();

		assert_eq!(lanes.lanes.len(), 2);
		assert_eq!(lanes.centre, 2);
		assert_eq!(lanes.kerb_left, None);
		assert_eq!(lanes.kerb_right, None);
	}

	#[test]
	fn lane_widths() {
		let tags = new_tag! {
			highway = "primary",
			width = "8",
			vehicle:lanes:forward = "yes|no",
			bicycle:lanes:forward = "yes|designated",
			parking:right = "lane",
			parking:right:width = "3",
		};

		let lanes = lanes(&tags, &["DE"]).unwrap();

		assert_eq!(lanes.lanes.len(), 4);

		assert_eq!(lanes.lanes[0].width, Some(quantity!(2 Distance::Metre)));
		assert_eq!(lanes.lanes[1].width, Some(quantity!(2 Distance::Metre)));
		assert_eq!(lanes.lanes[2].width, Some(quantity!(1 Distance::Metre)));
		assert_eq!(lanes.lanes[3].width, Some(quantity!(3 Distance::Metre)));
	}

	#[test]
	fn cycleway_with_cycleway() {
		let tags = new_tag! {
			highway = "cycleway",
			oneway = "yes",
			cycleway = "lane",
		};

		let lanes = lanes(&tags, &["JP"]).unwrap();
		assert_eq!(lanes.lanes.len(), 1);
	}

	#[test]
	fn dont_skip_oneway_sidewalk() {
		let tags = new_tag! {
			highway = "residential",
			sidewalk = "left",
			sidewalk:left:bicycle = "yes",
			sidewalk:left:oneway = "-1",
		};

		let lanes = lanes(&tags, &["JP"]).unwrap();
		assert_eq!(lanes.lanes.len(), 3);
	}

	#[test]
	fn busway_lanes_count() {
		let tags = new_tag! {
			highway = "primary",
			busway:left = "lane",
			lanes = "4",
		};

		let lanes = lanes(&tags, &["FI"]).unwrap();
		assert_eq!(lanes.lanes.len(), 4);
	}

	#[test]
	fn bus_lanes_ignore_busway() {
		let tags = new_tag! {
			highway = "secondary",
			busway:left = "lane",
			bus:lanes:backward = "yes|designated"
		};

		let lanes = lanes(&tags, &["MX"]).unwrap();
		assert_eq!(lanes.lanes.len(), 3);

		assert_access(
			&lanes.lanes[0],
			TMode::Bus,
			AccessLevel::No,
			AccessLevel::Designated,
		);
	}

	#[test]
	fn bus_lanes_count() {
		let tags = new_tag! {
			highway = "residential",
			lanes = "4",
			lanes:bus:forward = "1",
		};

		let lanes = lanes(&tags, &["NL"]).unwrap();
		assert_eq!(lanes.lanes.len(), 4);

		assert_access(
			&lanes.lanes[3],
			TMode::MotorVehicle,
			AccessLevel::No,
			AccessLevel::No,
		);
		assert_access(
			&lanes.lanes[3],
			TMode::Bus,
			AccessLevel::Designated,
			AccessLevel::No,
		);
	}

	#[test]
	fn busway_opposite_oneway() {
		let tags = new_tag! {
			highway = "residential",
			oneway = "yes",
			oneway:bus = "no",
			lanes = "2",
			lanes:forward = "1",
			lanes:backward = "1",
		};

		let lanes = lanes(&tags, &["BE"]).unwrap();
		assert_eq!(lanes.lanes.len(), 2);

		assert_access(
			&lanes.lanes[0],
			TMode::Bus,
			AccessLevel::No,
			AccessLevel::Yes,
		);
		assert_access(
			&lanes.lanes[0],
			TMode::Motorcar,
			AccessLevel::No,
			AccessLevel::No,
		);

		assert_access(
			&lanes.lanes[1],
			TMode::Bus,
			AccessLevel::Yes,
			AccessLevel::No,
		);
		assert_access(
			&lanes.lanes[1],
			TMode::Motorcar,
			AccessLevel::Yes,
			AccessLevel::No,
		);
	}
}
