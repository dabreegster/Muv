use crate::{
	get_tag,
	lanes::{
		highway::{forward_on, parse_oneway, BaseLanes, HighwayClassification, OnewayDirection},
		travel::{TravelLane, TravelLaneDirection},
		DoubleTag, Lane, Lanes, Side,
	},
	units::Quantity,
	AccessLevel, FromOsmStr, TModes, Tag, Taglike,
};

pub(crate) struct SideLane<'a, R: AsRef<str>> {
	highway_class: HighwayClassification,
	parent_oneway: Option<OnewayDirection>,
	oneway: bool,
	regions: &'a [R],
	driving_side: Side,
	tags: DoubleTag<'a>,
	contraflow: bool,
	pub(crate) segregated: Option<bool>,
	pub(crate) kerb: bool,
}

impl<'a, R: AsRef<str>> SideLane<'a, R> {
	pub(crate) fn add(&self, lanes: &mut Lanes, side: Side) {
		if side == Side::Right {
			add_buffer(lanes, self.tags);
		}

		let oneway_default = if self.oneway {
			let mut oneway = match self.parent_oneway {
				Some(oneway @ (OnewayDirection::Forward | OnewayDirection::Backward)) => oneway,
				_ => {
					if self.driving_side == side {
						OnewayDirection::Forward
					} else {
						OnewayDirection::Backward
					}
				}
			};
			if self.contraflow {
				oneway = oneway.flip();
			}
			oneway
		} else {
			OnewayDirection::BothWays
		};

		let (oneway, oneway_vehicle) = parse_oneway(
			Some(self.highway_class),
			self.tags,
			get_tag!(self.tags, oneway:),
			oneway_default,
		);

		let base_lanes = BaseLanes {
			regions: self.regions,
			driving_side: self.driving_side,

			tags: self.tags,
			lanes: &mut lanes.lanes,
			is_sidepath: true,
			return_center: false,
			segregated: self.segregated,

			class: Some(self.highway_class),
			oneway: &oneway,
			oneway_vehicle,
			platform: false,
			has_sidewalk: false,
			busway_forward: 0,
			busway_backward: 0,
		};
		let (count, _) = base_lanes.add();

		if side == Side::Left {
			lanes.centre += count * 2;
			lanes.centre_line += count * 2;
			add_buffer(lanes, self.tags);
		}
	}
}

#[inline]
fn add_buffer(lanes: &mut Lanes, tags: DoubleTag) {
	let Some(buffer) = get_tag!(tags, buffer).and_then(Quantity::from_osm_str) else {
		return;
	};

	let mut lane: Lane = TravelLane::both(TravelLaneDirection::with_access(TModes::all(
		AccessLevel::No.into(),
	)))
	.into();
	lane.width = Some(buffer);
	lanes.add_lane_left(lane);
}

type SideLaneExists = Option<(bool, bool)>;

#[inline]
#[allow(clippy::too_many_arguments)]
fn parse_side_lanes<'a, R: AsRef<str>>(
	tags: &'a Tag,
	mode_lanes_tags: Option<&'a Tag>,
	highway_class: HighwayClassification,
	parent_oneway: Option<OnewayDirection>,
	oneway: bool,
	regions: &'a [R],
	driving_side: Side,
	(mut left, mut right): (SideLaneExists, SideLaneExists),
	mut allowed_side: impl FnMut(&str) -> SideLaneExists,
) -> (Option<SideLane<'a, R>>, Option<SideLane<'a, R>>) {
	let both_tags = get_tag!(tags, both:);
	let left_tags = get_tag!(tags, left:).or(both_tags);
	let right_tags = get_tag!(tags, right:).or(both_tags);

	if let Some(both) = both_tags.and_then(Taglike::value).map(&mut allowed_side) {
		left = both;
		right = both;
	}
	left = left_tags
		.and_then(Taglike::value)
		.map_or(left, &mut allowed_side);
	right = right_tags
		.and_then(Taglike::value)
		.map_or(right, &mut allowed_side);

	if let Some(lanes) = get_tag!(tags, lanes:) {
		if lanes.value().is_some() {
			left = None;
			right = None;
		} else {
			if get_tag!(lanes, forward).is_some() {
				match driving_side {
					Side::Left => left = None,
					Side::Right => right = None,
				}
			}
			if get_tag!(lanes, backward).is_some() {
				match driving_side {
					Side::Left => right = None,
					Side::Right => left = None,
				}
			}
		}
	}

	if let Some(lanes) = mode_lanes_tags {
		if lanes.value().is_some() {
			left = None;
			right = None;
		} else {
			if get_tag!(lanes, forward).is_some() {
				match driving_side {
					Side::Left => left = None,
					Side::Right => right = None,
				}
			}
			if get_tag!(lanes, backward).is_some() {
				match driving_side {
					Side::Left => right = None,
					Side::Right => left = None,
				}
			}
		}
	}

	let both_tags = both_tags.unwrap_or(tags);

	(
		left.map(|(contraflow, kerb)| {
			let tags = DoubleTag::new_reverse(left_tags, both_tags);
			SideLane {
				highway_class,
				parent_oneway,
				oneway,
				regions,
				driving_side,
				tags,
				contraflow,
				segregated: get_tag!(tags, segregated).and_then(bool::from_osm_str),
				kerb,
			}
		}),
		right.map(|(contraflow, kerb)| {
			let tags = DoubleTag::new_reverse(right_tags, both_tags);
			SideLane {
				highway_class,
				parent_oneway,
				oneway,
				regions,
				driving_side,
				tags,
				contraflow,
				segregated: get_tag!(tags, segregated).and_then(bool::from_osm_str),
				kerb,
			}
		}),
	)
}

pub(crate) fn parse_sidewalk_lanes<'a, R: AsRef<str>>(
	tags: &'a Tag,
	regions: &'a [R],
	driving_side: Side,
	parent_oneway: Option<OnewayDirection>,
) -> (bool, Option<SideLane<'a, R>>, Option<SideLane<'a, R>>) {
	let mut separate = false;
	let (left, right) = parse_side_lanes(
		tags,
		None,
		HighwayClassification::Footway,
		parent_oneway,
		false,
		regions,
		driving_side,
		match tags.value() {
			Some("yes" | "both") => (Some((false, true)), Some((false, true))),
			Some("left") => (Some((false, true)), None),
			Some("right") => (None, Some((false, true))),
			Some("separate") => {
				separate = true;
				(None, None)
			}
			_ => (None, None),
		},
		|s| match s {
			"yes" => Some((false, true)),
			"separate" => {
				separate = true;
				None
			}
			_ => None,
		},
	);
	(separate, left, right)
}

fn position(
	parent_oneway: Option<OnewayDirection>,
	driving_side: Side,
	kerb: bool,
) -> (SideLaneExists, SideLaneExists) {
	let left = parent_oneway.map_or(true, |oneway| {
		oneway.in_direction_ever(driving_side == Side::Left)
	});
	let right = parent_oneway.map_or(true, |oneway| {
		oneway.in_direction_ever(driving_side == Side::Right)
	});

	(
		left.then_some((false, kerb)),
		right.then_some((false, kerb)),
	)
}

fn position_opposite(
	parent_oneway: Option<OnewayDirection>,
	driving_side: Side,
	kerb: bool,
) -> (SideLaneExists, SideLaneExists) {
	if forward_on(parent_oneway, driving_side, Side::Left) {
		(None, Some((true, kerb)))
	} else {
		(Some((true, kerb)), None)
	}
}

pub(crate) fn parse_cycleway_lanes<'a, R: AsRef<str>>(
	tags: &'a Tag,
	bicycle_lanes_tags: Option<&'a Tag>,
	regions: &'a [R],
	driving_side: Side,
	parent_oneway: Option<OnewayDirection>,
) -> (Option<SideLane<'a, R>>, Option<SideLane<'a, R>>) {
	parse_side_lanes(
		tags,
		bicycle_lanes_tags,
		HighwayClassification::Cycleway,
		parent_oneway,
		true,
		regions,
		driving_side,
		match tags.value() {
			// TODO: Add to busway instead of creating new lane
			Some("lane" | "share_busway") => position(parent_oneway, driving_side, false),
			Some("track") => position(parent_oneway, driving_side, true),
			Some("opposite_lane" | "opposite_share_busway") => {
				position_opposite(parent_oneway, driving_side, false)
			}
			Some("opposite_track") => position_opposite(parent_oneway, driving_side, true),
			_ => (None, None),
		},
		|s| {
			Some(match s {
				"lane" => (false, false),
				"track" => (false, true),
				"opposite_lane" => (true, false),
				"opposite_track" => (true, true),
				_ => return None,
			})
		},
	)
}

pub(crate) fn parse_busway_lanes<'a, R: AsRef<str>>(
	tags: &'a Tag,
	regions: &'a [R],
	driving_side: Side,
	parent_oneway: Option<OnewayDirection>,
) -> (Option<SideLane<'a, R>>, Option<SideLane<'a, R>>) {
	parse_side_lanes(
		tags,
		None,
		HighwayClassification::Busway,
		parent_oneway,
		true,
		regions,
		driving_side,
		match tags.value() {
			Some("lane") => position(parent_oneway, driving_side, false),
			Some("opposite_lane") => position_opposite(parent_oneway, driving_side, false),
			_ => (None, None),
		},
		|s| (s == "lane").then_some((false, false)),
	)
}

pub(crate) fn parse_bus_bay_lanes<'a, R: AsRef<str>>(
	tags: &'a Tag,
	regions: &'a [R],
	driving_side: Side,
	parent_oneway: Option<OnewayDirection>,
) -> (Option<SideLane<'a, R>>, Option<SideLane<'a, R>>) {
	parse_side_lanes(
		tags,
		None,
		HighwayClassification::Busway,
		parent_oneway,
		true,
		regions,
		driving_side,
		match tags.value() {
			Some("yes" | "both") => (Some((false, false)), Some((false, false))),
			Some("left") => (Some((false, false)), None),
			Some("right") => (None, Some((false, false))),
			_ => (None, None),
		},
		|s| matches!(s, "yes" | "lane").then_some((false, false)),
	)
}

pub(crate) fn parse_shoulder_lanes<'a, R: AsRef<str>>(
	tags: &'a Tag,
	regions: &'a [R],
	driving_side: Side,
	parent_oneway: Option<OnewayDirection>,
) -> (Option<SideLane<'a, R>>, Option<SideLane<'a, R>>) {
	parse_side_lanes(
		tags,
		None,
		HighwayClassification::Shoulder,
		parent_oneway,
		false,
		regions,
		driving_side,
		match tags.value() {
			Some("yes" | "both") => (Some((false, false)), Some((false, false))),
			Some("left") => (Some((false, false)), None),
			Some("right") => (None, Some((false, false))),
			_ => (None, None),
		},
		|s| (s == "yes").then_some((false, false)),
	)
}

#[cfg(test)]
mod tests {
	use crate::{
		lanes::{
			assert_access,
			highway::{
				driving_side, highway_lanes, parse_bus_bay_lanes, parse_busway_lanes,
				parse_cycleway_lanes, parse_sidewalk_lanes, OnewayDirection,
			},
			Lane, Lanes, Side,
		},
		new_tag, quantity,
		units::Distance,
		AccessLevel, Lifecycle, SurfaceKind, TMode, Tag, Taglike,
	};

	fn test_side_lane(
		tags: &Tag,
		region: &str,
		kind: &str,
		parent_oneway: Option<OnewayDirection>,
		left_len: usize,
		right_len: usize,
	) -> Vec<Lane> {
		let tags = tags.get(kind).unwrap();
		let driving_side = driving_side(region).unwrap();

		let regions = &[region];
		let (left, right) = match kind {
			"sidewalk" => {
				let (_, left, right) =
					parse_sidewalk_lanes(tags, regions, driving_side, parent_oneway);
				(left, right)
			}
			"cycleway" => parse_cycleway_lanes(tags, None, regions, driving_side, parent_oneway),
			"busway" => parse_busway_lanes(tags, regions, driving_side, parent_oneway),
			"bus_bay" => parse_bus_bay_lanes(tags, regions, driving_side, parent_oneway),
			_ => panic!("unknown side lane kind: {kind}"),
		};

		let mut lanes = Lanes::new_empty(Lifecycle::Normal);
		if let Some(left) = left {
			left.add(&mut lanes, Side::Left);
		}
		assert_eq!(lanes.lanes.len(), left_len);

		if let Some(right) = right {
			right.add(&mut lanes, Side::Right);
		}
		assert_eq!(lanes.lanes.len(), left_len + right_len);

		lanes.lanes
	}

	#[test]
	fn sidewalks() {
		let tags = new_tag! {
			sidewalk:both:surface = "paving_stones",
			sidewalk:left = "seperate",
			sidewalk:right = "yes"
		};
		let lanes = test_side_lane(&tags, "DE", "sidewalk", Some(OnewayDirection::Both), 0, 1);

		assert_eq!(lanes[0].surface, Some(SurfaceKind::PavingStones.into()));
		assert_access(
			&lanes[0],
			TMode::Foot,
			AccessLevel::Designated,
			AccessLevel::Designated,
		);
	}

	#[test]
	fn bus_bay() {
		let tags = new_tag! {
			bus_bay = "left",
			bus_bay:right = "lane",
		};
		let lanes = test_side_lane(&tags, "IT", "bus_bay", None, 1, 1);

		assert_access(
			&lanes[0],
			TMode::Bus,
			AccessLevel::No,
			AccessLevel::Designated,
		);
		assert_access(&lanes[0], TMode::Motorcar, AccessLevel::No, AccessLevel::No);

		assert_access(
			&lanes[1],
			TMode::Bus,
			AccessLevel::Designated,
			AccessLevel::No,
		);
		assert_access(&lanes[1], TMode::Motorcar, AccessLevel::No, AccessLevel::No);
	}

	#[test]
	fn cycleway_lane() {
		let tags = new_tag! {
			cycleway = "lane",
		};
		let lanes = test_side_lane(&tags, "IN", "cycleway", None, 1, 1);

		assert_access(
			&lanes[0],
			TMode::Bicycle,
			AccessLevel::Designated,
			AccessLevel::No,
		);
		assert_access(
			&lanes[1],
			TMode::Bicycle,
			AccessLevel::No,
			AccessLevel::Designated,
		);
	}

	#[test]
	fn cycleway_lane_on_oneway() {
		let tags = new_tag! {
			cycleway = "lane",
		};
		let lanes = test_side_lane(
			&tags,
			"CH",
			"cycleway",
			Some(OnewayDirection::Backward),
			1,
			0,
		);

		assert_access(
			&lanes[0],
			TMode::Bicycle,
			AccessLevel::No,
			AccessLevel::Designated,
		);
	}

	#[test]
	fn cycleway_opposite() {
		let tags = new_tag! {
			cycleway = "opposite_track",
		};
		let lanes = test_side_lane(
			&tags,
			"TR",
			"cycleway",
			Some(OnewayDirection::Forward),
			1,
			0,
		);

		assert_access(
			&lanes[0],
			TMode::Bicycle,
			AccessLevel::No,
			AccessLevel::Designated,
		);
	}

	#[test]
	fn cycleway_side_opposite() {
		let tags = new_tag! {
			cycleway:left = "opposite_lane",
			cycleway:right = "track"
		};
		let lanes = test_side_lane(
			&tags,
			"GB",
			"cycleway",
			Some(OnewayDirection::BothWays),
			1,
			1,
		);

		assert_access(
			&lanes[0],
			TMode::Bicycle,
			AccessLevel::No,
			AccessLevel::Designated,
		);

		assert_access(
			&lanes[1],
			TMode::Bicycle,
			AccessLevel::No,
			AccessLevel::Designated,
		);
	}

	#[test]
	fn cycleway_side_on_oneway() {
		let tags = new_tag! {
			cycleway:left = "lane",
			cycleway:right = "opposite_track"
		};
		let lanes = test_side_lane(
			&tags,
			"SI",
			"cycleway",
			Some(OnewayDirection::Forward),
			1,
			1,
		);

		assert_access(
			&lanes[0],
			TMode::Bicycle,
			AccessLevel::Designated,
			AccessLevel::No,
		);

		assert_access(
			&lanes[1],
			TMode::Bicycle,
			AccessLevel::No,
			AccessLevel::Designated,
		);
	}

	#[ignore]
	#[test]
	fn cycleway_on_busway() {
		todo!();
	}

	#[test]
	fn no_duplicate_cycleway_lanes() {
		let tags = new_tag! {
			highway = "secondary",
			lanes:backward = "2",
			cycleway:left = "lane",
			cycleway:right = "lane",
			cycleway:lanes:forward = "lane|no",
			vehicle:lanes:forward = "no|yes",
			bicycle:lanes:forward = "designated|yes",
		};

		let lanes = highway_lanes(&tags, &["US"]).unwrap();
		assert_eq!(lanes.lanes.len(), 3 + 2);
		assert_eq!(lanes.centre, 6);
		assert_eq!(lanes.centre_line, 6);
	}

	#[test]
	fn no_duplicate_bicycle_lanes() {
		let tags = new_tag! {
			highway = "secondary",
			cycleway:left = "lane",
			cycleway:right = "lane",
			bicycle:lanes:forward = "yes|designated|yes",
		};

		let lanes = highway_lanes(&tags, &["DK"]).unwrap();
		assert_eq!(lanes.lanes.len(), 2 + 3);
		assert_eq!(lanes.centre, 4);
		assert_eq!(lanes.centre_line, 4);
	}

	#[test]
	fn no_duplicate_cycleway_lanes_on_oneway() {
		let tags = new_tag! {
			highway = "secondary",
			oneway = "yes",
			cycleway:right = "lane",
			cycleway:lanes = "no|lane|no",
			vehicle:lanes = "yes|no|yes",
			bicycle:lanes = "no|designated|yes",
		};

		let lanes = highway_lanes(&tags, &["US"]).unwrap();
		assert_eq!(lanes.lanes.len(), 3);
		assert_eq!(lanes.centre, 3);
		assert_eq!(lanes.centre_line, 3);
	}

	#[test]
	fn cycleway_buffer() {
		let tags = new_tag! {
			cycleway:left = "lane",
			cycleway:left:buffer = "0.2",
			cycleway:right = "track",
			cycleway:right:buffer = "0.75"
		};
		let lanes = test_side_lane(&tags, "GB", "cycleway", Some(OnewayDirection::Both), 2, 2);

		assert_eq!(lanes[0].width, None);
		assert_eq!(lanes[1].width, Some(quantity!(0.2 Distance::Metre)));

		assert_eq!(lanes[2].width, Some(quantity!(0.75 Distance::Metre)));
		assert_eq!(lanes[3].width, None);
	}

	#[test]
	fn cycleway_segregated() {
		let tags = new_tag! {
			cycleway:left = "opposite_track",
			cycleway:left:oneway = "-1",
			cycleway:left:foot = "designated",
			cycleway:left:bicycle = "designated",
			cycleway:left:segregated = "yes",
		};
		let lanes = test_side_lane(&tags, "DE", "cycleway", None, 2, 0);

		assert_access(&lanes[0], TMode::Bicycle, AccessLevel::No, AccessLevel::No);
		assert_access(
			&lanes[0],
			TMode::Foot,
			AccessLevel::Designated,
			AccessLevel::Designated,
		);

		assert_access(
			&lanes[1],
			TMode::Bicycle,
			AccessLevel::No,
			AccessLevel::Designated,
		);
		assert_access(&lanes[1], TMode::Foot, AccessLevel::No, AccessLevel::No);
	}
}
