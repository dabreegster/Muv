use std::num::NonZeroU8;

use crate::FromOsmStr;

pub(crate) enum Placement {
	Transition,
	MiddleOf(NonZeroU8),
	RightOf(NonZeroU8),
	LeftOf(NonZeroU8),
}

impl FromOsmStr for Placement {
	fn from_osm_str(s: &str) -> Option<Self> {
		if s == "transition" {
			return Some(Self::Transition);
		}

		let (relative, lane_str) = s.split_once(':')?;
		let lane = lane_str.parse().ok()?;

		let variant = match relative {
			"middle_of" => Self::MiddleOf,
			"right_of" => Self::RightOf,
			"left_of" => Self::LeftOf,
			_ => return None,
		};
		Some(variant(lane))
	}
}

impl Placement {
	pub(crate) const fn to_center_index(&self) -> Option<u8> {
		// placements lane indecies start at 1
		Some(match self {
			Self::Transition => return None,
			Self::MiddleOf(lane) => lane.get() * 2 - 1,
			Self::RightOf(lane) => lane.get() * 2,
			Self::LeftOf(lane) => lane.get() * 2 - 2,
		})
	}
}
