#[allow(clippy::module_inception)]
mod highway;
pub use highway::*;

mod side_lanes;
pub(crate) use side_lanes::*;

mod placement;
pub(crate) use placement::*;

mod oneway;
pub(crate) use oneway::*;

mod destination;
pub use destination::*;

mod utils;
pub(crate) use utils::*;

mod access;

mod class;
pub(crate) use class::*;

mod base;
pub(crate) use base::*;

mod direction;
pub(crate) use direction::*;

use super::Side;

pub(crate) mod traffic_signs;

mod driving_side;
/// [Driving side](https://en.wikipedia.org/wiki/Left-_and_right-hand_traffic) of a region
///
/// Region is assumed to be passed as an uppercase
/// [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) region code.
#[must_use]
pub fn driving_side(region: &str) -> Option<Side> {
	driving_side::driving_side(region)
}

#[cfg(feature = "lanes-default-speeds")]
pub(crate) mod speed_default;
