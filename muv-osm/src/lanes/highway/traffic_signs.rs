use crate::{lanes::travel::Overtaking, Conditional, TMode, TModes};

pub(crate) fn overtaking(s: &str) -> Option<TModes<Conditional<Overtaking>>> {
	s.split(';').find_map(|s| match s {
		"DE:277.1" => Some(TModes::from([(TMode::All, Overtaking::No.into())])),
		_ => None,
	})
}
