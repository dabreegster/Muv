use std::{cmp::Ordering, iter::FusedIterator};

pub(crate) struct RepeatN<T> {
	pub(crate) item: Option<T>,
	pub(crate) count: usize,
}

impl<T: Clone> Iterator for RepeatN<T> {
	type Item = T;

	fn next(&mut self) -> Option<Self::Item> {
		match self.count.cmp(&1) {
			Ordering::Greater => {
				self.count -= 1;
				self.item.clone()
			}
			Ordering::Equal => self.item.take(),
			Ordering::Less => None,
		}
	}

	fn size_hint(&self) -> (usize, Option<usize>) {
		(self.count, Some(self.count))
	}

	fn last(self) -> Option<Self::Item> {
		self.item
	}

	fn count(self) -> usize {
		self.count
	}
}

impl<T: Clone> ExactSizeIterator for RepeatN<T> {}

impl<T: Clone> FusedIterator for RepeatN<T> {}
