use crate::{
	get_tag, lanes::highway::access, AccessLevel, Conditional, FromOsmStr, TModes, Taglike,
};

#[cfg(feature = "lanes-default-speeds")]
use crate::lanes::highway::speed_default::{self, DefaultSpeed};

#[derive(Debug, Clone, Copy, PartialEq)]
pub(crate) enum HighwayClassification {
	Motorway(bool),
	Trunk(bool),
	Primary(bool),
	Secondary(bool),
	Tertiary(bool),

	Unclassified,
	Residential,

	LivingStreet,
	Service,
	Pedestrian,
	Track,
	BusGuideway,
	Escape,
	Raceway,
	Road,
	Busway,

	Footway,
	Bridleway,
	Steps,
	Corridor,
	Path,
	ViaFerrata,
	Cycleway,
	Platform,

	Shoulder,
}

impl FromOsmStr for HighwayClassification {
	fn from_osm_str(s: &str) -> Option<Self> {
		Some(match s {
			"motorway" => Self::Motorway(false),
			"motorway_link" => Self::Motorway(true),

			"trunk" => Self::Trunk(false),
			"trunk_link" => Self::Trunk(true),

			"primary" => Self::Primary(false),
			"primary_link" => Self::Primary(true),

			"secondary" => Self::Secondary(false),
			"secondary_link" => Self::Secondary(true),

			"tertiary" => Self::Tertiary(false),
			"tertiary_link" => Self::Tertiary(true),

			"unclassified" => Self::Unclassified,
			"residential" => Self::Residential,

			"living_street" => Self::LivingStreet,
			"service" => Self::Service,
			"pedestrian" => Self::Pedestrian,
			"track" => Self::Track,
			"bus_guideway" => Self::BusGuideway,
			"escape" => Self::Escape,
			"raceway" => Self::Raceway,
			"road" => Self::Road,
			"busway" => Self::Busway,

			"footway" => Self::Footway,
			"bridleway" => Self::Bridleway,
			"steps" => Self::Steps,
			"corridor" => Self::Corridor,
			"path" => Self::Path,
			"via_ferrata" => Self::ViaFerrata,
			"cycleway" => Self::Cycleway,
			"platform" => Self::Platform,

			_ => return None,
		})
	}
}

impl HighwayClassification {
	pub(crate) fn default_access<'a, T: Taglike<'a>>(
		self,
		regions: &[impl AsRef<str>],
		tags: T,
		motorroad: bool,
	) -> TModes<Conditional<AccessLevel>> {
		if motorroad {
			return access::motorway();
		}

		let bicycle_road = get_tag!(tags, bicycle_road) == Some("yes");
		if bicycle_road || get_tag!(tags, cyclestreet) == Some("yes") {
			return access::cyclestreet(regions, bicycle_road);
		}

		match self {
			Self::Motorway(_) => access::motorway(),
			Self::Trunk(_)
			| Self::Primary(_)
			| Self::Secondary(_)
			| Self::Tertiary(_)
			| Self::Unclassified
			| Self::Residential
			| Self::LivingStreet
			| Self::Service
			| Self::Road
			| Self::Track => access::highway(),
			Self::Path => access::path(),
			Self::Bridleway => access::bridleway(),
			Self::Cycleway => access::cycleway(),
			Self::Pedestrian
			| Self::Footway
			| Self::Corridor
			| Self::ViaFerrata
			| Self::Platform => access::footway(),
			Self::Steps => access::steps(tags),
			Self::Busway => access::busway(),
			Self::BusGuideway => access::bus_guideway(),

			Self::Shoulder => access::shoulder(),
			Self::Escape => access::escape(),
			Self::Raceway => access::raceway(),
		}
	}

	#[cfg(feature = "lanes-default-speeds")]
	pub(crate) fn default_speed(
		self,
		regions: &[impl AsRef<str>],
	) -> Option<(DefaultSpeed, DefaultSpeed)> {
		match self {
			Self::Pedestrian => regions
				.iter()
				.find_map(|r| speed_default::pedestrian(r.as_ref())),
			Self::LivingStreet => regions
				.iter()
				.find_map(|r| speed_default::living_street(r.as_ref())),
			Self::Service => regions
				.iter()
				.find_map(|r| speed_default::service(r.as_ref())),
			Self::Motorway(_) => regions
				.iter()
				.find_map(|r| speed_default::motorway(r.as_ref())),
			_ => None,
		}
	}
}
