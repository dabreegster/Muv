use std::iter::Map;

use crate::{Tag, Taglike};

#[derive(Clone, Copy)]
pub(crate) struct DoubleTag<'a> {
	a: &'a Tag<'a>,
	b: Option<&'a Tag<'a>>,
}

impl<'a> DoubleTag<'a> {
	pub(crate) const fn new_reverse(a: Option<&'a Tag<'a>>, b: &'a Tag<'a>) -> Self {
		if let Some(a) = a {
			Self { a, b: Some(b) }
		} else {
			Self { a: b, b: None }
		}
	}

	pub(crate) const fn maybe_new(a: Option<&'a Tag<'a>>, b: Option<&'a Tag<'a>>) -> Option<Self> {
		if let Some(a) = a {
			Some(Self { a, b })
		} else if let Some(b) = b {
			Some(Self { a: b, b: None })
		} else {
			None
		}
	}
}

impl<'a> Taglike<'a> for DoubleTag<'a> {
	type Iter =
		Map<<&'a Tag<'a> as Taglike<'a>>::Iter, fn((&'a str, &'a Tag)) -> (&'a str, DoubleTag<'a>)>;

	fn value(self) -> Option<&'a str> {
		self.a.value().or_else(|| self.b?.value())
	}

	fn get(self, key: &str) -> Option<Self> {
		if let Some(a) = self.a.get(key) {
			Some(Self {
				a,
				b: self.b.and_then(|b| b.get(key)),
			})
		} else {
			Some(DoubleTag {
				a: self.b?.get(key)?,
				b: None,
			})
		}
	}

	fn iter_subtags(self) -> Self::Iter {
		// TODO
		self.a
			.iter_subtags()
			.map(|(k, v)| (k, Self { a: v, b: None }))
	}
}
