use std::mem::swap;

use crate::{
	get_tag,
	lanes::{
		parking::ParkingLane,
		travel::{TravelLane, TravelLaneDirection},
	},
	simple_enum,
	units::{Distance, Frequency, Quantity, Voltage},
	AccessLevel, FromOsm, FromOsmStr, Surface, TModes, Taglike, ToOsmStr,
};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(
	feature = "serde",
	derive(Serialize, Deserialize),
	serde(rename_all = "lowercase")
)]
pub enum LaneVariant {
	Travel(Box<TravelLane>),
	Parking(Box<ParkingLane>),
}

#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Lane {
	#[cfg_attr(feature = "serde", serde(flatten))]
	pub variant: LaneVariant,

	/// Sidepath in this instance refers to any lane that is not part of the core lanes referred to by
	/// the `highway`, `railway`, etc. tag. This includes sidewalks (`sidewalk=*`) and cycleways (`cycleway=*`),
	/// but also sidewalks (`footway=sidewalk`) and sidepaths (`is_sidepath=yes`) mapped separately
	/// from the main way.
	pub is_sidepath: bool,

	/// Width of the lane given explicitly or derived from the full width of the way
	pub width: Option<Quantity<Distance>>,
	/// Surface of the lane if known
	pub surface: Option<Surface>,
	/// Smoothness of the lane if known
	pub smoothness: Option<Smoothness>,

	pub electrifications: Vec<Electrification>,
	pub track_widths: Vec<Quantity<Distance>>,
}

impl<T: Into<LaneVariant>> From<T> for Lane {
	fn from(value: T) -> Self {
		Self {
			variant: value.into(),
			is_sidepath: false,
			width: None,
			surface: None,
			smoothness: None,
			electrifications: Vec::default(),
			track_widths: Vec::default(),
		}
	}
}

impl Lane {
	pub(crate) fn add_parse<'a, T: Taglike<'a>>(mut self, tags: T) -> Self {
		self.width = get_tag!(tags, width)
			.and_then(Quantity::from_osm_str)
			.or(self.width);
		self.surface = get_tag!(tags, surface:)
			.and_then(Surface::from_osm)
			.or(self.surface);
		self.smoothness = get_tag!(tags, smoothness)
			.and_then(Smoothness::from_osm_str)
			.or(self.smoothness);
		self
	}
}

impl Lane {
	#[inline]
	pub(crate) fn travel_wrap(lane: TravelLaneDirection) -> Self {
		TravelLane {
			forward: lane,
			backward: TravelLaneDirection::default_empty(),
		}
		.into()
	}

	#[inline]
	pub(crate) fn unwrap_travel(&mut self) -> &mut TravelLaneDirection {
		match &mut self.variant {
			LaneVariant::Travel(t) => &mut t.forward,
			LaneVariant::Parking(_) => unreachable!(),
		}
	}

	#[inline]
	pub(crate) fn travel_wrap_build(&mut self, forward: bool, backward: bool) {
		let lane = match &mut self.variant {
			LaneVariant::Travel(t) => t,
			LaneVariant::Parking(_) => unreachable!(),
		};
		match (forward, backward) {
			(true, true) => lane.backward = lane.forward.clone(),
			(true, false) => {
				lane.backward.access = TModes::all(AccessLevel::No.into());
			}
			(false, true) => {
				swap(&mut lane.forward, &mut lane.backward);
				lane.forward.access = TModes::all(AccessLevel::No.into());
			}
			(false, false) => {
				lane.backward.access = TModes::all(AccessLevel::No.into());
				lane.forward = lane.backward.clone();
			}
		};
	}
}

simple_enum!(Smoothness
/// Physical usability of a way for vehicles due to surface regularity and flatness
///
/// [OSM Wiki](https://wiki.openstreetmap.org/wiki/Key:smoothness) / [Gallery](https://wiki.openstreetmap.org/wiki/Key:smoothness/Gallery)
{
	/// As-new [asphalt](`crate::SurfaceKind::Asphalt`) or [concrete](`crate::SurfaceKind::Concrete`) surface usable by roller blades and skateboards
	Excellent,
	/// A surface showing narrow cracks and shallow dents in which rainwater may collect usable by racing bikes
	Good,
	/// A surface showing signs of maintainance such as patches of repaired pavement and wider cracks usable by city bikes and sport cars
	Intermediate,
	/// Heavily damages paved surface with many potholes usable by normal cars
	Bad,
	/// Unpaved surface with potholes and ruts passable by cars high ground clearance
	VeryBad,
	/// Unpaved track with ruts and rocks passable by heavy-duty off road vehicles
	Horrible,
	/// Tracks with deep ruts and other obstacles passable by tractors and mountain bikes
	VeryHorrible,
	/// Not passable by vehicles
	Impassable,
});

simple_enum!(ElectrificationSystem
/// System used to electrify a certain road or railway
///
/// [OSM Wiki](https://wiki.openstreetmap.org/wiki/Key:electrified)
{
	#[osm_str("yes")]
	Unknown,
	/// An overhead wire (or rail, particularly in tunnels) above the roadway or rail tracks
	///
	/// [OSM Wiki](https://wiki.openstreetmap.org/wiki/Tag:electrified%3Dcontact_line) / [Wikipedia](https://en.wikipedia.org/wiki/Overhead_line)
	#[osm_str("contact_line")]
	OverheadLine,
	/// An additional rail providing power running next to or between the rails
	///
	/// [OSM Wiki](https://wiki.openstreetmap.org/wiki/Tag:electrified%3Drail) / [Wikipedia](https://en.wikipedia.org/wiki/Third_rail)
	#[osm_str("rail")]
	ThirdRail,
	/// Two additional rails providing power running next to or between the rails
	///
	/// [OSM Wiki](https://wiki.openstreetmap.org/wiki/Tag:electrified%3D4th_rail) / [Wikipedia](https://en.wikipedia.org/wiki/Railway_electrification#Fourth_rail)
	#[osm_str("4th_rail")]
	FourthRail,
	/// A line providing power, only powered on as vehicles pass over them, running in the middle of the road
	///
	/// [Wikipedia](https://en.wikipedia.org/wiki/Ground-level_power_supply)
	#[osm_str("ground-level_power_supply")]
	GroundLevelPowerSupply,
	/// Power provided through induction, currently used by maglevs
	Inductive,
});

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Electrification {
	pub system: ElectrificationSystem,
	pub voltage: Option<Quantity<Voltage>>,
	pub frequency: Option<Quantity<Frequency>>,
}

impl<'a, T: Taglike<'a>> FromOsm<'a, T> for Vec<Electrification> {
	fn from_osm(tag: T) -> Option<Self> {
		// TODO: Support multiple electrification systems
		let system = ElectrificationSystem::from_osm_str(get_tag!(tag, electrified)?)?;
		Some(vec![Electrification {
			system,
			voltage: get_tag!(tag, voltage).and_then(Quantity::from_osm_str),
			frequency: get_tag!(tag, frequency).and_then(Quantity::from_osm_str),
		}])
	}
}

#[cfg(test)]
use crate::{Conditional, TMode};

#[cfg(test)]
pub fn assert_access(
	lane: &Lane,
	mode: TMode,
	forward: impl Into<Conditional<AccessLevel>>,
	backward: impl Into<Conditional<AccessLevel>>,
) {
	let travel: &TravelLane = (&lane.variant).try_into().unwrap();
	assert_eq!(
		travel.forward.access.get(mode),
		Some(&forward.into()),
		"forward {mode:?}"
	);
	assert_eq!(
		travel.backward.access.get(mode),
		Some(&backward.into()),
		"backward {mode:?}"
	);
}
