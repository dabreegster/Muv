use std::fmt::{self, Display, Formatter};

use crate::{get_tag, simple_enum, FromOsm, FromOsmStr, Taglike, ToOsmStr};

#[derive(Debug, Default)]
pub struct ProtectionSystem {
	pub cbtc: Option<Field<Cbtc>>,
	pub etcs: Option<Field<Etcs>>,

	pub atacs: Option<bool>,
	pub atb: Option<bool>,
	pub atb_eg: Option<bool>,
	pub atb_ng: Option<bool>,
	pub caws: Option<bool>,
	pub ctcs: Option<Field<Ctcs>>,
	pub evm: Option<bool>,
	pub gnt: Option<bool>,
	pub jkv: Option<bool>,
	pub kvb: Option<bool>,
	pub ls: Option<bool>,
	pub lzb: Option<bool>,
	pub pzb: Option<bool>,
	pub scmt: Option<bool>,
	pub shp: Option<bool>,
	pub ssc: Option<bool>,
	pub tvm: Option<Tvm>,
	pub zbs: Option<bool>,
	pub zsi127: Option<bool>,

	pub zub122: Option<bool>,
	pub zub123: Option<bool>,
	pub zub222c: Option<bool>,
	pub zub262: Option<bool>,
}

impl<'a, T: Taglike<'a>> FromOsm<'a, T> for ProtectionSystem {
	fn from_osm(tag: T) -> Option<Self> {
		let mut res = Self {
			cbtc: get_tag!(tag, cbtc).and_then(FromOsmStr::from_osm_str),
			etcs: get_tag!(tag, etcs).and_then(FromOsmStr::from_osm_str),

			atacs: get_tag!(tag, atacs).and_then(FromOsmStr::from_osm_str),
			atb: get_tag!(tag, atb).and_then(FromOsmStr::from_osm_str),
			atb_eg: tag.get_value("atb-eg").and_then(FromOsmStr::from_osm_str),
			atb_ng: tag.get_value("atb-ng").and_then(FromOsmStr::from_osm_str),
			caws: get_tag!(tag, caws).and_then(FromOsmStr::from_osm_str),
			ctcs: get_tag!(tag, ctcs).and_then(FromOsmStr::from_osm_str),
			evm: get_tag!(tag, evm).and_then(FromOsmStr::from_osm_str),
			gnt: None,
			jkv: get_tag!(tag, jkv).and_then(FromOsmStr::from_osm_str),
			kvb: get_tag!(tag, kvb).and_then(FromOsmStr::from_osm_str),
			ls: get_tag!(tag, ls).and_then(FromOsmStr::from_osm_str),
			lzb: get_tag!(tag, lzb).and_then(FromOsmStr::from_osm_str),
			pzb: get_tag!(tag, pzb).and_then(FromOsmStr::from_osm_str),
			scmt: get_tag!(tag, scmt).and_then(FromOsmStr::from_osm_str),
			shp: get_tag!(tag, shp).and_then(FromOsmStr::from_osm_str),
			ssc: get_tag!(tag, ssc).and_then(FromOsmStr::from_osm_str),
			tvm: get_tag!(tag, tvm).and_then(FromOsmStr::from_osm_str),
			zbs: get_tag!(tag, zbs).and_then(FromOsmStr::from_osm_str),
			zsi127: get_tag!(tag, zsi127).and_then(FromOsmStr::from_osm_str),

			zub122: None,
			zub123: get_tag!(tag, zub123).and_then(FromOsmStr::from_osm_str),
			zub222c: get_tag!(tag, zub222c).and_then(FromOsmStr::from_osm_str),
			zub262: None,
		};

		if res.atb.is_none() {
			if res.atb_eg == Some(true) || res.atb_ng == Some(true) {
				res.atb = Some(true);
			}
			if res.atb_eg == Some(false) && res.atb_ng == Some(false) {
				res.atb = Some(false);
			}
		}

		match get_tag!(tag, gnt) {
			Some("no") => res.gnt = Some(false),
			Some("yes") => res.gnt = Some(true),
			Some("zub122") => {
				res.gnt = Some(true);
				res.zub122 = Some(true);
				res.zub262 = Some(false);
			}
			Some("zub262") => {
				res.gnt = Some(true);
				res.zub122 = Some(false);
				res.zub262 = Some(true);
			}
			_ => {}
		}

		Some(res)
	}
}

#[derive(Debug)]
pub enum Field<T> {
	No,
	Yes,
	Value(T),
}

impl<T: FromOsmStr> FromOsmStr for Field<T> {
	fn from_osm_str(s: &str) -> Option<Self> {
		match s {
			"yes" => Some(Self::Yes),
			"no" => Some(Self::No),
			_ => T::from_osm_str(s).map(Self::Value),
		}
	}
}

simple_enum!(Cbtc { Uto, Sto, Dto });

simple_enum!(Etcs {
	#[osm_str("1")]
	One,
	#[osm_str("2")]
	Two,
	#[osm_str("3")]
	Three,
});

impl Display for Etcs {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		f.write_str(match self {
			Self::One => "ETCS 1",
			Self::Two => "ETCS 2",
			Self::Three => "ETCS 3",
		})
	}
}

simple_enum!(Ctcs {
	#[osm_str("0")]
	Zero,
	#[osm_str("1")]
	One,
	#[osm_str("2")]
	Two,
	#[osm_str("3D")]
	ThreeD,
	#[osm_str("3")]
	Three,
	#[osm_str("4")]
	Four,
});

impl Display for Ctcs {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		f.write_str(match self {
			Self::Zero => "CTCS 0",
			Self::One => "CTCS 1",
			Self::Two => "CTCS 2",
			Self::Three => "CTCS 3",
			Self::ThreeD => "CTCS 3D",
			Self::Four => "CTCS 4",
		})
	}
}

simple_enum!(Tvm {
	#[osm_str("300")]
	ThreeHundred,
	#[osm_str("430")]
	FourHundredAndThirty,
});

impl Display for Tvm {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		f.write_str(match self {
			Self::ThreeHundred => "TVM-300",
			Self::FourHundredAndThirty => "TVM-430",
		})
	}
}
