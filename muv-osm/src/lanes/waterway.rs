use crate::{
	lanes::{
		travel::{TravelLane, TravelLaneDirection},
		Lane, Lanes,
	},
	AccessLevel, Conditional, FromOsm, FromOsmStr, Lifecycle, TMode, TModes, Tag,
};

#[derive(Clone, FromOsmStr)]
enum WaterwayClassification {
	River,
}

/// Parse tags of a waterway into lanes
///
/// Returns [`None`] in case the tags aren't a waterway
#[must_use]
pub fn waterway_lanes(tags: &Tag) -> Option<Lanes> {
	let (lifecycle, _, _) = Lifecycle::from_osm::<WaterwayClassification>(tags, "waterway")?;

	let mut access = TModes::from([]);
	for (mode, val) in
		TMode::iter_tags(tags).filter_map(|(mode, tag)| Some((mode, Conditional::from_osm(tag)?)))
	{
		access.set(mode, val);
	}
	if access.is_empty() {
		return None;
	}
	access.set(TMode::All, AccessLevel::No.into());

	let travel_lane = TravelLaneDirection::with_access(access);

	let mut lane: Lane = TravelLane::both(travel_lane).into();
	lane = lane.add_parse(tags);

	Some(Lanes::new_single(lane, lifecycle))
}
