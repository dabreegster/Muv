#![deny(clippy::as_underscore, clippy::as_conversions)]
#![warn(
	clippy::pedantic,
	clippy::nursery,
	clippy::panic,
	clippy::print_stdout,
	clippy::print_stderr
)]
#![allow(
	clippy::bool_assert_comparison,
	clippy::similar_names,
	clippy::module_name_repetitions,
	clippy::option_if_let_else,
	clippy::redundant_pub_crate,
	clippy::struct_excessive_bools
)]
#![cfg_attr(test, allow(clippy::panic, clippy::missing_panics_doc))]

pub use muv_osm_derive::{FromOsmStr, ToOsmStr};

#[warn(missing_docs)]
mod access;
pub use access::*;

mod tmode;
pub use tmode::*;

#[warn(missing_docs)]
mod hierarchy;
pub use hierarchy::*;

mod colour;
pub use colour::*;

#[cfg(feature = "conditional")]
pub mod conditional;
#[cfg(feature = "conditional")]
#[doc(inline)]
pub use conditional::Conditional;

mod location;
pub use location::*;

mod parse;
pub use parse::*;

#[cfg(feature = "lanes")]
mod bitflags;
#[cfg(feature = "lanes")]
#[allow(unused_imports)]
pub use bitflags::*;

mod surface;
pub use surface::*;

mod tags;
pub use tags::*;

mod lifecycle;
pub use lifecycle::*;

pub mod units;

mod vehicle;
pub use vehicle::*;

mod name;
pub use name::*;

#[cfg(feature = "reader")]
mod reader;
#[cfg(feature = "reader")]
pub use reader::*;

#[cfg(feature = "conditional")]
mod restriction;
#[cfg(feature = "conditional")]
pub use restriction::*;

#[cfg(feature = "addr")]
pub mod addr;

#[cfg(feature = "lanes")]
pub mod lanes;

// TODO
// #[cfg(feature = "tree")]
// pub mod tree;
