macro_rules! simple_bitflags {
    ($name:ident $(($($derive:ident),+))? : $base:ident $(#[$($attr:tt)*])* { $( $(#[$($flagattr:tt)*])* $flag:ident($osmname:ident) = $flagval:expr,)+ }) => {
        $(#[$($attr)*])*
        #[derive(Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, $($($derive),+)?)]
        pub struct $name(
            /// The internal bits of the bitflag
            pub $base
        );

        impl $name {
            $(
                $(#[$($flagattr)*])*
                #[allow(non_upper_case_globals)]
                pub const $flag: Self = Self($flagval);
            )+
        }

        impl $name {
            #[inline]
            #[must_use]
            pub const fn contains(self, other: Self) -> bool {
                self.0 & other.0 == other.0
            }

            #[inline]
            #[must_use]
            pub const fn any(self, other: Self) -> bool {
                self.0 & other.0 != 0
            }

            #[must_use]
            pub fn from_osm_str_single(s: &str) -> Option<Self> {
                Some(match s {
                    $(stringify!($osmname) => Self::$flag,)+
                    _ => return None
                })
            }
        }

        impl $crate::FromOsmStr for $name {
            fn from_osm_str(s: &str) -> Option<Self> {
                s.split(';')
                    .filter_map(Self::from_osm_str_single)
                    .reduce(std::ops::BitOr::bitor)
            }
        }

        impl $crate::ToOsmStr for $name {
            fn to_osm_str(self) -> Option<std::borrow::Cow<'static, str>> {
                [$((Self::$flag, stringify!($osmname))),+]
                    .into_iter()
                    .filter(|(flag, _)| self.contains(*flag))
                    .map(|(_, s)| std::borrow::Cow::Borrowed(s))
                    .reduce(|mut b, s| {
                        b.to_mut().push(';');
                        b.to_mut().push_str(&s);
                        b
                    })
                    .or(Some("".into()))
            }
        }

        impl std::fmt::Debug for $name {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                write!(f, "{}(", stringify!($name))?;
                let mut content = [$((Self::$flag, stringify!($flag))),+].into_iter().filter(|(flag, _)| self.contains(*flag));
                if let Some((_, name)) = content.next() {
                    write!(f, "{name}")?;
                    for (_, name) in content {
                        write!(f, " | {name}")?;
                    }
                }
                write!(f, ")")
            }
        }

        impl std::ops::BitOr for $name {
            type Output = Self;
            #[must_use]
            fn bitor(self, rhs: Self) -> Self::Output {
                Self(self.0 | rhs.0)
            }
        }

        impl std::ops::BitOrAssign for $name {
            fn bitor_assign(&mut self, rhs: Self) {
                self.0 |= rhs.0;
            }
        }

        impl std::ops::BitAnd for $name {
            type Output = Self;
            #[must_use]
            fn bitand(self, rhs: Self) -> Self::Output {
                Self(self.0 & rhs.0)
            }
        }

        #[cfg(feature = "serde")]
        impl serde::Serialize for $name {
            fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
            where
                S: serde::Serializer,
            {
                if serializer.is_human_readable() {
                    use serde::ser::SerializeSeq;
                    let mut seq = serializer.serialize_seq(None)?;
                    for (flag, name) in [$((Self::$flag, stringify!($flag))),+] {
                        if self.contains(flag) {
                            seq.serialize_element(name)?;
                        }
                    }
                    seq.end()
                } else {
                    self.0.serialize(serializer)
                }
            }
        }

        #[cfg(feature = "serde")]
        impl<'de> serde::Deserialize<'de> for $name {
            fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
            where
                D: serde::Deserializer<'de>,
            {
                use serde::de::{Error, SeqAccess, Visitor};
                use std::fmt;

                struct BitflagsVisitor;

                impl<'de> Visitor<'de> for BitflagsVisitor {
                    type Value = $name;

                    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                        formatter.write_str("a sequence of strings")
                    }

                    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E> where E: Error {
                        match v {
                            $(stringify!($flag) => Ok($name::$flag),)+
                            _ => Err(Error::custom("unknown bitflag field")),
                        }
                    }

                    fn visit_seq<A>(self, mut seq: A) -> Result<$name, A::Error> where A: SeqAccess<'de> {
                        let mut res = $name(0);
                        while let Some(value) = seq.next_element()? {
                            res |= value;
                        }
                        Ok(res)
                    }
                }

                if deserializer.is_human_readable() {
                    deserializer.deserialize_seq(BitflagsVisitor)
                } else {
                    Ok(Self($base::deserialize(deserializer)?))
                }
            }
        }
    };
}
pub(crate) use simple_bitflags;
