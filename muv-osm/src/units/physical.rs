use crate::{
	n, simple_enum,
	units::{Number, PartialUnit, Quantity, Unit},
	FromOsmStr, ToOsmStr,
};
use std::{
	fmt::{self, Display, Formatter},
	time,
};

#[cfg(feature = "serde")]
use serde::{Deserialize, Deserializer, Serialize, Serializer};

macro_rules! dec_unit {
    ($name:ident $dunit:ident => $dfirstmatch:tt $(| $dmatch:tt)*, $($unit:ident $(($mult:expr))? => $firstmatch:tt $(| $match:tt)*,)*) => {
        simple_enum!($name(Default) {
            #[default]
            #[osm_str($dfirstmatch $(| $dmatch)*)]
            $dunit,
            $(
                #[osm_str($firstmatch $(| $match)*)]
                $unit
            ),*
        });

        impl Display for $name {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                f.write_str(match self {
                    Self::$dunit => $dfirstmatch,
                    $(Self::$unit => $firstmatch,)*
                })
            }
        }

        dec_unit! { $name $dunit, $($unit $(($mult))?,)* }
    };
    ($name:ident $dunit:ident, $($unit:ident ($mult:expr),)*) => {
        impl $name {
            fn to_base(self) -> Number {
                #[allow(clippy::unreadable_literal)]
                Number::new(match self {
                    Self::$dunit => 1.0,
                    $(Self::$unit => $mult),*
                })
            }
        }

        impl PartialUnit for $name {
            fn partial_default() -> Option<Self> {
                Some(Self::default())
            }

            fn partial_convert(&self, to: &Self, value: Number) -> Option<Number> {
                Some(self.convert(to, value))
            }
        }

        impl Unit for $name {
            fn convert(&self, to: &Self, value: Number) -> Number {
                value * self.to_base() / to.to_base()
            }
        }
    };
    ($name:ident $dunit:ident, $($unit:ident $(($mult:expr))?,)*) => {
        impl $name {
            fn to_base(self) -> Option<Number> {
                #[allow(clippy::unreadable_literal)]
                Some(Number::new(match self {
                    Self::$dunit => 1.0,
                    $($(Self::$unit => $mult,)?)*
                    _ => return None,
                }))
            }
        }

        impl PartialUnit for $name {
            fn partial_default() -> Option<Self> {
                Some(Self::default())
            }

            fn partial_convert(&self, to: &Self, value: Number) -> Option<Number> {
                Some(value * self.to_base()? / to.to_base()?)
            }
        }
    };
}

dec_unit! {
	Distance
	Metre => "m" | "metre" | "metres" | "meter" | "meters",
	Kilometre(1000.0) => "km" | "kilometre" | "kilometres" | "kilometer" | "kilometers",
	Millimetre(0.001) => "mm" | "millimetre" | "millimetres" | "millimeter" | "millimeters",
	Mile(1609.344) => "mi" | "mile" | "miles",
	NauticalMile(1852.0) => "nmi" | "international nautical mile",
	Feet(0.3048) => "ft" | "'" | "feet",
	Inch(0.0254) => "in" | "\"" | "inch" | "inches",
}

dec_unit! {
	FlowRate
	CubicMetrePerSecond => "m³/s" | "m3/s",
	CubicMetrePerHour(1.0/3_600.0) => "m³/h" | "m3/h",
	LitrePerMinute(1.0/60_000.0) => "l/min" | "L/min",
}

dec_unit! {
	Frequency
	Hertz => "Hz",
	Kilohertz(1000.0) => "kHz",
	Megahertz(1000000.0) => "MHz",
}

impl Quantity<Frequency> {
	/// Checks if the frequency is alternating current
	#[inline]
	#[must_use]
	pub fn is_ac(&self) -> bool {
		self.value != n!(0)
	}

	/// Checks if the frequency is direct current
	#[inline]
	#[must_use]
	pub fn is_dc(&self) -> bool {
		self.value == n!(0)
	}
}

dec_unit! {
	Voltage
	Volts => "V",
	Kilovolts(1000.0) => "kV",
}

dec_unit! {
	Power
	Watt => "W",
	Kilowatt(1000.0) => "kW",
	Megawatt(1000000.0) => "MW",
	Gigawatt(1000000000.0) => "GW",
}

dec_unit! {
	Pressure
	Bar => "bar" | "bars",
	MegaPascal(10.0) => "MPa" | "Mpa",
	PoundPerSquareInch(0.068947573) => "psi",
}

dec_unit! {
	Speed
	KilometresPerHour => "km/h" | "kph" | "kmh" | "kmph",
	MilesPerHour(1.609344) => "mph",
	Knots(1.852) => "knots",
	MetresPerSecond(3.6) => "m/s" | "m/sec",
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum WalkSpeed {
	Speed(Speed),
	Walk,
}

impl Default for WalkSpeed {
	fn default() -> Self {
		Self::Speed(Speed::default())
	}
}

impl Display for WalkSpeed {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			Self::Speed(unit) => write!(f, "{unit}"),
			Self::Walk => f.write_str("walk"),
		}
	}
}

impl FromOsmStr for WalkSpeed {
	fn from_osm_str(s: &str) -> Option<Self> {
		if s == "walk" {
			Some(Self::Walk)
		} else {
			Speed::from_osm_str(s).map(Self::Speed)
		}
	}
}

#[cfg(feature = "serde")]
#[derive(Serialize, Deserialize)]
#[serde(rename = "WalkSpeed")]
enum WalkSpeedSerde {
	KilometresPerHour,
	MilesPerHour,
	Knots,
	MetresPerSecond,
	Walk,
}

#[cfg(feature = "serde")]
impl Serialize for WalkSpeed {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: Serializer,
	{
		match self {
			Self::Speed(Speed::KilometresPerHour) => WalkSpeedSerde::KilometresPerHour,
			Self::Speed(Speed::MilesPerHour) => WalkSpeedSerde::MilesPerHour,
			Self::Speed(Speed::Knots) => WalkSpeedSerde::Knots,
			Self::Speed(Speed::MetresPerSecond) => WalkSpeedSerde::MetresPerSecond,
			Self::Walk => WalkSpeedSerde::Walk,
		}
		.serialize(serializer)
	}
}

#[cfg(feature = "serde")]
impl<'de> Deserialize<'de> for WalkSpeed {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: Deserializer<'de>,
	{
		let speed_serde = WalkSpeedSerde::deserialize(deserializer)?;
		Ok(match speed_serde {
			WalkSpeedSerde::KilometresPerHour => Self::Speed(Speed::KilometresPerHour),
			WalkSpeedSerde::MilesPerHour => Self::Speed(Speed::MilesPerHour),
			WalkSpeedSerde::Knots => Self::Speed(Speed::Knots),
			WalkSpeedSerde::MetresPerSecond => Self::Speed(Speed::MetresPerSecond),
			WalkSpeedSerde::Walk => Self::Walk,
		})
	}
}

impl PartialUnit for WalkSpeed {
	fn partial_default() -> Option<Self> {
		Some(Self::default())
	}

	fn from_special_quantity_osm_str(s: &str) -> Option<Quantity<Self>> {
		if s == "walk" {
			Some(Quantity::new(Number::new(1.0), Self::Walk))
		} else if let Some((_, mut speed_str)) = s.split_once("zone") {
			speed_str = speed_str.trim_start_matches(':');
			let speed = speed_str.parse().ok()?;
			Some(Quantity::new(speed, Speed::KilometresPerHour.into()))
		} else {
			let (_, speed_str) = s.split_once(':')?;
			if speed_str == "walk" {
				Some(Quantity::new(Number::new(1.0), Self::Walk))
			} else {
				Quantity::from_osm_str_with_unit_fn_base(speed_str, Self::partial_default)
			}
		}
	}

	fn partial_convert(&self, to: &Self, value: Number) -> Option<Number> {
		match (self, to) {
			(Self::Speed(from), Self::Speed(to)) => from.partial_convert(to, value),
			(Self::Walk, Self::Walk) => Some(value),
			_ => None,
		}
	}
}

impl From<Speed> for WalkSpeed {
	fn from(value: Speed) -> Self {
		Self::Speed(value)
	}
}

impl From<Quantity<Speed>> for Quantity<WalkSpeed> {
	fn from(value: Quantity<Speed>) -> Self {
		Self {
			value: value.value,
			unit: value.unit.into(),
		}
	}
}

impl Quantity<WalkSpeed> {
	#[inline]
	#[must_use]
	pub const fn into_speed(self, walk: Quantity<Speed>) -> Quantity<Speed> {
		match self.unit {
			WalkSpeed::Speed(unit) => Quantity {
				value: self.value,
				unit,
			},
			WalkSpeed::Walk => walk,
		}
	}
}

dec_unit! {
	Weight
	Tonne => "t",
	Kilogram(0.001) => "kg",
	ShortTon(0.9071847) => "st" | "ST" | "ton" | "tons",
	LongTon(1.016047) => "lt",
	PoundMass(0.00045359237) => "lbs" | "lb",
	LongHundredweight(0.0508) => "cwt",
}

dec_unit! {
	Volume
	CubicMetre => "m³" | "m3",
	Litre(0.001) => "l" | "L",
}

dec_unit! {
	Duration
	Second => "s" | "second" | "seconds" | "sec" | "secs",
	Minute(60.0) => "min" | "minute" | "minutes",
	Hour(3600.0) => "h" |"hour" | "hours" | "hr" | "hrs" | "hs",
	Night => "nights" | "night",
	Day => "d" | "day" | "days" | "ds",
	Week => "w" | "week" | "weeks",
	Month => "M" | "month" | "months",
	Year => "y" |"year" | "years" | "ys" | "a",
}

#[derive(Debug, Clone)]
pub enum TimeToDurationError {
	ImpreciseConversion(Quantity<Duration>),
}

impl TryFrom<Quantity<Duration>> for time::Duration {
	type Error = TimeToDurationError;

	fn try_from(value: Quantity<Duration>) -> Result<Self, Self::Error> {
		value
			.partial_to(Duration::Second)
			.map(|unit| Self::from_secs_f64(unit.value.into()))
			.ok_or(TimeToDurationError::ImpreciseConversion(value))
	}
}

#[cfg(feature = "chrono")]
impl TryFrom<Quantity<Duration>> for chrono::Duration {
	type Error = TimeToDurationError;

	fn try_from(value: Quantity<Duration>) -> Result<Self, Self::Error> {
		Ok(Self::from_std(value.try_into()?).unwrap())
	}
}

#[cfg(test)]
mod tests {
	use crate::{
		quantity,
		units::{Number, Quantity, Speed, WalkSpeed},
		FromOsmStr,
	};

	#[test]
	fn parse_zone() {
		assert_eq!(
			Quantity::from_osm_str("FR:zone30"),
			Some(quantity!(30 WalkSpeed::from(Speed::KilometresPerHour)))
		);
		assert_eq!(
			Quantity::from_osm_str("DE:zone:30"),
			Some(quantity!(30 WalkSpeed::from(Speed::KilometresPerHour)))
		);
		assert_eq!(
			Quantity::from_osm_str("zone20"),
			Some(quantity!(20 WalkSpeed::from(Speed::KilometresPerHour)))
		);
		assert_eq!(
			Quantity::from_osm_str("AT:30"),
			Some(quantity!(30 WalkSpeed::from(Speed::KilometresPerHour)))
		);
		assert_eq!(
			Quantity::from_osm_str("NL:walk"),
			Some(Quantity::new(Number::new(1.0), WalkSpeed::Walk))
		);
		assert_eq!(Quantity::<WalkSpeed>::from_osm_str("zone30:ends"), None);
	}
}
