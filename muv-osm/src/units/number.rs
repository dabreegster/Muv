use std::{
	borrow::Cow,
	cmp::Ordering,
	f32::consts,
	fmt::{self, Debug, Display, Formatter},
	num::Saturating,
	ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign},
	str::FromStr,
};

use crate::{FromOsmStr, ToOsmStr};

#[cfg(feature = "serde")]
use serde::{de::Visitor, Deserialize, Deserializer, Serialize, Serializer};

pub const PI: Number = Number::new_unchecked(consts::PI);

#[derive(Debug, Clone, Copy)]
pub struct Number {
	number: f32,
	pub significant_figures: u8,
}

impl Number {
	pub const INFINITY: Self = Self::new_unchecked(f32::INFINITY);

	/// # Panics
	/// Panics if `NaN` is passed as the argument.
	#[inline]
	#[must_use]
	pub fn new(num: f32) -> Self {
		assert!(!num.is_nan(), "float expected not to be NaN");
		Self::new_unchecked(num)
	}

	#[inline]
	#[must_use]
	pub const fn new_unchecked(num: f32) -> Self {
		Self {
			number: num,
			significant_figures: u8::MAX,
		}
	}

	#[inline]
	#[must_use]
	pub const fn number(self) -> f32 {
		self.number
	}

	/// # Panics
	/// Panics if `NaN` is passed as the argument.
	#[inline]
	pub fn set_number(&mut self, num: f32) {
		assert!(!num.is_nan(), "float expected not to be NaN");
		self.number = num;
	}

	#[inline]
	#[must_use]
	pub const fn with_significant_figures(mut self, sig: u8) -> Self {
		self.significant_figures = sig;
		self
	}

	#[inline]
	#[must_use]
	pub const fn with_infinite_precision(mut self) -> Self {
		self.significant_figures = u8::MAX;
		self
	}

	#[inline]
	#[must_use]
	pub fn parse_start(s: &str) -> Option<(Self, &str)> {
		let mut decimal = false;

		let mut digit = false;
		let mut sig: Saturating<u8> = Saturating(0);
		let mut zeros: Saturating<u8> = Saturating(0);

		let mut i = 0;
		for b in s.bytes() {
			match b {
				b'-' if i == 0 => {}
				b'0' if !digit => {}
				b'0' => {
					if decimal {
						sig += 1;
					} else {
						zeros += 1;
					}
				}
				b'1'..=b'9' => {
					sig += zeros + Saturating(1);
					zeros.0 = 0;
					digit = true;
				}
				b'.' if !decimal => {
					decimal = true;
				}
				_ => break,
			}
			i += 1;
		}

		let number = s[..i].parse().ok()?;
		Some((
			Self {
				number,
				significant_figures: sig.0,
			},
			&s[i..],
		))
	}
}

#[derive(Debug)]
pub enum NumberParseError {
	UnknownPrefix,
	UnknownSuffix,
}

impl Display for NumberParseError {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		f.write_str(match self {
			Self::UnknownPrefix => "number starts with unknown characters",
			Self::UnknownSuffix => "number ends with unknown characters",
		})
	}
}

impl FromStr for Number {
	type Err = NumberParseError;
	fn from_str(s: &str) -> Result<Self, Self::Err> {
		let (n, s) = Self::parse_start(s).ok_or(NumberParseError::UnknownPrefix)?;
		s.is_empty()
			.then_some(n)
			.ok_or(NumberParseError::UnknownSuffix)
	}
}

impl FromOsmStr for Number {
	fn from_osm_str(s: &str) -> Option<Self> {
		let (n, s) = Self::parse_start(s)?;
		s.is_empty().then_some(n)
	}
}

impl Display for Number {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		let abs = self.number.abs();
		let digits = if abs < 1.0 { 0 } else { floor_log(abs) + 1 };
		if self.significant_figures < digits {
			let shift = (10.0f64).powi((digits - self.significant_figures).into());
			let n = (f64::from(self.number) / shift).round() * shift;
			write!(f, "{n:.0}")
		} else {
			let dec: usize = (self.significant_figures - digits).into();
			write!(f, "{:.dec$}", self.number)
		}
	}
}

impl ToOsmStr for Number {
	fn to_osm_str(self) -> Option<Cow<'static, str>> {
		Some(self.to_string().into())
	}
}

#[allow(
	clippy::as_conversions,
	clippy::cast_possible_truncation,
	clippy::cast_sign_loss
)]
#[inline]
#[must_use]
fn floor_log(n: f32) -> u8 {
	n.log10() as u8
}

#[cfg(feature = "serde")]
impl Serialize for Number {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: Serializer,
	{
		self.to_string().serialize(serializer)
	}
}

#[cfg(feature = "serde")]
struct NumberVisitor;

#[cfg(feature = "serde")]
impl<'de> Visitor<'de> for NumberVisitor {
	type Value = Number;

	fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
		formatter.write_str("a decimal number")
	}

	fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
	where
		E: serde::de::Error,
	{
		Number::from_str(v).map_err(E::custom)
	}
}

#[cfg(feature = "serde")]
impl<'de> Deserialize<'de> for Number {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: Deserializer<'de>,
	{
		deserializer.deserialize_str(NumberVisitor)
	}
}

impl Neg for Number {
	type Output = Self;
	fn neg(mut self) -> Self::Output {
		self.number = -self.number;
		self
	}
}

impl Mul for Number {
	type Output = Self;
	fn mul(mut self, rhs: Self) -> Self::Output {
		self *= rhs;
		self
	}
}

impl MulAssign for Number {
	fn mul_assign(&mut self, rhs: Self) {
		self.number *= rhs.number;
		self.significant_figures = self.significant_figures.min(rhs.significant_figures);
	}
}

impl Div for Number {
	type Output = Self;
	fn div(mut self, rhs: Self) -> Self::Output {
		self /= rhs;
		self
	}
}

impl DivAssign for Number {
	fn div_assign(&mut self, rhs: Self) {
		self.number /= rhs.number;
		assert!(!self.number.is_nan(), "division by 0");
		self.significant_figures = self.significant_figures.min(rhs.significant_figures);
	}
}

impl Add for Number {
	type Output = Self;
	fn add(mut self, rhs: Self) -> Self::Output {
		self += rhs;
		self
	}
}

impl AddAssign for Number {
	fn add_assign(&mut self, rhs: Self) {
		self.number += rhs.number;
		self.significant_figures = self.significant_figures.min(rhs.significant_figures);
	}
}

impl Sub for Number {
	type Output = Self;
	fn sub(mut self, rhs: Self) -> Self::Output {
		self -= rhs;
		self
	}
}

impl SubAssign for Number {
	fn sub_assign(&mut self, rhs: Self) {
		self.number -= rhs.number;
		self.significant_figures = self.significant_figures.min(rhs.significant_figures);
	}
}

impl PartialEq for Number {
	fn eq(&self, other: &Self) -> bool {
		self.number == other.number
			&& (self.significant_figures == other.significant_figures
				|| self.number.is_infinite()
				|| other.number.is_infinite())
	}
}

impl Eq for Number {}

impl PartialOrd for Number {
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
		Some(self.cmp(other))
	}
}

impl Ord for Number {
	fn cmp(&self, other: &Self) -> Ordering {
		let res = self.number.partial_cmp(&other.number).unwrap();
		match res {
			Ordering::Equal => self.significant_figures.cmp(&other.significant_figures),
			_ => res,
		}
	}
}

impl From<Number> for f32 {
	fn from(value: Number) -> Self {
		value.number
	}
}
impl From<&Number> for f32 {
	fn from(value: &Number) -> Self {
		value.number
	}
}

impl From<Number> for f64 {
	fn from(value: Number) -> Self {
		value.number.into()
	}
}
impl From<&Number> for f64 {
	fn from(value: &Number) -> Self {
		value.number.into()
	}
}

impl From<f32> for Number {
	fn from(value: f32) -> Self {
		Self::new(value)
	}
}

impl From<u16> for Number {
	fn from(value: u16) -> Self {
		Self::new(value.into())
	}
}
impl From<i16> for Number {
	fn from(value: i16) -> Self {
		Self::new(value.into())
	}
}

#[macro_export]
macro_rules! n {
	($l:literal) => {{
		use $crate::FromOsmStr;
		$crate::units::Number::from_osm_str(stringify!($l)).unwrap()
	}};
}

#[cfg(test)]
mod tests {
	use crate::units::Number;

	#[test]
	fn parse() {
		assert_eq!(
			Number::parse_start("-123"),
			Some((
				Number {
					number: -123.0,
					significant_figures: 3
				},
				""
			))
		);
		assert_eq!(
			Number::parse_start("100m"),
			Some((
				Number {
					number: 100.0,
					significant_figures: 1
				},
				"m"
			))
		);
		assert_eq!(
			Number::parse_start("-135.42"),
			Some((
				Number {
					number: -135.42,
					significant_figures: 5
				},
				""
			))
		);
		assert_eq!(
			Number::parse_start("-0.013"),
			Some((
				Number {
					number: -0.013,
					significant_figures: 2
				},
				""
			))
		);
		assert_eq!(
			Number::parse_start("64.0 bits"),
			Some((
				Number {
					number: 64.0,
					significant_figures: 3
				},
				" bits"
			))
		);
		assert_eq!(Number::parse_start("--14"), None);
		assert_eq!(Number::parse_start(""), None);
		assert_eq!(
			Number::parse_start("-0.00s"),
			Some((
				Number {
					number: -0.0,
					significant_figures: 0
				},
				"s"
			))
		);
	}

	#[test]
	fn mul() {
		assert_eq!(n!(12) * n!(4.5), n!(54));
		assert_eq!(n!(-9) * n!(10.0), n!(-90));
	}

	#[test]
	fn div() {
		assert_eq!(n!(87.4) / n!(2), n!(43.7).with_significant_figures(1));
	}

	#[test]
	fn add() {
		assert_eq!(n!(24) + n!(5), n!(29).with_significant_figures(1));
		assert_eq!(n!(42.5) + n!(0.23), n!(42.73).with_significant_figures(2));
		assert_eq!(n!(0.4) + n!(92), n!(92.4).with_significant_figures(1));
		assert_eq!(n!(0.2) + n!(0.1), n!(0.3));
	}

	#[test]
	fn sub() {
		assert_eq!(n!(8.5) - n!(3), n!(5.5).with_significant_figures(1));
	}

	#[test]
	fn ord() {
		assert!(n!(-5) < n!(0.98));
		assert!(n!(6.6) > n!(6.59));
	}

	#[test]
	#[allow(clippy::float_cmp)]
	fn to_f64() {
		assert_eq!(f64::from(&n!(12.5)), 12.5);
		assert_eq!(f64::from(&n!(-20)), -20.0);
	}

	#[test]
	fn to_string() {
		assert_eq!(n!(-0.45).to_string(), "-0.45");
		assert_eq!(n!(1000).to_string(), "1000");
		assert_eq!(n!(24.3).with_significant_figures(2).to_string(), "24");
		assert_eq!(n!(156).with_significant_figures(1).to_string(), "200");
	}

	#[test]
	fn long_number() {
		let (n, s) = Number::parse_start("01234567890123456789012345678901234567890123456789.012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789XYZ").unwrap();

		assert_eq!(s, "XYZ");
		assert_eq!(n.significant_figures, u8::MAX);
	}

	#[test]
	#[cfg(feature = "serde")]
	fn serde() {
		use serde_test::{assert_tokens, Configure, Token};

		let number = n!(-23.78);

		assert_tokens(&number.readable(), &[Token::String("-23.78")]);
		assert_tokens(&number.compact(), &[Token::String("-23.78")]);
	}
}
