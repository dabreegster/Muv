use std::{
	collections::{
		btree_map::{IntoIter, Iter, IterMut},
		BTreeMap,
	},
	iter::{once, FilterMap},
};

#[cfg(feature = "lanes")]
use std::collections::btree_map::Entry;

use crate::{simple_enum, FromOsmStr, Taglike, ToOsmStr};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

simple_enum!(TMode {
	#[osm_str()]
	All,

	#[osm_str()]
	LandBased,
	Foot,

	Animal,
	Dog,
	Horse,

	Ski,
	#[osm_str()]
	SkiNordic,
	#[osm_str()]
	SkiAlpine,
	#[osm_str()]
	SkiTelemark,
	InlineSkates,

	Vehicle,
	Bicycle,
	ElectricBicycle,
	Mtb,
	CargoBike,
	KickScooter,

	Stroller,

	Carriage,
	CycleRickshaw,
	HandCart,
	Trailer,
	Caravan,

	MotorVehicle,
	Motorcycle,
	Moped,
	SpeedPedelec,
	Mofa,
	SmallElectricVehicle,

	DoubleTrackedMotorVehicle,
	Motorcar,
	Motorhome,
	TouristBus,
	Coach,
	Goods,
	Hgv,
	HhvArticulated,
	BDouble,
	Agricultural,
	#[osm_str("auto_rickshaw" | "tricycle")]
	AutoRickshaw,
	Nev,
	GolfCart,
	Atv,
	Ohv,
	Snowmobile,

	Psv,
	Taxi,
	Minibus,
	ShareTaxi,

	Bus,
	SchoolBus,
	TruckBus,

	Hov,
	Carpool,
	CarSharing,
	Emergency,
	Disabled,

	Hazmat,
	#[osm_str()]
	HazmatWater,
	#[osm_str()]
	HazmatExplosive,
	#[osm_str()]
	HazmatE,
	#[osm_str()]
	HazmatD,
	#[osm_str()]
	HazmatC,
	#[osm_str()]
	HazmatB,
	#[osm_str()]
	HazmatA,

	#[osm_str()]
	RailBased,
	#[osm_str()]
	Train,
	#[osm_str()]
	Monorail,
	#[osm_str()]
	Funicular,
	#[osm_str()]
	LightRail,
	#[osm_str()]
	Subway,
	#[osm_str()]
	Tram,

	#[osm_str()]
	WaterBased,
	Swimming,
	IceSkates,

	Boat,
	Motorboat,
	Sailboat,
	Canoe,

	FishingVessel,

	Ship,
	Passenger,
	Cargo,
	Bulk,
	Tanker,
	Container,
	Imdg,
	Isps,

	#[osm_str()]
	AirBased,
	#[osm_str()]
	Aeroplane,
});

enum TModeInnerIter<Ski, Hazmat> {
	Nothing,
	Ski(Ski),
	Hazmat(Hazmat),
}

impl<'a, T: Taglike<'a>, Ski: Iterator<Item = (TMode, T)>, Hazmat: Iterator<Item = (TMode, T)>>
	Iterator for TModeInnerIter<Ski, Hazmat>
{
	type Item = (TMode, T);

	fn next(&mut self) -> Option<Self::Item> {
		match self {
			Self::Nothing => None,
			Self::Ski(i) => i.next(),
			Self::Hazmat(i) => i.next(),
		}
	}
}

type IterInnerTags<'a, T> = TModeInnerIter<
	FilterMap<<T as Taglike<'a>>::Iter, fn((&str, T)) -> Option<(TMode, T)>>,
	FilterMap<<T as Taglike<'a>>::Iter, fn((&str, T)) -> Option<(TMode, T)>>,
>;

impl TMode {
	#[inline]
	pub fn iter_tags_with_all<'a, T: Taglike<'a>>(tags: T) -> impl Iterator<Item = (Self, T)> + 'a {
		once((Self::All, tags)).chain(Self::iter_tags(tags))
	}

	#[inline]
	pub fn iter_tags<'a, T: Taglike<'a>>(tags: T) -> impl Iterator<Item = (Self, T)> + 'a {
		tags.iter_subtags()
			.filter_map(|(key, tag)| {
				let mode = Self::from_osm_str(key)?;
				let i = Self::iter_inner_tag(mode, tag);
				Some(once((mode, tag)).chain(i))
			})
			.flatten()
	}

	#[inline]
	fn iter_inner_tag<'a, T: Taglike<'a>>(mode: Self, tag: T) -> IterInnerTags<'a, T> {
		match mode {
			Self::Ski => TModeInnerIter::Ski(
				tag.iter_subtags()
					.filter_map(|(key, tag)| Some((Self::from_osm_ski_str(key)?, tag))),
			),
			Self::Hazmat => TModeInnerIter::Hazmat(
				tag.iter_subtags()
					.filter_map(|(key, tag)| Some((Self::from_osm_hazmat_str(key)?, tag))),
			),
			_ => TModeInnerIter::Nothing,
		}
	}
}

impl TMode {
	#[must_use]
	pub fn from_osm_ski_str(s: &str) -> Option<Self> {
		Some(match s {
			"nordic" => Self::SkiNordic,
			"alpine" => Self::SkiAlpine,
			"telemark" => Self::SkiTelemark,
			_ => return None,
		})
	}

	#[must_use]
	pub fn from_osm_hazmat_str(s: &str) -> Option<Self> {
		Some(match s {
			"water" => Self::HazmatWater,
			"explosive" => Self::HazmatExplosive,
			"A" => Self::HazmatA,
			"B" => Self::HazmatB,
			"C" => Self::HazmatC,
			"D" => Self::HazmatD,
			"E" => Self::HazmatE,
			_ => return None,
		})
	}
}

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct TModes<T>(BTreeMap<TMode, T>);

impl<T> TModes<T> {
	#[must_use]
	pub const fn new() -> Self {
		Self(BTreeMap::new())
	}

	#[inline]
	pub fn all(value: T) -> Self {
		Self::from([(TMode::All, value)])
	}

	#[inline]
	pub fn from<const N: usize>(value: [(TMode, T); N]) -> Self {
		Self(BTreeMap::from(value))
	}
}

impl<T> Default for TModes<T> {
	fn default() -> Self {
		Self::new()
	}
}

impl<T> TModes<T> {
	/// Set the value associated with this mode.
	///
	/// If this mode was associated with a value before, the value is replaced and the old value is returned.
	///
	/// ```rust
	/// use muv_osm::{TMode, TModes};
	///
	/// let mut modes = TModes::new();
	///
	/// modes.set(TMode::Foot, 9);
	/// assert_eq!(modes.get(TMode::Foot), Some(&9));
	///
	/// let old = modes.set(TMode::Foot, 3);
	/// assert_eq!(old, Some(9));
	/// assert_eq!(modes.get(TMode::Foot), Some(&3));
	/// ```
	#[inline]
	pub fn set(&mut self, mode: TMode, val: T) -> Option<T> {
		self.0.insert(mode, val)
	}

	/// Get the value associated with this exact mode
	///
	/// This differs from [`get`][`TModes::get`], as it doesn't iterate the [hierarchy](`TMode::iter_hierarchy`).
	///
	/// ```rust
	/// use muv_osm::{TMode, TModes};
	///
	/// let modes = TModes::from([
	///     (TMode::Vehicle, 4),
	///     (TMode::Bicycle, 7),
	/// ]);
	///
	/// assert_eq!(modes.get_raw(TMode::Bicycle), Some(&7));
	/// assert_eq!(modes.get_raw(TMode::Vehicle), Some(&4));
	/// assert_eq!(modes.get_raw(TMode::Motorcar), None);
	/// ```
	#[inline]
	#[must_use]
	pub fn get_raw(&self, mode: TMode) -> Option<&T> {
		self.0.get(&mode)
	}

	#[inline]
	#[must_use]
	pub fn get_raw_mut(&mut self, mode: TMode) -> Option<&mut T> {
		self.0.get_mut(&mode)
	}

	/// Get the associated value of the mode closest in [hierarchy](`TMode::iter_hierarchy`) to the `mode` provided.
	///
	/// To only check the exact [`TMode`] without iterating the hiearchy, check out [`get_raw`][`TModes::get_raw`].
	///
	/// ```rust
	/// use muv_osm::{TMode, TModes};
	///
	/// let modes = TModes::from([
	///     (TMode::Vehicle, 4),
	///     (TMode::Bicycle, 7),
	/// ]);
	///
	/// assert_eq!(modes.get(TMode::Bicycle), Some(&7));
	/// assert_eq!(modes.get(TMode::Vehicle), Some(&4));
	/// assert_eq!(modes.get(TMode::Motorcar), Some(&4));
	/// assert_eq!(modes.get(TMode::Foot), None);
	/// ```
	#[inline]
	#[must_use]
	pub fn get(&self, mode: TMode) -> Option<&T> {
		mode.iter_hierarchy().find_map(|mode| self.get_raw(mode))
	}

	#[inline]
	#[must_use]
	pub fn get_mut(&mut self, mode: TMode) -> Option<&mut T> {
		let found = mode
			.iter_hierarchy()
			.find(|mode| self.get_raw(*mode).is_some())?;
		self.get_raw_mut(found)
	}

	#[inline]
	#[must_use]
	pub fn len(&self) -> usize {
		self.0.len()
	}

	#[inline]
	#[must_use]
	pub fn is_empty(&self) -> bool {
		self.0.is_empty()
	}

	#[cfg(feature = "lanes")]
	pub(crate) fn entry(&mut self, key: TMode) -> Entry<TMode, T> {
		self.0.entry(key)
	}

	#[must_use]
	pub fn build(&self) -> TModes<&T> {
		self.map(|mode, _| self.get(mode))
	}

	pub fn into_map<R>(self, mut f: impl FnMut(TMode, T) -> Option<R>) -> TModes<R> {
		TModes(
			self.0
				.into_iter()
				.filter_map(|(mode, val)| Some((mode, f(mode, val)?)))
				.collect(),
		)
	}

	pub fn map<R>(&self, mut f: impl FnMut(TMode, &T) -> Option<R>) -> TModes<R> {
		TModes(
			self.0
				.iter()
				.filter_map(|(mode, val)| Some((*mode, f(*mode, val)?)))
				.collect(),
		)
	}

	#[cfg(feature = "lanes")]
	pub(crate) fn zip<O, R>(
		&self,
		other: &TModes<O>,
		mut f: impl FnMut(Option<&T>, Option<&O>) -> Option<R>,
	) -> TModes<R> {
		let mut res = TModes(BTreeMap::new());
		for mode in self.0.keys().chain(other.0.keys()).copied() {
			if res.get_raw(mode).is_none() {
				if let Some(val) = f(self.get(mode), other.get(mode)) {
					res.set(mode, val);
				}
			}
		}
		res
	}
}

impl<T> FromIterator<(TMode, T)> for TModes<T> {
	fn from_iter<I: IntoIterator<Item = (TMode, T)>>(iter: I) -> Self {
		Self(iter.into_iter().collect())
	}
}

impl<T> TModes<T> {
	pub fn iter(&self) -> Iter<TMode, T> {
		self.0.iter()
	}

	pub fn iter_mut(&mut self) -> IterMut<TMode, T> {
		self.0.iter_mut()
	}
}

impl<T> IntoIterator for TModes<T> {
	type Item = (TMode, T);
	type IntoIter = IntoIter<TMode, T>;
	fn into_iter(self) -> Self::IntoIter {
		self.0.into_iter()
	}
}
impl<'a, T> IntoIterator for &'a mut TModes<T> {
	type Item = (&'a TMode, &'a mut T);
	type IntoIter = IterMut<'a, TMode, T>;
	fn into_iter(self) -> Self::IntoIter {
		self.0.iter_mut()
	}
}
impl<'a, T> IntoIterator for &'a TModes<T> {
	type Item = (&'a TMode, &'a T);
	type IntoIter = Iter<'a, TMode, T>;
	fn into_iter(self) -> Self::IntoIter {
		self.0.iter()
	}
}

#[cfg(test)]
mod tests {
	use crate::{new_tag, TMode, Taglike};

	#[test]
	fn iter_tags() {
		let tags = new_tag! {
			ski = "15",
			ski:alpine = "12",
			motorcar = "23",
			bus = "49",
			hazmat:C = "32",
		};

		let modes: Vec<(TMode, u8)> = TMode::iter_tags(&tags)
			.filter_map(|(mode, tag)| Some((mode, tag.value()?.parse().unwrap())))
			.collect();

		assert_eq!(
			modes,
			&[
				(TMode::Ski, 15),
				(TMode::SkiAlpine, 12),
				(TMode::Motorcar, 23),
				(TMode::Bus, 49),
				(TMode::HazmatC, 32)
			]
		);
	}
}
