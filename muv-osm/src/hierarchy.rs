use std::iter::FusedIterator;

use crate::TMode;

impl TMode {
	/// Returns the `TMode` this mode is the direct child of.
	///
	/// ```rust
	/// # use muv_osm::TMode;
	/// assert_eq!(TMode::Bicycle.hierarchy_parent(), Some(TMode::Vehicle));
	/// assert_eq!(TMode::Motorcycle.hierarchy_parent(), Some(TMode::MotorVehicle));
	/// assert_eq!(TMode::Boat.hierarchy_parent(), Some(TMode::WaterBased));
	/// assert_eq!(TMode::AirBased.hierarchy_parent(), Some(TMode::All));
	/// assert_eq!(TMode::All.hierarchy_parent(), None);
	/// ```
	///
	/// [OSM Wiki](https://wiki.openstreetmap.org/wiki/Key:access#Land-based_transportation)
	#[must_use]
	#[inline]
	pub const fn hierarchy_parent(self) -> Option<Self> {
		self.hierarchy_len_parent().0
	}

	const fn hierarchy_len_parent(self) -> (Option<Self>, usize) {
		let (mode, len) = match self {
			Self::All => return (None, 1),

			Self::LandBased | Self::RailBased | Self::WaterBased | Self::AirBased => (Self::All, 2),

			Self::Foot | Self::Animal | Self::Ski | Self::InlineSkates | Self::Vehicle => {
				(Self::LandBased, 3)
			}

			Self::Dog | Self::Horse => (Self::Animal, 4),

			Self::SkiNordic | Self::SkiAlpine | Self::SkiTelemark => (Self::Ski, 4),

			Self::Bicycle
			| Self::KickScooter
			| Self::Stroller
			| Self::Carriage
			| Self::CycleRickshaw
			| Self::HandCart
			| Self::Trailer
			| Self::MotorVehicle => (Self::Vehicle, 4),

			Self::ElectricBicycle | Self::Mtb | Self::CargoBike => (Self::Bicycle, 5),

			Self::Caravan => (Self::Trailer, 5),

			Self::Motorcycle
			| Self::Moped
			| Self::Mofa
			| Self::SmallElectricVehicle
			| Self::DoubleTrackedMotorVehicle
			| Self::Psv
			| Self::Hov
			| Self::CarSharing
			| Self::Emergency
			| Self::Disabled
			| Self::Hazmat => (Self::MotorVehicle, 5),

			Self::SpeedPedelec => (Self::Moped, 6),

			Self::Motorcar
			| Self::Motorhome
			| Self::TouristBus
			| Self::Goods
			| Self::Hgv
			| Self::Agricultural
			| Self::AutoRickshaw
			| Self::Nev
			| Self::GolfCart
			| Self::Atv
			| Self::Ohv
			| Self::Snowmobile => (Self::DoubleTrackedMotorVehicle, 6),

			Self::Coach => (Self::TouristBus, 7),

			Self::HhvArticulated | Self::BDouble => (Self::Hgv, 7),

			Self::Bus | Self::Taxi | Self::Minibus | Self::ShareTaxi => (Self::Psv, 6),

			Self::SchoolBus | Self::TruckBus => (Self::Bus, 7),

			Self::Carpool => (Self::Hov, 6),

			Self::HazmatWater | Self::HazmatExplosive | Self::HazmatE => (Self::Hazmat, 6),
			Self::HazmatD => (Self::HazmatE, 7),
			Self::HazmatC => (Self::HazmatD, 8),
			Self::HazmatB => (Self::HazmatC, 9),
			Self::HazmatA => (Self::HazmatB, 10),

			Self::Train
			| Self::Monorail
			| Self::Funicular
			| Self::LightRail
			| Self::Subway
			| Self::Tram => (Self::RailBased, 3),

			Self::Swimming | Self::IceSkates | Self::Boat | Self::FishingVessel | Self::Ship => {
				(Self::WaterBased, 3)
			}

			Self::Motorboat | Self::Sailboat | Self::Canoe => (Self::Boat, 4),

			Self::Passenger | Self::Cargo | Self::Isps => (Self::Ship, 4),

			Self::Bulk | Self::Tanker | Self::Container | Self::Imdg => (Self::Cargo, 5),

			Self::Aeroplane => (Self::AirBased, 3),
		};
		(Some(mode), len)
	}

	/// Iterates up the hierarchy using [`TMode::hierarchy_parent`], starting with itself and yielding the parents on the way.
	/// ```
	/// # use muv_osm::TMode;
	/// let mut iter = TMode::HazmatExplosive.iter_hierarchy();
	///
	/// assert_eq!(Some(TMode::HazmatExplosive), iter.next());
	/// assert_eq!(Some(TMode::Hazmat), iter.next());
	/// assert_eq!(Some(TMode::MotorVehicle), iter.next());
	/// assert_eq!(Some(TMode::Vehicle), iter.next());
	/// assert_eq!(Some(TMode::LandBased), iter.next());
	/// assert_eq!(Some(TMode::All), iter.next());
	/// assert_eq!(None, iter.next());
	/// ```
	#[inline]
	#[must_use]
	pub const fn iter_hierarchy(self) -> TModeHierarchyIter {
		TModeHierarchyIter { next: Some(self) }
	}
}

/// Iterator that returns the [hierarchical parent](TMode::hierarchy_parent) of the [`TMode`] yielded the iteration before
///
/// This `struct` is created by the [`TMode::iter_hierarchy`] function.
#[derive(Debug, Clone)]
pub struct TModeHierarchyIter {
	next: Option<TMode>,
}

impl TModeHierarchyIter {
	/// Return next value without advancing the iterator
	/// ```
	/// # use muv_osm::TMode;
	/// let mut iter = TMode::Bus.iter_hierarchy();
	///
	/// assert_eq!(Some(TMode::Bus), iter.next());
	/// assert_eq!(Some(TMode::Psv), iter.peek());
	/// assert_eq!(Some(TMode::Psv), iter.next());
	/// ```
	#[must_use]
	pub const fn peek(&self) -> Option<TMode> {
		self.next
	}
}

impl Iterator for TModeHierarchyIter {
	type Item = TMode;

	fn next(&mut self) -> Option<Self::Item> {
		let this = self.next?;
		self.next = this.hierarchy_parent();
		Some(this)
	}

	fn size_hint(&self) -> (usize, Option<usize>) {
		let len = self.next.map_or(0, |mode| mode.hierarchy_len_parent().1);
		(len, Some(len))
	}

	fn count(self) -> usize {
		self.len()
	}

	fn last(self) -> Option<Self::Item> {
		if self.next.is_some() {
			Some(TMode::All)
		} else {
			None
		}
	}

	fn max(self) -> Option<Self::Item> {
		self.next
	}

	fn min(self) -> Option<Self::Item> {
		self.last()
	}
}

impl ExactSizeIterator for TModeHierarchyIter {}

impl FusedIterator for TModeHierarchyIter {}

#[cfg(test)]
mod tests {
	use std::mem::transmute;

	use crate::TMode;

	#[test]
	fn len() {
		assert_eq!(TMode::All.iter_hierarchy().len(), 1);
		assert_eq!(TMode::Bicycle.iter_hierarchy().len(), 4);

		let mut empty = TMode::All.iter_hierarchy();
		empty.next();
		assert_eq!(empty.peek(), None);
		assert_eq!(empty.len(), 0);
	}

	fn all_modes() -> impl Iterator<Item = TMode> {
		const LAST: TMode = TMode::Aeroplane;
		(0..unsafe { transmute(LAST) }).map(|i: u8| unsafe { transmute(i) })
	}

	// Required for the `len`, `count` and `size_hint` functions
	#[test]
	fn count() {
		for mode in all_modes() {
			let mut count = 0;

			let mut next = Some(mode);
			while let Some(current) = next {
				count += 1;
				next = current.hierarchy_parent();
			}

			let len = mode.hierarchy_len_parent().1;
			assert_eq!(len, count, "{mode:?}");
		}
	}

	// Required for the `min` and `max` functions
	#[test]
	fn sorted() {
		for mode in all_modes() {
			let mut next = Some(mode);
			while let Some(current) = next {
				next = current.hierarchy_parent();
				assert!(Some(current) > next, "{current:?} -> {next:?}");
			}
		}
	}
}
