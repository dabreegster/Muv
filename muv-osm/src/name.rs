use std::collections::BTreeMap;

use crate::{get_tag, FromOsm, Taglike};

pub type Languages<'a> = BTreeMap<&'a str, Language<'a>>;

pub struct Language<'a> {
	pub value: &'a str,
	pub pronunciation: Option<&'a str>,
}

impl<'a, T: Taglike<'a>> FromOsm<'a, T> for Language<'a> {
	fn from_osm(tag: T) -> Option<Self> {
		Some(Self {
			value: tag.value()?,
			pronunciation: get_tag!(tag, pronunciation),
		})
	}
}
