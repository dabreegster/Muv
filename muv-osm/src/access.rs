use std::fmt::Debug;

use crate::{simple_enum, FromOsmStr, ToOsmStr};

simple_enum!(AccessLevel
/// Level of access or conditions required for access to an element
///
/// [OSM Wiki](https://wiki.openstreetmap.org/wiki/Key:access#List_of_possible_values)
{
	/// Preferred or designated route
	///
	/// One example of this might be a seperate bike path next to a road.
	Designated,
	/// Offically and legally accessible to the general public
	///
	/// This applies to most public roads and places.
	Yes,
	/// De-facto open to the general public but the owner is legally allowed to revoke this right in the future
	Permissive,
	/// Legally allowed but offically discouraged
	///
	/// This might be a narrow bridge discouraged for trucks by a sign.
	Discouraged,
	/// Only accessible when possessing a permit
	Permit,
	/// Only accessible for specific individuals
	///
	/// This might be a private driveway or a employee-only parking lot.
	Private,
	/// Access forbidden or not possible
	No,

	/// Only allowed to access when the vehicle or animal is dismounted.
	///
	/// This might be a graveyard where you are allowed to push your bike but not ride it.
	Dismount,
	/// Use the compulsory parallel path instead
	UseSidepath,

	/// Only local traffic and no through traffic allowed
	Destination,
	/// Only allowed for customers
	Customers,

	/// Only for delivery and supply traffic
	///
	/// Often found in pedestrian zones.
	Delivery,
	/// Only for agricultural traffic
	Agricultural,
	/// Only for forestry traffic
	Forestry,
	/// Only for military traffic
	Military,

	/// The access changes depending on for example the time of the day.
	///
	/// This is often found on highways and denoted by screens.
	///
	/// [OSM Wiki](https://wiki.openstreetmap.org/wiki/Variable-access_lanes)
	Variable,
	/// Escape ramps and stopways
	Escape,

	/// The access conditions are unknown or unclear.
	Unknown,
});
