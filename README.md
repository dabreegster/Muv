# Muv

`muv-osm` is parsing library for [OpenStreetMap](https://wiki.openstreetmap.org) data.

## Lanes

The `muv-osm::lanes` module allows you to parse `highway`s, `railway`s, `waterway`s, and `aeroway`s to get a lane-by-lane output of that way.

## License

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
