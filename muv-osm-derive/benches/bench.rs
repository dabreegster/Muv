use criterion::{black_box, criterion_group, criterion_main, Criterion};
use muv_osm_derive::FromOsmStr;
use phf::phf_map;
use std::{
	collections::{BTreeMap, HashMap},
	path::PathBuf,
};

use osmpbf::{Element, ElementReader};

fn load_pbf(name: &str) -> Vec<Vec<(String, String)>> {
	let path: PathBuf = [
		env!("CARGO_MANIFEST_DIR"),
		"benches",
		&format!("{}.osm.pbf", name),
	]
	.iter()
	.collect();

	let r = ElementReader::from_path(path).unwrap();
	let mut tags = Vec::new();

	r.for_each(|e| {
		if let Element::Way(e) = e {
			tags.push(
				e.tags()
					.map(|(k, v)| (k.to_owned(), v.to_owned()))
					.collect(),
			);
		}
	})
	.unwrap();

	tags
}

trait Table<T> {
	fn get(&self, key: &str) -> Option<T>;
}

impl<T: Copy> Table<T> for Vec<(&str, T)> {
	fn get(&self, key: &str) -> Option<T> {
		Some(self.iter().find(|(k, _)| &key == k)?.1)
	}
}

impl<T: Copy> Table<T> for HashMap<&str, T> {
	fn get(&self, key: &str) -> Option<T> {
		self.get(key).copied()
	}
}

impl<T: Copy> Table<T> for BTreeMap<&str, T> {
	fn get(&self, key: &str) -> Option<T> {
		self.get(key).copied()
	}
}

impl<T: Copy> Table<T> for phf::Map<&str, T> {
	fn get(&self, key: &str) -> Option<T> {
		self.get(key).copied()
	}
}

trait FromOsmStr: Sized {
	fn from_osm_str(s: &str) -> Option<Self>;
}

macro_rules! bench_tables {
    ($c:ident $name:literal in $data:expr; $($key:literal => $val:ident,)+) => {{
        let mut group = $c.benchmark_group($name);

        #[derive(Clone, Copy, FromOsmStr, enum_utils::FromStr)]
        enum Val {
            $(
                #[enumeration(rename = stringify!($key))]
                $val,
            )+
        }

        struct Match;

        impl Table<Val> for Match {
            fn get(&self, key: &str) -> Option<Val> {
                Some(match key {
                    $($key => Val::$val,)+
                    _ => return None
                })
            }
        }

        struct Lookup;

        impl Table<Val> for Lookup {
            fn get(&self, key: &str) -> Option<Val> {
                Val::from_osm_str(key)
            }
        }

        struct EnumUtils;

        impl Table<Val> for EnumUtils {
            fn get(&self, key: &str) -> Option<Val> {
                key.parse().ok()
            }
        }

        let linear = vec![$(($key, Val::$val)),+];
        let hash_map: HashMap<_, _> = linear.iter().cloned().collect();
        let btree_map: BTreeMap<_, _> = linear.iter().cloned().collect();
        let phf = phf_map! { $($key => Val::$val,)+ };

        let tables = [
            ("lookup", Box::new(Lookup) as Box<dyn Table<_>>),
            ("match", Box::new(Match)),
            ("enum_utils::FromStr", Box::new(EnumUtils)),
            ("std::Vec", Box::new(linear)),
            ("std::HashMap", Box::new(hash_map)),
            ("std::BTreeMap", Box::new(btree_map)),
            ("phf::Map", Box::new(phf)),
        ];

        for (name, table) in tables {
            group.bench_function(name, |b| b.iter(|| $data.iter().for_each(|tags| tags.iter().filter_map(|(key, _)| table.get(black_box(key))).for_each(|v| { black_box(v); }))));
        }
    }};
}

fn bench(c: &mut Criterion) {
	let data = load_pbf("bremen");

	let tmode_data: Vec<_> = data
		.iter()
		.filter(|tags| !tags.iter().any(|(key, _)| key.contains("highway")))
		.take(100)
		.collect();
	bench_tables! { c "TMode" in tmode_data;
		"foot" => Foot,
		"dog" => Dog,
		"ski" => Ski,
		"inline_skates" => InlineSkates,
		"horse" => Horse,

		"vehicle" => Vehicle,
		"bicycle" => Bicycle,
		"electric_bicycle" => ElectricBicycle,

		"kick_scooter" => KickScooter,
		"carriage" => Carriage,
		"cycle_rickshaw" => CycleRickshaw,
		"hand_cart" => HandCart,
		"trailer" => Trailer,
		"caravan" => Caravan,

		"motor_vehicle" => MotorVehicle,
		"motorcycle" => Motorcycle,
		"moped" => Moped,
		"speed_pedelec" => SpeedPedelec,
		"mofa" => Mofa,
		"small_electric_vehicle" => SmallElectricVehicle,

		"double_tracked_motor_vehicle" => DoubleTrackedMotorVehicle,
		"motorcar" => Motorcar,
		"motorhome" => Motorhome,
		"tourist_bus" => TouristBus,
		"coach" => Coach,
		"goods" => Goods,
		"hgv" => Hgv,
		"hgv_articulated" => HhvArticulated,
		"bdouble" => BDouble,
		"agricultural" => Agricultural,
		"auto_rickshaw" => AutoRickshaw,
		"nev" => Nev,
		"golf_cart" => GolfCart,
		"atv" => Atv,
		"ohv" => Ohv,
		"snowmobile" => Snowmobile,

		"psv" => Psv,
		"bus" => Bus,
		"taxi" => Taxi,
		"minibus" => Minibus,
		"share_taxi" => ShareTaxi,
		"hov" => Hov,
		"carpool" => Carpool,
		"car_sharing" => CarSharing,
		"emergency" => Emergency,
		"hazmat" => Hazmat,
		"disabled" => Disabled,
	};
}

criterion_group!(benches, bench);
criterion_main!(benches);
